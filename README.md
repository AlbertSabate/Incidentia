ESTANDARS DE PROGRAMACIÖ

* Noms en anglès

* Nom de les classes: Les classes començaran amb majuscula i seguiran el seguent format <Concept>[Activity/Fragment/...].java ex. LoginActivity.java, MapActivity.java

* Nom dels atributs interns i variables locals de les classes: Els atributs començaran amb minuscula i si es més d'una paraula de la segona en endavant començaran en majuscula, seràn autoexplicatius. Ex. sum, incidenciaPoints.

* Nom dels widgets de les pantalles: Els noms dels widgets començen amb minuscula i els noms del concepte per el qual serveixen començen amb majuscula. Segueixen el seguent format <widget name><Concept>. Ex. buttonDelete, textViewTitle.

* Nom de variables referents a widgets: el nom d'aquestes variables començara amb "m" i segira amb el nom del widget. La primera lletra del nom que abans era minuscula passa a ser majuscula. Ex. mButtonDelete, mTextViewTitle.

* Nom de funcions: començaran amb minuscula, seran noms autoexplicatius (o el més clars possibles) i si tenen més d'una paraula és distingiran amb la primera lletra majuscula. Ex. calculeIncidenciaPoints()