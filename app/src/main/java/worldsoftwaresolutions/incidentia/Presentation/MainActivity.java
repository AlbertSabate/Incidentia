package worldsoftwaresolutions.incidentia.Presentation;


import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.R;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    final int RC_SIGN_IN = 007;
    private static GoogleApiClient mGoogleApiClient;
    static boolean isRegisteredUser;
    private static Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_drawer);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (getIntent().getExtras() != null) {
            String priority = getIntent().getExtras().getString("priority");
            String category = getIntent().getExtras().getString("category");
            transaction.add(R.id.activity_content, MapFragment.newInstance(category,priority));

        }
        else transaction.add(R.id.activity_content, MapFragment.newInstance(null,null));
        transaction.commit();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.action_main_map);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");

        promptUserDetails();

        if (isRegisteredUser) navigationView.getHeaderView(0).findViewById(R.id.imageViewUserPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ShowProfileActivity.class);
                //                intent.putExtra("userToShow", CurrentUser.getInstance().getUser());
                intent.putExtra("mailToShow", CurrentUser.getInstance().getUser().getMail());
                startActivity(intent);
            }
        });
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                Intent intent = new Intent(MainActivity.this, FilterIncidencesActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.action_add_incidence)
        {
            Intent intent = new Intent(MainActivity.this, IncidenceFormActivity.class);
            intent.putExtra(getString(R.string.open_camera), false);
            intent.putExtra(getString(R.string.form_type), IncidenceFormActivity.Type.CREATE);
            startActivity(intent);
        }
        else if (id == R.id.action_main_map)
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (getIntent().getExtras() != null) {
                String priority = getIntent().getExtras().getString("priority");
                String category = getIntent().getExtras().getString("category");
                transaction.replace(R.id.activity_content, MapFragment.newInstance(category,priority));

            }
            else transaction.replace(R.id.activity_content, MapFragment.newInstance(null,null));
            transaction.commit();

        } else if (id == R.id.action_show_ranking)
        {
            Intent intent = new Intent(MainActivity.this, RankingActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.action_info)
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_content,new AboutFragment());
            transaction.addToBackStack("main");
            transaction.commit();
        }
        else if (id == R.id.action_contact)
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_content,new ContactFragment());
            transaction.addToBackStack("main");
            transaction.commit();
        }
        else if (id == R.id.action_help)
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_content,new HelpFragment());
            transaction.addToBackStack("main");
            transaction.commit();
        }
        else if (id == R.id.action_login)
        {
            Intent intent = new Intent(MainActivity.this, LogInActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.action_logout)
        {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            System.out.println("SignOut");
                        }
                    });
            DomainController.signOut(getApplicationContext());
             Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private String getIncidencesString(int nb)
    {
        if (nb == 1) return String.valueOf(nb) + " " + getResources().getString(R.string.prompt_incidence_single);
        else return String.valueOf(nb) + " " + getResources().getString(R.string.prompt_incidence_multiple);
    }

    private void promptUserDetails()
    {
        View header = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);
        User userToShow = CurrentUser.getInstance().getUser();
        String userIncidences = "";
        String userName="";
        if (CurrentUser.getInstance().getUser() != null && CurrentUser.getInstance().getUser().getMail().equals(getString(R.string.guest_user_mail))) {
            //GUEST USER
            isRegisteredUser = false;
            RoundedBitmapDrawable roundedBitmapDrawable = DomainController.getRoundedThumbnail(getResources(),userToShow.getPicture());
            ((ImageView) header.findViewById(R.id.imageViewUserPhoto)).setImageDrawable(roundedBitmapDrawable);
            userName = userToShow.getName() + " " + userToShow.getSurname();
            Menu nav_Menu = ((NavigationView) findViewById(R.id.nav_view)).getMenu();
            nav_Menu.findItem(R.id.action_logout).setVisible(false);
            nav_Menu.findItem(R.id.action_add_incidence).setVisible(false);
        }
        else if(CurrentUser.getInstance().getUser()!=null){
            //REGISTERED USER
            isRegisteredUser = true;
            userToShow = CurrentUser.getInstance().getUser();
            int nb = CurrentUser.getInstance().getIncidenceArray().size();
            userIncidences = getIncidencesString(nb);
            userName = userToShow.getName() + " " + userToShow.getSurname().charAt(0) + ".";
            ((ImageView) header.findViewById(R.id.imageViewUserPhoto)).setImageDrawable(DomainController.getRoundedThumbnail(getResources(),userToShow.getPicture()));
            Menu nav_Menu = ((NavigationView) findViewById(R.id.nav_view)).getMenu();
            nav_Menu.findItem(R.id.action_login).setVisible(false);
        }
        ((TextView) header.findViewById(R.id.textViewUserIncidences)).setText(userIncidences);
        ((TextView) header.findViewById(R.id.textViewUserName)).setText(userName);
    }

    @Override
    public void onResume() {
        super.onResume();
        promptUserDetails();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        promptUserDetails();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void reloadUserIncidencesNb() {
        String userIncidences;
        if (CurrentUser.getInstance().getUser() != null && !CurrentUser.getInstance().getUser().getMail().equals(getString(R.string.guest_user_mail))) {
            int nb = CurrentUser.getInstance().getIncidenceArray().size();
            userIncidences = getIncidencesString(nb);
        }
        else {
            userIncidences = "";
        }
        View header = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);
        ((TextView) header.findViewById(R.id.textViewUserIncidences)).setText(userIncidences);
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static class MapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

        private GoogleApiClient mGoogleApiClient;
        private LocationRequest mLocationRequest;
        private final static int MY_PERMISSION_FINE_LOCATION = 101;
        private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
        private static final int NO_LATITUDE = 91;
        private static final int NO_LONGITUDE = 181;

        private String category;
        private String priority;

        LatLng currentPosition;

        MapView mMapView;
        private GoogleMap mMap;

        public MapFragment() {}

        public static MapFragment newInstance(String category, String priority) {
            MapFragment fragment = new MapFragment();
            Bundle args = new Bundle();
            args.putString("category", category);
            args.putString("priority", priority);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                category = getArguments().getString("category");
                priority = getArguments().getString("priority");
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View mView=  inflater.inflate(R.layout.fragment_main_map, container, false);

            FloatingActionButton btnCreateIncidence = (FloatingActionButton) mView.findViewById(R.id.floatingActionButtonCreateIncidence);

            if (!isRegisteredUser) btnCreateIncidence.hide();
            currentPosition = new LatLng(NO_LATITUDE, NO_LONGITUDE);


            btnCreateIncidence.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), IncidenceFormActivity.class);
                    intent.putExtra(getString(R.string.open_camera), true);
                    intent.putExtra(getString(R.string.form_type), IncidenceFormActivity.Type.CREATE);
                    startActivity(intent);
                }
            });

            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    // The next two lines tell the new client that “this” current class will handle connection stuff
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    //fourth line adds the LocationServices API endpoint from GooglePlayServices
                    .addApi(LocationServices.API)
                    .build();

            // Create the LocationRequest object
            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(1 * 1000); // 1 second, in milliseconds
            mMapView = (MapView) mView.findViewById(R.id.mainMapView);
            mMapView.onCreate(savedInstanceState);

            mMapView.onResume(); // needed to get the map to display immediately

            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            mMapView.getMapAsync(this);
            setHasOptionsMenu(true);

            return mView;
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater.inflate(R.menu.map_menu, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            mMap.getUiSettings().setScrollGesturesEnabled(true);

            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.clear();
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                if (currentPosition.latitude != NO_LATITUDE && currentPosition.longitude != NO_LONGITUDE) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPosition,15));
                    DomainController.getIncidencesByRadius(currentPosition.latitude, currentPosition.longitude, 380, priority, category, new GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusListener() {
                        @Override
                        public void onGetIncidencesByRadius(List<Incidence> listIncidence) {
                            displayIncidences(listIncidence);
                        }

                        @Override
                        public void onDoesNotGetIncidencesByRadius() {

                        }

                        @Override
                        public void onNotConnection() {

                        }
                    });
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
                }
            }

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String incidenceId = marker.getTitle();
                    String incidenceUser = marker.getSnippet();
                    Intent intent = new Intent(getActivity(),ShowIncidenceActivity.class);
                    intent.putExtra("from", "map");
                    intent.putExtra("idToShow",incidenceId);
                    intent.putExtra("mailUser", incidenceUser);
                    startActivity(intent);
                    getActivity().finish();
                    return true;
                }
            });

        }

        @Override
        public void onResume() {
            super.onResume();
            mMapView.onResume();
            mGoogleApiClient.connect();
        }

        @Override
        public void onPause() {
            super.onPause();
            mMapView.onPause();
            Log.v(this.getClass().getSimpleName(), "onPause()");

            //Disconnect from API onPause()
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mMapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mMapView.onLowMemory();
        }

        @Override
        public void onConnected(@Nullable Bundle bundle) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if (location == null) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

                } else {
                    //If everything went fine lets get latitude and longitude
                    currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                    DomainController.getIncidencesByRadius(currentPosition.latitude, currentPosition.longitude, 380, priority, category, new GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusListener() {
                        @Override
                        public void onGetIncidencesByRadius(List<Incidence> listIncidence) {
                            displayIncidences(listIncidence);
                        }

                        @Override
                        public void onDoesNotGetIncidencesByRadius() {

                        }

                        @Override
                        public void onNotConnection() {

                        }
                        });

                    if (mMap != null) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPosition,15));
                    }
                }
            }
            else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
                }
            }
        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
            if (connectionResult.hasResolution()) {
                try {
                    // Start an Activity that tries to resolve the error
                    connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
                } catch (IntentSender.SendIntentException e) {
                    // Log the error
                    e.printStackTrace();
                }
            } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
                Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
            }
        }

        @Override
        public void onLocationChanged(Location location) {

        }

        private void displayIncidences(List<Incidence> listIncidence) {
            if (mMap != null) {
                mMap.clear();
                for(Incidence i: listIncidence) {
                    Bitmap markerCategory = getCategoryIcon(i.getCategory());
                    Bitmap markerPriority = getPriorityIcon(i.getPriority());
                    Bitmap incidenceMarker = overlay(markerPriority, markerCategory);
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(incidenceMarker);
                    String incidenceId = i.getId();
                    String incidenceUser = i.getUserId();
                    mMap.addMarker(new MarkerOptions().position(i.getLocation()).icon(icon).title(incidenceId).snippet(incidenceUser));
                }
            }
        }

        //Given an Incidence.Category, returns a bitmap corresponding to its icon
        private Bitmap getCategoryIcon(Incidence.Category category) {
            switch (category){
                case SIDEWALK:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.sidewalk);
                case ROAD:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.road);
                case PARK:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.park);
                case LIGHTS:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.lights);
                case TRAFFIC_LIGHTS:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.traffic_lights);
                case CLEANING:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.cleaning);
                case CONTAINER:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.container);
                case TREE:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.tree);
                case OTHER:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.other);
                default:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.other);
            }
        }

        //Given a priority level, returns a bitmap corresponding to its icon
        private Bitmap getPriorityIcon(Incidence.Priority priority) {
            switch (priority) {
                case LOW:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.low_priority);
                case MID:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.mid_priority);
                case HIGH:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.high_priority);
                default:
                    return BitmapFactory.decodeResource(getResources(), R.drawable.low_priority);
            }
        }

        public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
            Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
            Canvas canvas = new Canvas(bmOverlay);
            canvas.drawBitmap(bmp1, new Matrix(), null);
            canvas.drawBitmap(bmp2, 0, 0, null);
            return bmOverlay;
        }


    }

}
