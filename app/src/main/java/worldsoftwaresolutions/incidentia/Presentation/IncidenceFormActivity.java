package worldsoftwaresolutions.incidentia.Presentation;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import worldsoftwaresolutions.incidentia.Domain.CreateIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.EditIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.GetIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.R;

public class IncidenceFormActivity
        extends AppCompatActivity
        implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private String from;

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    enum Type {
        CREATE, EDIT
    }

    //keep track of camera capture intent
    private final int REQUEST_IMAGE_CAPTURE = 1;
    //keep track of pick image intent
    private final int REQUEST_IMAGE_PICK = 2;

    //HashMap to relate each category in all supported languages to its name in English
    private HashMap<String, String> categoryMap;
    private Incidence.Priority priorityIncidence;
    private Incidence.Category categoryIncidence;
    private int priority;
    private String category;

    private boolean imageTools;
    private boolean defaultImage;
    private boolean cameraFirst;
    Context context;

    private Toolbar mToolbar;
    private EditText mEditTextDescription;
    private ImageView mImageViewPreview;
    private Spinner mSpinnerCategory;
    private Spinner mSpinnerPriority;

    private static GoogleMap mMapPreview;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private static final int NO_LATITUDE = 91;
    private static final int NO_LONGITUDE = 181;

    private static LatLng incidencePosition;
    private static double incidenceLongitude;
    private static double incidenceLatitude;
    private static String incidencePoblation;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private Bitmap imageBitmap;
    private static Bitmap incidenceMarker;
    private Bitmap defaultPicture;


    private String id_incidence;
    private Type type;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            } else {
                //If everything went fine lets get latitude and longitude
                if (type == Type.CREATE) {
                    incidenceLatitude = location.getLatitude();
                    incidenceLongitude = location.getLongitude();
                    if (mMapPreview != null) {
                        mMapPreview.clear();
                        incidencePosition = new LatLng(incidenceLatitude, incidenceLongitude);
                        mMapPreview.addMarker(new MarkerOptions().position(incidencePosition)
                                .icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
                        mMapPreview.animateCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition, 15));
                    }
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapPreview = googleMap;
        mMapPreview.getUiSettings().setScrollGesturesEnabled(false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mMapPreview.clear();
            mMapPreview.setMyLocationEnabled(false);
            mMapPreview.getUiSettings().setMyLocationButtonEnabled(true);
            if (incidenceLatitude != NO_LATITUDE && incidenceLongitude != NO_LONGITUDE) {
                incidencePosition = new LatLng(incidenceLatitude, incidenceLongitude);

                mMapPreview.moveCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition, 15));
                mMapPreview.addMarker(new MarkerOptions()
                        .position(incidencePosition).icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();

            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }

        mMapPreview.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.overlay_fragment_container, new SelectLocationFragment(), "Map");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        mSpinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ItemData categorySelected = (ItemData) adapterView.getItemAtPosition(i);
                category = categoryMap.get(categorySelected.getText());
                Bitmap markerCategory = DomainController.getCategoryIcon(Incidence.Category.valueOf(category), getResources());
                Bitmap markerPriority = DomainController.getPriorityIcon(priority, getResources());
                incidenceMarker = DomainController.overlay(markerPriority, markerCategory);
                if (incidenceLatitude != NO_LATITUDE && incidenceLongitude != NO_LONGITUDE) {
                    mMapPreview.clear();
                    mMapPreview.addMarker(new MarkerOptions()
                            .position(incidencePosition).icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSpinnerPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                priority = i;
                Bitmap markerCategory = DomainController.getCategoryIcon(Incidence.Category.valueOf(category), getResources());
                Bitmap markerPriority = DomainController.getPriorityIcon(priority, getResources());
                incidenceMarker = DomainController.overlay(markerPriority, markerCategory);
                if (incidenceLatitude != NO_LATITUDE && incidenceLongitude != NO_LONGITUDE) {
                    mMapPreview.clear();
                    mMapPreview.addMarker(new MarkerOptions()
                            .position(incidencePosition).icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) { // TODO: Consider calling
                        mMapPreview.clear();
                        if (incidenceLatitude != -1 && incidenceLongitude != -1) {
                            incidencePosition = new LatLng(incidenceLatitude, incidenceLongitude);
                            mMapPreview.addMarker(new MarkerOptions().position(incidencePosition)
                                    .icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
                            mMapPreview.animateCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition, 15));

                        }
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "This app requires location permissions", Toast.LENGTH_LONG).show();
                }
                break;
        }

    }


    private HashMap<String, String> mapStringArrays(String[] keyArray, String[] objectArray) {
        HashMap<String, String> ret = new HashMap<>();
        for (int i = 0; i < keyArray.length; ++i) {
            ret.put(keyArray[i], objectArray[i].replaceAll(" ", "_").toUpperCase());
        }
        return ret;
    }


    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container_incidence_form);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        //GETTING VIEWS
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mEditTextDescription = (EditText) findViewById(R.id.editTextDescription);
        mImageViewPreview = (ImageView) findViewById(R.id.imageViewPreview);
        mSpinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);
        mSpinnerPriority = (Spinner) findViewById(R.id.spinnerPriority);
        ImageButton mImageButtonEditPicture = (ImageButton) findViewById(R.id.imageButtonEditPicture);


        //SETTING UP TOOLBAR
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //INITIALIZING CLASS PARAMETERS
        mMapPreview = null;
        incidenceLongitude = NO_LATITUDE;
        incidenceLatitude = NO_LONGITUDE;
        context = this;

        cameraFirst = false;
        defaultPicture = BitmapFactory.decodeResource(getResources(), R.drawable.default_picture);
        imageBitmap = defaultPicture;

        //myPosition = new LatLng(41.387003,2.170058);

        //SETTING UP CATEGORY SPINNER
        final ArrayList<ItemData> categoryList = createCategoryList();
        final SpinnerAdapter mSpinnerAdapterCategory = new SpinnerAdapter(this,
                R.layout.icon_spinner_layout, R.id.txt, categoryList);
        mSpinnerCategory.setAdapter(mSpinnerAdapterCategory);

        //SETTING UP PRIORITY SPINNER
        ArrayList<ItemData> priorityList = createPriorityList();
        SpinnerAdapter mArrayAdapterPriority = new SpinnerAdapter(this,
                R.layout.icon_spinner_layout, R.id.txt, priorityList);
        mSpinnerPriority.setAdapter(mArrayAdapterPriority);

        mImageButtonEditPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageTools(true);
            }
        });

        category = categoryMap.get(categoryList.get(0).getText());
        priority = 0;

        Bitmap markerCategory = DomainController.getCategoryIcon(Incidence.Category.valueOf(category), getResources());
        Bitmap markerPriority = DomainController.getPriorityIcon(priority, getResources());

        incidenceMarker = DomainController.overlay(markerPriority, markerCategory);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mSupportMapFragment.getMapAsync(this);

        //ACKNOWLEDGING IF THE ACTIVITY IS EITHER EDIT OR CREATE
        type = Type.CREATE; //CREATE by default
        if (!getIntent().getExtras().isEmpty()) {
            if (getIntent().getExtras().size() == 3) {
                from = getIntent().getStringExtra("from");
            }
            type = (Type) getIntent().getSerializableExtra(getString(R.string.form_type));
            //If type equals CREATE, we have to know if it's necessary to open the camera
            if (type == Type.CREATE) {
                if (getIntent().getExtras().size() > 1) {
                    if ((boolean) getIntent().getSerializableExtra(getString(R.string.open_camera))) {
                        cameraFirst = true;
                        dispatchTakePictureIntent();
                    }
                }
            } else {
                id_incidence = getIntent().getExtras().getString("incidenceToEdit");
                from = getIntent().getStringExtra("from");

                Incidence incidence = CurrentUser.getInstance().getIncidenceById(id_incidence);
                mEditTextDescription.setText(incidence.getDescription());
                imageBitmap = incidence.getPicture();
                Bitmap tmpBitmap = DomainController.getSquareThumbnail(imageBitmap);
                mImageViewPreview.setImageBitmap(tmpBitmap);

                String[] translatedCategoryNames = getResources().getStringArray(R.array.categoryNames);
                String[] categoryNames = getResources().getStringArray(R.array.categoryNames_en);
                String cat = null;
                category = (incidence.getCategory().toString());
                String result = category.replace("_", " ");

                for (int i = 0; i < categoryNames.length; ++i) {
                    if (categoryNames[i].equalsIgnoreCase(result)) {
                        cat = translatedCategoryNames[i];
                    }
                }

                for (int i = 0; i < categoryList.size(); ++i) {
                    System.out.println(categoryList.get(i).getText());
                    if (categoryList.get(i).getText().equalsIgnoreCase(cat))
                        mSpinnerCategory.setSelection(i);
                }

                priority = getPriorityPosition(incidence.getPriority());
                mSpinnerPriority.setSelection(getPriorityPosition(incidence.getPriority()));

                markerCategory = DomainController.getCategoryIcon(Incidence.Category.valueOf(category), getResources());
                markerPriority = DomainController.getPriorityIcon(priority, getResources());

                incidenceLatitude = incidence.getLocation().latitude;
                incidenceLongitude = incidence.getLocation().longitude;
                incidencePosition = new LatLng(incidenceLatitude, incidenceLongitude);
                incidenceMarker = DomainController.overlay(markerPriority, markerCategory);
                if (mMapPreview != null) {
                    mMapPreview.clear();
                    mMapPreview.moveCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition, 15));
                    mMapPreview.addMarker(new MarkerOptions()
                            .position(incidencePosition).icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
                }

            }
        }
        imageTools = false;
        defaultImage = imageBitmap.equals(defaultPicture);

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                mImageViewPreview.setImageBitmap(DomainController.getSquareThumbnail(imageBitmap));
            }
            else if (requestCode == REQUEST_IMAGE_PICK && data != null && data.getData() != null) {
                Uri uri = data.getData();
                try {
                    imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    mImageViewPreview.setImageBitmap(DomainController.getSquareThumbnail(imageBitmap));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            showImageTools(false);
            defaultImage = false;
            if (cameraFirst) cameraFirst = false;
        }
        else {
            if (cameraFirst) finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (imageTools) {
            showImageTools(false);
        }
        else {
            if(type == Type.EDIT) {

                Intent intent = new Intent(IncidenceFormActivity.this,ShowIncidenceActivity.class);
                intent.putExtra("idToShow",id_incidence);
                intent.putExtra("mailUser",CurrentUser.getInstance().getUser().getMail());
                intent.putExtra("from", from);

                startActivity(intent);
                finish();
            } else {
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.incidence_form_menu, menu);
        if (type == Type.EDIT) {
            //If type == EDIT, we must change the action_submit action icon
            menu.findItem(R.id.action_submit).setIcon(getDrawable(R.drawable.ic_save_white));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_submit:
                if (type == Type.EDIT) {

                    final String description = mEditTextDescription.getText().toString();

                    Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
                    List<Address> addresses = null;
                    try {
                        addresses = gcd.getFromLocation(incidenceLatitude, incidenceLongitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    final String poblation;
                    if(addresses.size() == 0 || addresses.get(0).getLocality()==null) {
                        poblation = "unknown";
                    }
                    else {
                        poblation = addresses.get(0).getLocality();
                    }


                    DomainController.editIncidence(id_incidence, imageBitmap, description, incidencePosition, priority, category, poblation, new EditIncidenceManager.TaskEditIncidenceListener() {
                        @Override
                        public void onEditIncidence() {

                            Incidence i = CurrentUser.getInstance().getIncidenceById(id_incidence);
                            i.setPicture(imageBitmap);
                            i.setDescription(description);
                            i.setLocation(incidencePosition);
                            i.setCategory(getCategory(category));
                            i.setPriority(getPriority(priority));
                            i.setPoblation(poblation);

                            Intent intent = new Intent(IncidenceFormActivity.this,ShowIncidenceActivity.class);
                            intent.putExtra("idToShow",id_incidence);
                            intent.putExtra("mailUser",CurrentUser.getInstance().getUser().getMail());
                            intent.putExtra("from", from);

                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onDoesNotEditIncidence() {
                            Toast.makeText(context, R.string.error_not_edit, Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onNotConnection() {
                            Toast.makeText(context, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                else createIncidence();
                return true;
            case R.id.action_takePicture:
                dispatchTakePictureIntent();
                return true;
            case R.id.action_uploadPicture:
                dispatchUploadPictureIntent();
                return true;
            case R.id.action_deletePicture:
                AlertDialog.Builder builder = new AlertDialog.Builder(IncidenceFormActivity.this);
                builder.setMessage(R.string.alert_delete_picture);
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        resetPicture();
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showImageTools(false);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private int getPriorityPosition(Incidence.Priority priority) {
        switch (priority) {
            case LOW:
                return 0;
            case MID:
                return 1;
            case HIGH:
                return 2;
            default:
                return 0;
        }
    }

    //Returns an ArrayList<ItemData> in which each element contains a category and its icon,
    //ordered by category name in every supported language
    private ArrayList<ItemData> createCategoryList() {
        ArrayList<ItemData> list = new ArrayList<>();
        String [] translatedCategoryNames = getResources().getStringArray(R.array.categoryNames);
        String[] categoryNames = getResources().getStringArray(R.array.categoryNames_en);
        final TypedArray categoryIcons = getResources().obtainTypedArray(R.array.category_icons);
        categoryMap = mapStringArrays(translatedCategoryNames, categoryNames);
        for(int i= 0; i < translatedCategoryNames.length; ++i) {
            list.add(new ItemData(translatedCategoryNames[i], categoryIcons.getResourceId(i, -1)));
        }
        Collections.sort(list);
        categoryIcons.recycle();
        return list;

    }

    //Returns an ArrayList<ItemData> in which each element contains a priority and its icon
    private ArrayList<ItemData> createPriorityList() {
        ArrayList<ItemData> list = new ArrayList<>();
        String[] priorities = getResources().getStringArray(R.array.priorities);
        list.add(new ItemData(priorities[0], R.drawable.low_priority));
        list.add(new ItemData(priorities[1], R.drawable.mid_priority));
        list.add(new ItemData(priorities[2], R.drawable.high_priority));
        return list;

    }
    private Incidence.Category getCategory(String category) {
        switch (category) {
            case "SIDEWALK":
                return Incidence.Category.SIDEWALK;
            case "ROAD":
                return Incidence.Category.ROAD;
            case "PARK":
                return Incidence.Category.PARK;
            case "LIGHTS":
                return Incidence.Category.LIGHTS;
            case "TRAFFIC_LIGHTS":
                return Incidence.Category.TRAFFIC_LIGHTS;
            case "CLEANING":
                return Incidence.Category.CLEANING;
            case "CONTAINER":
                return Incidence.Category.CONTAINER;
            case "TREE":
                return Incidence.Category.TREE;
            case "OTHER":
                return Incidence.Category.OTHER;
            default:
                return Incidence.Category.OTHER;

        }

    }

    private Incidence.Priority getPriority(int priority) {
        switch (priority) {
            case 0:
                return Incidence.Priority.LOW;
            case 1:
                return Incidence.Priority.MID;
            case 2:
                return Incidence.Priority.HIGH;
            default:
                return Incidence.Priority.LOW;
        }
    }

    private void createIncidence() {
        final String description = mEditTextDescription.getText().toString();
        Location location = null;
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(incidenceLatitude, incidenceLongitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        final String poblation;
        if(addresses.size() == 0 || addresses.get(0).getLocality()==null) {
            poblation = "unknown";
        }
        else {
            poblation = addresses.get(0).getLocality();
        }
        ProgressDialog.show(IncidenceFormActivity.this, "", getString(R.string.creating_inicidence), true);
        DomainController.createIncidence(imageBitmap, description, incidencePosition, priority, category, 0, poblation, new CreateIncidenceManager.TaskCreateIncidenceListener() {
            @Override
            public void onCreateIncidence(Incidence incidence) {
                CurrentUser.getInstance().addIncidence(incidence);
                finish();
            }

            @Override
            public void onDoesNotCreateIncidence() {
                Toast.makeText(context, R.string.error_not_created, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNotConnection() {
                Toast.makeText(context, getString(R.string.create_failed_connection), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void dispatchUploadPictureIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_IMAGE_PICK);
    }

    //Resets the picture to the one by default
    private void resetPicture() {
        mImageViewPreview.setImageDrawable(getDrawable(R.drawable.default_picture));
        showImageTools(false);
        defaultImage = true;
    }

    private static void updateMarker() {
        mMapPreview.clear();
        incidencePosition = new LatLng(incidenceLatitude, incidenceLongitude);
        mMapPreview.addMarker(new MarkerOptions().position(incidencePosition)
                .icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
        mMapPreview.animateCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition,15));
    }

    //On true, activates the picture options of the toolbar
    //On false, deactivates them
    private void showImageTools(boolean activate) {
        if (activate) {
            imageTools = true;
            Menu menu = mToolbar.getMenu();
            if(defaultImage) menu.findItem(R.id.action_takePicture).setVisible(true);
            if(defaultImage) menu.findItem(R.id.action_uploadPicture).setVisible(true);
            if(!defaultImage) menu.findItem(R.id.action_deletePicture).setVisible(true);
            menu.findItem(R.id.action_submit).setVisible(false);
            mToolbar.setLogo(null);
            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mToolbar.setNavigationIcon(R.drawable.ic_back);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showImageTools(false);
                }
            });
        }
        else {
            imageTools = false;
            Menu menu = mToolbar.getMenu();
            if (menu.findItem(R.id.action_takePicture) != null) menu.findItem(R.id.action_takePicture).setVisible(false);
            if (menu.findItem(R.id.action_uploadPicture) != null) menu.findItem(R.id.action_uploadPicture).setVisible(false);
            if (menu.findItem(R.id.action_deletePicture) != null) menu.findItem(R.id.action_deletePicture).setVisible(false);
            if (menu.findItem(R.id.action_submit) != null) menu.findItem(R.id.action_submit).setVisible(true);
            mToolbar.setLogo(getDrawable(R.drawable.name));
            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mToolbar.setNavigationIcon(null);
        }
    }


    //Inner class to implement an Adapter to a Spinner which supports one icon per item
    private class SpinnerAdapter extends ArrayAdapter<ItemData> {
        int groupid;
        ArrayList<ItemData> list;
        LayoutInflater inflater;
        public SpinnerAdapter(Activity context, int groupid, int id, ArrayList<ItemData>
                list){
            super(context,id,list);
            this.list=list;
            inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.groupid=groupid;
        }

        public View getView(int position, View convertView, ViewGroup parent ){
            View itemView=inflater.inflate(groupid,parent,false);
            ImageView imageView=(ImageView)itemView.findViewById(R.id.img);
            imageView.setImageResource(list.get(position).getImageId());
            TextView textView=(TextView)itemView.findViewById(R.id.txt);
            textView.setText(list.get(position).getText());
            return itemView;
        }

        public View getDropDownView(int position, View convertView, ViewGroup
                parent){
            return getView(position,convertView,parent);

        }

    }


    //Inner class to implement the data for each item of the SpinnerAdapter
    private class ItemData implements Comparable<ItemData>{

        String text;
        Integer imageId;

        private Collator collator;

        public ItemData(String text, Integer imageId){
            this.text=text;
            this.imageId=imageId;
            collator = Collator.getInstance();
            collator.setStrength(Collator.PRIMARY);
        }


        public String getText(){
            return text;
        }

        public Integer getImageId(){
            return imageId;
        }

        @Override
        public int compareTo(ItemData data) {
            return collator.compare(this.getText(), data.getText());
        }
    }

    public static class SelectLocationFragment extends Fragment implements OnMapReadyCallback {

        public SelectLocationFragment() {}

        private static final String LATITUDE = "latitude";
        private static final String LONGITUDE = "longitude";

        private double selectedLatitude;
        private double selectedLongitude;

        MapView mMapView;
        private GoogleMap mMap;

        public static SelectLocationFragment newInstance(double latitude, double longitude) {
            SelectLocationFragment fragment = new SelectLocationFragment();
            Bundle args = new Bundle();
            args.putDouble(LATITUDE, latitude);
            args.putDouble(LONGITUDE, longitude);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View mView=  inflater.inflate(R.layout.fragment_map, container, false);

            View background = mView.findViewById(R.id.background);
            background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            mView.findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateMarker();
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            });


            mMapView = (MapView) mView.findViewById(R.id.mapView);
            mMapView.onCreate(savedInstanceState);

            mMapView.onResume(); // needed to get the map to display immediately

            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            mMapView.getMapAsync(this);
            return mView;
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
                }
            }
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            if (incidenceLatitude != -1 && incidenceLongitude != -1) {
                incidencePosition = new LatLng(incidenceLatitude, incidenceLongitude);
                mMap.addMarker(new MarkerOptions().position(incidencePosition).icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition,17));
            }
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    mMap.clear();
                    incidencePosition = latLng;
                    incidenceLatitude = latLng.latitude;
                    incidenceLongitude = latLng.longitude;
                    mMap.addMarker(new MarkerOptions().position(incidencePosition).icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(incidencePosition));
                }
            });

        }

        @Override
        public void onResume() {
            super.onResume();
            mMapView.onResume();
        }

        @Override
        public void onPause() {
            super.onPause();
            mMapView.onPause();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mMapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mMapView.onLowMemory();
        }
    }
    


}
