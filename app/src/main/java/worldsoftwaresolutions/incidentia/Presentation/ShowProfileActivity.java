package worldsoftwaresolutions.incidentia.Presentation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetLocalUsersSortedByScoreManager;
import worldsoftwaresolutions.incidentia.Domain.GetUserManager;
import worldsoftwaresolutions.incidentia.Domain.GetUsersSortedByScoreManager;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.R;

public class ShowProfileActivity extends AppCompatActivity {

    private ShowProfileActivity showProfileA;
    public String shownMail;
    private Integer selectedFragment;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_profile);
        this.showProfileA=this;

        //<editor-fold defaultstate="collapsed" desc="View definition">
        final ImageView imageView = (ImageView) findViewById(R.id.imageViewUser);
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollViewShowProfile);
        final RelativeLayout layoutScrolled = (RelativeLayout) findViewById(R.id.layoutScrolled);
        final TextView userName = (TextView) findViewById(R.id.textViewNomUser);
        final TextView userName2 = (TextView) findViewById(R.id.textViewNomUser2);
        final TextView numberIncidence = (TextView) findViewById(R.id.textViewNumberIncidence);
        final TextView numberRanking = (TextView) findViewById(R.id.textViewNumberRanking);
        final TextView stringIncidence = (TextView) findViewById(R.id.textViewIncidence);
        final TextView stringRanking = (TextView) findViewById(R.id.textViewRanking);
        final TextView numberIncidence2 = (TextView) findViewById(R.id.textViewNumberIncidence2);
        final TextView numberRanking2 = (TextView) findViewById(R.id.textViewNumberRanking2);
        final TextView stringIncidence2 = (TextView) findViewById(R.id.textViewIncidence2);
        final TextView stringRanking2 = (TextView) findViewById(R.id.textViewRanking2);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Parameter Controll">
        Bundle parameters = getIntent().getExtras();

        if (parameters != null){

            String mail = parameters.getString("mailToShow");

            if (mail != null) {

                this.shownMail=mail;

                DomainController.getUser(mail, new GetUserManager.TaskGetUserListener() {
                    @Override
                    public void onGetUser(User user) {
                        userName.setText(user.getName() + " " + user.getSurname().charAt(0) + ".");
                        userName2.setText(user.getName() + " " + user.getSurname().charAt(0) + ".");

                        String numberInc = Integer.toString(user.getIncidences().size());
                        String numberRank = Integer.toString(user.getScore());
                        String stringInc = getResources().getString(R.string.incidences);
                        String stringRank = getResources().getString(R.string.ranking);

                        stringRanking.setText(stringRank);
                        numberRanking.setText(numberRank);

                        stringIncidence.setText(stringInc);
                        numberIncidence.setText(numberInc);

                        stringRanking2.setText(stringRank);
                        numberRanking2.setText(numberRank);

                        stringIncidence2.setText(stringInc);
                        numberIncidence2.setText(numberInc);

                    }

                    @Override
                    public void onDoesNotGetUser() {
                        Toast.makeText(getApplicationContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNotConnection() {
                        Toast.makeText(getApplicationContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            }
            else {
                User shownUser = (User) parameters.get("userToShow");

                if(shownUser != null) {

                    this.shownMail=shownUser.getMail();

                    userName.setText(shownUser.getName()+" "+shownUser.getSurname().charAt(0)+".");
                    userName2.setText(shownUser.getName()+" "+shownUser.getSurname().charAt(0)+".");

                    String numberInc = Integer.toString(shownUser.getIncidences().size());
                    String numberRank = Integer.toString(shownUser.getScore());
                    String stringInc = getResources().getString(R.string.incidences);
                    String stringRank = getResources().getString(R.string.ranking);

                    stringRanking.setText(stringRank);
                    numberRanking.setText(numberRank);

                    stringIncidence.setText(stringInc);
                    numberIncidence.setText(numberInc);

                    stringRanking2.setText(stringRank);
                    numberRanking2.setText(numberRank);

                    stringIncidence2.setText(stringInc);
                    numberIncidence2.setText(numberInc);

                }
                else{
                    Toast.makeText(getApplicationContext(), R.string.strange_error, Toast.LENGTH_SHORT).show();
                }
            }

            if(shownMail!=null) {
                if(CurrentUser.getInstance().getUser()!=null && !shownMail.equals(CurrentUser.getInstance().getUser().getMail())) {
                    System.out.println("entro al normal");
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/users/");
                    StorageReference file = storageRef.child(shownMail + ".jpeg");

                    final long ONE_MEGABYTE = 1024 * 1024;
                    file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap userImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                            RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), DomainController.getSquareThumbnail(userImage));
                            roundedDrawable.setCornerRadius(DomainController.getSquareThumbnail(userImage).getHeight());
                            imageView.setImageDrawable(roundedDrawable);

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Bitmap userImage = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.user_default);

                            RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), DomainController.getSquareThumbnail(userImage));
                            roundedDrawable.setCornerRadius(DomainController.getSquareThumbnail(userImage).getHeight());
                            imageView.setImageDrawable(roundedDrawable);
                            System.out.println(e.getMessage());
                        }
                    });
                }
                else if (CurrentUser.getInstance().getUser()!=null){
                    System.out.println("entro a soc jo");

                    Bitmap userImage = CurrentUser.getInstance().getUser().getPicture();

                    RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), DomainController.getSquareThumbnail(userImage));
                    roundedDrawable.setCornerRadius(DomainController.getSquareThumbnail(userImage).getHeight());
                    imageView.setImageDrawable(roundedDrawable);
                }
                else {
                    System.out.println("entro al ultim");

                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/users/");
                    StorageReference file = storageRef.child(shownMail + ".jpeg");

                    final long ONE_MEGABYTE = 1024 * 1024;
                    file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap userImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                            RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), DomainController.getSquareThumbnail(userImage));
                            roundedDrawable.setCornerRadius(DomainController.getSquareThumbnail(userImage).getHeight());
                            imageView.setImageDrawable(roundedDrawable);

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Bitmap userImage = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.user_default);

                            RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), DomainController.getSquareThumbnail(userImage));
                            roundedDrawable.setCornerRadius(DomainController.getSquareThumbnail(userImage).getHeight());
                            imageView.setImageDrawable(roundedDrawable);
                            System.out.println(e.getMessage());
                        }
                    });
                }
            }
            else{
                Bitmap userImage = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.user_default);

                RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), DomainController.getSquareThumbnail(userImage));
                roundedDrawable.setCornerRadius(DomainController.getSquareThumbnail(userImage).getHeight());
                imageView.setImageDrawable(roundedDrawable);
            }


        }
        else {
            Toast.makeText(getApplicationContext(), R.string.strange_error, Toast.LENGTH_SHORT).show();
        }
        //</editor-fold>

        scrollView.post(new Runnable() {

          @Override
          public void run() {
              scrollView.fullScroll(ScrollView.FOCUS_UP);
          }
        });

        //<editor-fold defaultstate="collapsed" desc="Fragment Controll">
        LinearLayout leftFragment = (LinearLayout) findViewById(R.id.TabUserLeft);
        LinearLayout rightFragment = (LinearLayout) findViewById(R.id.TabUserRight);

        LinearLayout leftFragment2 = (LinearLayout) findViewById(R.id.TabUserLeft2);
        LinearLayout rightFragment2 = (LinearLayout) findViewById(R.id.TabUserRight2);

        if (selectedFragment == null) {
          getSupportFragmentManager().beginTransaction().replace(R.id.ContainerShowProfile, new ShowProfileLeftFragment(this)).commit();
          selectedFragment = 1;

          toSelected(stringIncidence);
          toSelected(numberIncidence);
          toSelected(stringIncidence2);
          toSelected(numberIncidence2);

          toDesSelected(stringRanking);
          toDesSelected(stringRanking2);
          toDesSelected(numberRanking);
          toDesSelected(numberRanking2);


        } else if (selectedFragment == 1) {
          getSupportFragmentManager().beginTransaction().replace(R.id.ContainerShowProfile, new ShowProfileLeftFragment(this)).commit();

          toSelected(stringIncidence);
          toSelected(numberIncidence);
          toSelected(stringIncidence2);
          toSelected(numberIncidence2);

          toDesSelected(stringRanking);
          toDesSelected(stringRanking2);
          toDesSelected(numberRanking);
          toDesSelected(numberRanking2);

        } else {
          getSupportFragmentManager().beginTransaction().replace(R.id.ContainerShowProfile, new ShowProfileRightFragment()).commit();

          toSelected(stringRanking);
          toSelected(numberRanking);
          toSelected(stringRanking2);
          toSelected(numberRanking2);

          toDesSelected(stringIncidence);
          toDesSelected(numberIncidence);
          toDesSelected(stringIncidence2);
          toDesSelected(numberIncidence2);
        }

        leftFragment.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              getSupportFragmentManager().beginTransaction()
                      .replace(R.id.ContainerShowProfile, new ShowProfileLeftFragment(showProfileA)).commit();

              toSelected(stringIncidence);
              toSelected(numberIncidence);
              toSelected(stringIncidence2);
              toSelected(numberIncidence2);

              toDesSelected(stringRanking);
              toDesSelected(stringRanking2);
              toDesSelected(numberRanking);
              toDesSelected(numberRanking2);

              scrollControll(scrollView, userName, 1);


              selectedFragment = 1;

          }
        });

        leftFragment2.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              getSupportFragmentManager().beginTransaction()
                      .replace(R.id.ContainerShowProfile, new ShowProfileLeftFragment(showProfileA)).commit();

              toSelected(stringIncidence);
              toSelected(numberIncidence);
              toSelected(stringIncidence2);
              toSelected(numberIncidence2);

              toDesSelected(stringRanking);
              toDesSelected(stringRanking2);
              toDesSelected(numberRanking);
              toDesSelected(numberRanking2);

              scrollControll(scrollView, userName, 1);

              selectedFragment = 1;
          }
        });

        rightFragment.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              getSupportFragmentManager().beginTransaction()
                      .replace(R.id.ContainerShowProfile, new ShowProfileRightFragment(showProfileA)).commit();

              toSelected(stringRanking);
              toSelected(numberRanking);
              toSelected(stringRanking2);
              toSelected(numberRanking2);

              toDesSelected(stringIncidence);
              toDesSelected(numberIncidence);
              toDesSelected(stringIncidence2);
              toDesSelected(numberIncidence2);

              scrollControll(scrollView, userName, 2);

              selectedFragment = 2;

          }
        });

        rightFragment2.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              getSupportFragmentManager().beginTransaction()
                      .replace(R.id.ContainerShowProfile, new ShowProfileRightFragment(showProfileA)).commit();

              toSelected(stringRanking);
              toSelected(numberRanking);
              toSelected(stringRanking2);
              toSelected(numberRanking2);

              toDesSelected(stringIncidence);
              toDesSelected(numberIncidence);
              toDesSelected(stringIncidence2);
              toDesSelected(numberIncidence2);

              scrollControll(scrollView, userName, 2);

              selectedFragment = 2;

          }
        });

        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
          @Override
          public void onScrollChange(View view, int i, int i1, int i2, int i3) {
              Rect scrollBounds = new Rect();
              scrollView.getDrawingRect(scrollBounds);
              float top = userName.getY();
              if (top > scrollBounds.top)
                  layoutScrolled.setVisibility(View.GONE);
              else
                  layoutScrolled.setVisibility(View.VISIBLE);
          }
        });

        //</editor-fold>

        scrollView.post(new Runnable() {

          @Override
          public void run() {
              scrollView.fullScroll(ScrollView.FOCUS_UP);
          }
        });

        final ImageButton backButton = (ImageButton) findViewById(R.id.imageButtonBack);
        final ImageButton editButton = (ImageButton) findViewById(R.id.imageButtonEdit);

        if(CurrentUser.getInstance().getUser()==null || !(this.shownMail.equals(CurrentUser.getInstance().getUser().getMail()))){
            editButton.setVisibility(View.GONE);
        }
        else {
            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ShowProfileActivity.this,EditUserActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
    public void showUser(String mail){
        Intent intent = new Intent(ShowProfileActivity.this,ShowProfileActivity.class);
        intent.putExtra("mailToShow",mail);
        startActivity(intent);
        finish();
    }

    public void showIncidence(String id){
        Intent intent = new Intent(ShowProfileActivity.this,ShowIncidenceActivity.class);
        intent.putExtra("idToShow",id);
        intent.putExtra("mailUser",shownMail);
        startActivity(intent);
        finish();
    }


    @Override
    public void onBackPressed() {

        finish();
    }


    public void toSelected(TextView textView){
        SpannableString spanString = new SpannableString(textView.getText());
        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        textView.setText(spanString);
        textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    public void toDesSelected(TextView textView){
        textView.setText(textView.getText().toString());
        textView.setTextColor(Color.DKGRAY);
    }

    public void scrollControll(ScrollView scrollView, TextView userName, int selected){
        Rect scrollBounds = new Rect();
        scrollView.getDrawingRect(scrollBounds);
        float top = userName.getY();
        int aux2 = (int) top;
        if (selectedFragment==selected){
            if(scrollBounds.top > top)
                scrollView.smoothScrollTo(0,aux2);
        }
        else {
            if(scrollBounds.top >= top)
                scrollView.scrollTo(0,aux2);
        }
    }

    public static class ShowProfileLeftFragment extends Fragment {

        private ShowProfileAdapter madapter;
        private LinearLayoutManager lManager;
        private RecyclerView recyclerView;
        private ShowProfileActivity showProfileActivity;

        public ShowProfileLeftFragment() {
        }

        public ShowProfileLeftFragment(ShowProfileActivity s) {
            this.showProfileActivity=s;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            final View mView = inflater.inflate(R.layout.fragment_show_profile_left, container, false);

            recyclerView = (RecyclerView) mView.findViewById(R.id.recyclerShowProfileLeft);

            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);

            lManager = new GridLayoutManager(getActivity().getApplicationContext(),2);
            lManager.setOrientation(LinearLayoutManager.VERTICAL);

            recyclerView.setLayoutManager(lManager);

            madapter = new ShowProfileAdapter(new ArrayList<String>(),showProfileActivity);

            DomainController.getUser(showProfileActivity.shownMail, new GetUserManager.TaskGetUserListener() {
                @Override
                public void onGetUser(User user) {
                    ArrayList<String> ids = user.getIncidences();
                    madapter = new ShowProfileAdapter(ids,showProfileActivity);
                    recyclerView.setAdapter(madapter);
                }

                @Override
                public void onDoesNotGetUser() {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNotConnection() {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                }
            });

            madapter = new ShowProfileAdapter(new ArrayList<String>(),showProfileActivity);
            recyclerView.setAdapter(madapter);
            return mView;
        }
    }

    public static class ShowProfileRightFragment extends Fragment {

        private RankingAdapter madapter;
        private LinearLayoutManager lManager;
        private RecyclerView recyclerView;
        private ShowProfileActivity showProfileActivity;
        private Button local,general;
        private TextView cityName;

        public ShowProfileRightFragment() {
        }

        public ShowProfileRightFragment(ShowProfileActivity s) {
            this.showProfileActivity=s;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


            final View mView = inflater.inflate(R.layout.fragment_ranking, container, false);

            local = (Button) mView.findViewById(R.id.localButton);
            general = (Button) mView.findViewById(R.id.generalButton);
            cityName = (TextView) mView.findViewById(R.id.cityName);

            local.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    local.setBackgroundTintList(getResources().getColorStateList(R.color.grey));
                    general.setBackgroundTintList(getResources().getColorStateList(R.color.lightGrey));
                    local.setTextColor(getResources().getColor(R.color.colorWhite));
                    general.setTextColor(getResources().getColor(R.color.black));
                    loadLocalRanking();
                }
            });

            general.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    general.setBackgroundTintList(getResources().getColorStateList(R.color.grey));
                    local.setBackgroundTintList(getResources().getColorStateList(R.color.lightGrey));
                    general.setTextColor(getResources().getColor(R.color.colorWhite));
                    local.setTextColor(getResources().getColor(R.color.black));
                    loadGeneralRanking();
                }
            });

            recyclerView = (RecyclerView) mView.findViewById(R.id.recyclerShowProfileRight);

            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);

            lManager = new LinearLayoutManager(getActivity().getApplicationContext());
            lManager.setOrientation(LinearLayoutManager.VERTICAL);

            loadLocalRanking();

            recyclerView.setLayoutManager(lManager);

            return mView;
        }

        private void loadGeneralRanking(){

            System.out.println("loadingGeneralRanking");

            DomainController.getUsersSortedByScore(new GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreListener() {
                @Override
                public void onGetUsersSortedByScore(ArrayList<User> users) {
                    System.out.println("Succes");
                    cityName.setText(R.string.prompt_global_ranking);
                    madapter = new RankingAdapter(users,showProfileActivity);
                    recyclerView.setAdapter(madapter);
                }

                @Override
                public void onDoesNotGetUsersSortedByScore() {
                    System.out.println("DoesNotGetUsersSortedByScore");
                }

                @Override
                public void onNotConnection() {
                    System.out.println("No connection");
                }
            });
        }

        private void loadLocalRanking(){

            System.out.println("loadingLocalRanking");

            DomainController.getUser(showProfileActivity.shownMail, new GetUserManager.TaskGetUserListener() {
                @Override
                public void onGetUser(final User user) {
                    System.out.println("USER: " + user);
                    DomainController.getLocalUsersSortedByScore(user.getPoblation(), new GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreListener() {
                        @Override
                        public void onGetLocalUsersSortedByScore(ArrayList<User> users) {
                            System.out.println("Succes");
                            if (user.getPoblation().equals("unknown")) cityName.setText(R.string.prompt_unknown_ranking);
                            else cityName.setText(user.getPoblation());

                            madapter = new RankingAdapter(users,showProfileActivity);
                            recyclerView.setAdapter(madapter);
                        }

                        @Override
                        public void onDoesNotGetLocalUsersSortedByScore() {
                            Toast.makeText(getContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNotConnection() {
                            Toast.makeText(getContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onDoesNotGetUser() {
                    Toast.makeText(getContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNotConnection() {
                    Toast.makeText(getContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();

                }
            });


        }
    }
}