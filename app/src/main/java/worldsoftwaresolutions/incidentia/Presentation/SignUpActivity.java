package worldsoftwaresolutions.incidentia.Presentation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Domain.CreateUserManager;
import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.Date;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;
import worldsoftwaresolutions.incidentia.Exceptions.MailIsNotValidException;
import worldsoftwaresolutions.incidentia.Exceptions.PasswordIsNotValidException;
import worldsoftwaresolutions.incidentia.R;

public class SignUpActivity extends AppCompatActivity {


    private static final int RC_SIGN_IN = 007;
    static String GoogleName = "";
    static String Googlemail = "";
    static String GoogleSurname = "";

    private static boolean isGoogleUser;
    private static final String TAG = SignUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        isGoogleUser = getIntent().getExtras().getBoolean("isGoogleUser");

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.Container, new SignUpStep1Fragment()).commit();
    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.Container) instanceof FinishSignUpFragment)
        {
            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else super.onBackPressed();
    }





    public static class SignUpStep1Fragment extends Fragment {

        private EditText mNameView;
        private EditText mSurnameView;
        private EditText mBirthdayView;
        private EditText mBirthmonthView;
        private EditText mBirthyearView;
        private Button btnSignOut;
        private SignInButton btnSignIn;
        private TextView textViewSignInWithGoogle;
        private TextView textViewSignInWithoutGoogle;
        private ProgressDialog mProgressDialog;
        private GoogleApiClient mGoogleApiClient;
        private Button nextButton;


        public SignUpStep1Fragment() {}

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View mView=  inflater.inflate(R.layout.fragment_sign_up_step1, container, false);

            nextButton = (Button) mView.findViewById(R.id.nextStep);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    continueAttempt();
                }
            });
            mNameView = (EditText) mView.findViewById(R.id.name);
            mSurnameView = (EditText) mView.findViewById(R.id.surname);
            mBirthdayView = (EditText) mView.findViewById(R.id.birthday);
            mBirthmonthView = (EditText) mView.findViewById(R.id.birthmonth);
            mBirthyearView = (EditText) mView.findViewById(R.id.birthyear);
            btnSignOut = (Button) mView.findViewById(R.id.btn_sign_outSignUp);
            btnSignOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signOut();
                }
            });
            btnSignIn = (SignInButton) mView.findViewById(R.id.btn_sign_inSignUp);

            textViewSignInWithGoogle = (TextView) mView.findViewById(R.id.textViewsign_inSignUp);
            textViewSignInWithoutGoogle = (TextView) mView.findViewById(R.id.textViewsign_outSignUp);

            btnSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signIn();
                }
            });
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                        .enableAutoManage((SignUpActivity) getActivity() /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                                // your code here
                            }
                        })
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();

            }

            // Customizing G+ button
            btnSignIn.setSize(SignInButton.SIZE_STANDARD);
            btnSignIn.setScopes(gso.getScopeArray());



            mBirthdayView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                @Override
                public void afterTextChanged(Editable editable) {
                    mBirthdayView.setError(null);
                    mBirthmonthView.setError(null);
                    mBirthyearView.setError(null);
                    if (editable.length() == 2) {
                        mBirthmonthView.requestFocus();
                    }
                }
            });

            mBirthmonthView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                @Override
                public void afterTextChanged(Editable editable) {
                    mBirthdayView.setError(null);
                    mBirthmonthView.setError(null);
                    mBirthyearView.setError(null);
                    if (editable.length() == 2) {
                        mBirthyearView.requestFocus();
                    }
                }
            });

            mBirthyearView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                @Override
                public void afterTextChanged(Editable editable) {
                    mBirthdayView.setError(null);
                    mBirthmonthView.setError(null);
                    mBirthyearView.setError(null);
                }
            });
            return mView;
        }
        private void signIn() {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }

        private void signOut() {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            updateUI(false);
                        }
                    });
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
            if (opr.isDone()) {
                // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                // and the GoogleSignInResult will be available instantly.
                Log.d(TAG, "Got cached sign-in");
                GoogleSignInResult result = opr.get();
                handleSignInResult(result);
            } else {
                // If the user has not previously signed in on this device or the sign-in has expired,
                // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                // single sign-on will occur in this branch.
                showProgressDialog();
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(GoogleSignInResult googleSignInResult) {
                        hideProgressDialog();
                        handleSignInResult(googleSignInResult);
                    }
                });
            }
        }


        private void showProgressDialog() {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage(getString(R.string.loading));
                mProgressDialog.setIndeterminate(true);
            }

            mProgressDialog.show();
        }

        private void hideProgressDialog() {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            }
        }

        private void updateUI(boolean isSignedIn) {
            if (isSignedIn) {
                nextButton.setText(getString(R.string.action_sign_up));
                btnSignIn.setVisibility(View.GONE);
                textViewSignInWithGoogle.setVisibility(View.GONE);
                btnSignOut.setVisibility(View.VISIBLE);
                textViewSignInWithoutGoogle.setVisibility(View.VISIBLE);
                mNameView.setText(GoogleName);
                mSurnameView.setText(GoogleSurname);
                isGoogleUser = true;


            } else {
                nextButton.setText(getString(R.string.action_next_step));
                btnSignIn.setVisibility(View.VISIBLE);
                textViewSignInWithGoogle.setVisibility(View.VISIBLE);
                btnSignOut.setVisibility(View.GONE);
                textViewSignInWithoutGoogle.setVisibility(View.GONE);
                isGoogleUser = false;
            }
        }

        private void handleSignInResult(GoogleSignInResult result) {
            Log.d(TAG, "handleSignInResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();
                Log.e(TAG, "display name: " + acct.getDisplayName());
                GoogleName = acct.getGivenName();
                GoogleSurname = acct.getFamilyName();
                Googlemail = acct.getEmail();
                Log.e(TAG, "Name: " + GoogleName + ", email: " + Googlemail);
                //TODO el que sigui
                updateUI(true);
            } else {
                // Signed out, show unauthenticated UI.
                updateUI(false);
            }
        }

        private void continueAttempt() {
            mNameView.setError(null);
            mSurnameView.setError(null);
            mBirthyearView.setError(null);

            String name = mNameView.getText().toString();
            String surname = mSurnameView.getText().toString();
            String day = mBirthdayView.getText().toString();
            String month = mBirthmonthView.getText().toString();
            String year = mBirthyearView.getText().toString();
            String  birthday= day + "/" + month + "/" + year;


            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(name)) {
                mNameView.setError(getString(R.string.error_field_required));
                focusView = mNameView;
                cancel = true;
            }
            else if (TextUtils.isEmpty(surname)) {
                mSurnameView.setError(getString(R.string.error_field_required));
                focusView = mSurnameView;
                cancel = true;
            }
            else if (TextUtils.isEmpty(day) || TextUtils.isEmpty(month) || TextUtils.isEmpty(year)) {
                mBirthyearView.setError(getString(R.string.error_field_required));
                focusView = mBirthyearView;
                cancel = true;
            }
            else {
                if (year.length() < 4) {
                    focusView = mBirthyearView;
                    cancel = true;
                }
                else try {
                    new Date(birthday);
                } catch (InvalidDateException e) {
                    mBirthyearView.setError(getString(R.string.error_invalid_date));
                    focusView=mBirthyearView;
                    cancel = true;
                }
            }

            if (cancel) focusView.requestFocus();
            else {
                if (isGoogleUser) {

                    final String password = "";
                    final Bitmap photo = BitmapFactory.decodeResource(getResources(), R.drawable.user_default);


                    try {
                        final Date birth = new Date(birthday);
                        DomainController.createUserGoogle(GoogleName,GoogleSurname, Googlemail, photo,birth, isGoogleUser, new CreateUserManager.TaskCreateUserListener() {
                            @Override
                            public void onCreateUser() {

                                User user = new User(Googlemail,password, GoogleName, GoogleSurname, photo, birth, isGoogleUser, new ArrayList<String>());
                                    CurrentUser currentUser = CurrentUser.getInstance();
                                    currentUser.setUser(user);
                                    ArrayList<Incidence> incidences = new ArrayList<>();
                                    currentUser.setIncidenceArray(incidences);

                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.Container, FinishSignUpFragment.newInstance(GoogleName));
                                transaction.addToBackStack(null);
                                transaction.commit();
                                DomainController.saveAutomaticLogIn(getContext(), Googlemail, password, isGoogleUser);
                            }

                            @Override
                            public void onDoesNotCreateUser() {
                                Toast.makeText(getContext(), getString(R.string.error_user_exists), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNotConnection() {
                                Toast.makeText(getContext(), getString(R.string.create_failed_connection), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (InvalidDateException e) {
                        e.printStackTrace();
                    }

                }
                else {


                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.Container, SignUpStep2Fragment.newInstance(name, surname, birthday));
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        }
    }

    public static class SignUpStep2Fragment extends Fragment {

        private static final String NAME = "name";
        private static final String SURNAME = "surname";
        private static final String DATE = "date";



        private String name;
        private String surname;
        private String email;
        private String password;
        private String date;

        private EditText mEmailView;
        private EditText mPasswordView;
        private EditText mPasswordConfirmationView;


        public SignUpStep2Fragment() {}

        public static SignUpStep2Fragment newInstance(String name, String surname, String date) {
            SignUpStep2Fragment fragment = new SignUpStep2Fragment();
            Bundle args = new Bundle();
            args.putString(NAME, name);
            args.putString(SURNAME, surname);
            args.putString(DATE, date);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                name = getArguments().getString(NAME);
                surname = getArguments().getString(SURNAME);
                date = getArguments().getString(DATE);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View mView=  inflater.inflate(R.layout.fragment_sign_up_step2, container, false);
            final Button nextButton = (Button) mView.findViewById(R.id.signUp);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    continueAttempt();
                }
            });

            mEmailView = (EditText) mView.findViewById(R.id.email);
            if (isGoogleUser) {
                mEmailView.setEnabled(false);
                //mEmailView.setInputType(InputType.TYPE_NULL);
                mEmailView.setText(Googlemail);
            }
            else {
                mEmailView.setEnabled(true);
                //mEmailView.setInputType(InputType.TYPE_CLASS_TEXT);
                mEmailView.setText("");
            }
            mPasswordView = (EditText) mView.findViewById(R.id.password);
            mPasswordConfirmationView = (EditText) mView.findViewById(R.id.confirm_password);
            return mView;
        }

        private void continueAttempt() {

            mEmailView.setError(null);
            mPasswordView.setError(null);
            email = mEmailView.getText().toString();
            password = mPasswordView.getText().toString();
            String passwordConfirmation = mPasswordConfirmationView.getText().toString();

            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            }
            else if (TextUtils.isEmpty(password)) {
                mPasswordView.setError(getString(R.string.error_field_required));
                focusView = mPasswordView;
                cancel = true;
            }
            else if (!password.equals(passwordConfirmation)) {
                mPasswordView.setError(getString(R.string.error_password_match));
                focusView = mPasswordView;
                cancel = true;
            }
            if (cancel) focusView.requestFocus();
            else {
                try {
                    final Bitmap photo = BitmapFactory.decodeResource(getResources(), R.drawable.user_default);
                    DomainController.createUser(name, surname, email, password, photo, new Date(date), isGoogleUser, new CreateUserManager.TaskCreateUserListener() {
                        @Override
                        public void onCreateUser() {
                            try {

                                User user = new User(email,password, name, surname, photo, new Date(date), isGoogleUser, new ArrayList<String>());
                                CurrentUser currentUser = CurrentUser.getInstance();
                                currentUser.setUser(user);

                                ArrayList<Incidence> incidences = new ArrayList<Incidence>();
                                currentUser.setIncidenceArray(incidences);

                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.Container, FinishSignUpFragment.newInstance(name));
                                transaction.addToBackStack(null);
                                transaction.commit();
                            } catch (InvalidDateException e) {
                                e.printStackTrace();
                            }
                            DomainController.saveAutomaticLogIn(getContext(), email, password, isGoogleUser);
                        }

                        @Override
                        public void onDoesNotCreateUser() {
                            mEmailView.setError(getString(R.string.error_user_exists));
                            mEmailView.requestFocus();
                        }

                        @Override
                        public void onNotConnection() {
                            Toast.makeText(getContext(), getString(R.string.create_failed_connection), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                catch (MailIsNotValidException e) {
                    mEmailView.setError(getString(R.string.error_invalid_email));
                    mEmailView.requestFocus();
                }
                catch (PasswordIsNotValidException e) {
                    mPasswordView.setError(getString(R.string.error_invalid_password));
                    mPasswordView.requestFocus();
                }
                catch (InvalidDateException e) {}


            }

        }
    }

    public static class FinishSignUpFragment extends Fragment {

        public FinishSignUpFragment() {}

        private static final String NAME= "name";

        private String name;

        public static FinishSignUpFragment newInstance(String name) {
            FinishSignUpFragment fragment = new FinishSignUpFragment();
            Bundle args = new Bundle();
            args.putString(NAME, name);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                name = getArguments().getString(NAME);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View mView=  inflater.inflate(R.layout.fragment_sign_up_finish, container, false);
            TextView mWelcome = (TextView) mView.findViewById(R.id.welcome);
            String welcome = getString(R.string.welcome_message) +  " " + name;
            mWelcome.setText(welcome);
            mView.findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
            return mView;
        }
    }












}




