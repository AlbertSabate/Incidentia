package worldsoftwaresolutions.incidentia.Presentation;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.EditLocationUserManager;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.R;

public class LocationMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    private GoogleMap mMap;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    Button mSatView;

    double longitude;
    double latitude;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private List<Address> addressList = new ArrayList<>();


    private Integer THRESHOLD = 2;
    private DelayAutoCompleteTextView mGeo_autocomplete;
    private ImageView mGeo_autocomplete_clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_map);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(null);
        toolbar.setTitle(R.string.prompt_location);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);

        longitude = getIntent().getExtras().getDouble("Longitude");
        latitude = getIntent().getExtras().getDouble("Latitude");


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment supportmapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        supportmapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds


        //final AutoCompleteTextView searchText = (AutoCompleteTextView) findViewById(R.id.etLocationEntry);
        mGeo_autocomplete_clear = (ImageView) findViewById(R.id.geo_autocomplete_clear);

        mGeo_autocomplete = (DelayAutoCompleteTextView) findViewById(R.id.geo_autocomplete);
        mGeo_autocomplete.setThreshold(THRESHOLD);
        mGeo_autocomplete.setAdapter(new GeoAutoCompleteAdapter(this)); // 'this' is Activity instance


        mGeo_autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                GeoSearchResult mResult = (GeoSearchResult) adapterView.getItemAtPosition(position);
                mGeo_autocomplete.setText(mResult.getAddress());

                try {
                    Geocoder mGeocoder = new Geocoder(LocationMapActivity.this);
                    Address address = mGeocoder.getFromLocationName(mResult.getAddress(),1).get(0);
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    updateMarkerAndPosition(latLng);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        mGeo_autocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0)
                {
                    mGeo_autocomplete_clear.setVisibility(View.VISIBLE);
                }
                else
                {
                    mGeo_autocomplete_clear.setVisibility(View.GONE);
                }
            }
        });

        mGeo_autocomplete_clear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mGeo_autocomplete.setText("");
            }
        });


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        mGeo_autocomplete.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    Geocoder mGeocoder = new Geocoder(LocationMapActivity.this);
                    try {
                        addressList = mGeocoder.getFromLocationName(mGeo_autocomplete.getText().toString(), 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if ((!addressList.isEmpty())) {
                        Address address = addressList.get(0);
                        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        updateMarkerAndPosition(latLng);
                    }
                }

                return false;
            }
        });

        mSatView = (Button) findViewById(R.id.btSatellite);

        mSatView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (mMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    mSatView.setText("NORM");
                } else {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mSatView.setText("SAT");
                }

            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                updateMarkerAndPosition(latLng);
            }
        });
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {

                if (ActivityCompat.checkSelfPermission(LocationMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    if (location == null) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, LocationMapActivity.this);

                    } else {
                        //If everything went fine lets get latitude and longitude
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        if (mMap != null) {
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            updateMarkerAndPosition(latLng);
                            return true;
                        }
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
                    }
                }
                return false;

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) { // TODO: Consider calling
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    mMap.setMyLocationEnabled(false);
                }
                break;
        }
    }

    private void updateMarkerAndPosition(LatLng latLng) {
        mMap.clear();
        String loc = DomainController.writeLocation(DomainController.getLocationName(latLng.latitude,latLng.longitude, getBaseContext()));
        if (!(loc.contentEquals(" "))) {
            mMap.addMarker(new MarkerOptions().position(latLng).title(loc)).showInfoWindow();
        } else {
            mMap.addMarker(new MarkerOptions().position(latLng)).showInfoWindow();
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17));
        longitude = latLng.longitude;
        latitude = latLng.latitude;

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mInflater = getMenuInflater();
        mInflater.inflate(R.menu.location_map_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                try {
                    action(R.string.save);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void action(int resid) throws IOException {
        final CurrentUser currentUser = CurrentUser.getInstance();
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses = gcd.getFromLocation(latitude, longitude, 1);
        final String poblation;
        if(addresses.size() == 0 || addresses.get(0).getLocality()==null) {
           poblation = "unknown";
        }
        else {
            poblation = addresses.get(0).getLocality();
        }
        DomainController.editLocation(currentUser.getUser().getMail(), latitude,longitude, poblation, new EditLocationUserManager.TaskEditLocationUserListener() {
            @Override
            public void onEditLocationUser() {
                try {
                    User auxUser=CurrentUser.getInstance().getUser();
                    auxUser.setLatitude(latitude);
                    auxUser.setLongitude(longitude);
                    auxUser.setPoblation(poblation);
                    CurrentUser.getInstance().setUser(auxUser);
                    Toast.makeText(getApplicationContext(), R.string.message_location, Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onDoesNotEditLocationUser() {
                Toast.makeText(LocationMapActivity.this, R.string.error_not_edit, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNotConnection() {
                Toast.makeText(LocationMapActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onBackPressed() {
        try {
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LatLng latLng;
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                if ((latitude != 91.0000000) && (longitude != 181.000000)) {
                    latLng = new LatLng(latitude, longitude);
                } else {
                    latLng = new LatLng(41.387003,2.170058);
                }
            } else {
                //If everything went fine lets get latitude and longitude
                if ((latitude != 91.0000000) && (longitude != 181.000000)) {
                    latLng = new LatLng(latitude, longitude);
                } else {
                    latLng = new LatLng(location.getLatitude(),location.getLongitude());
                }
            }
            updateMarkerAndPosition(latLng);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
 /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

}




