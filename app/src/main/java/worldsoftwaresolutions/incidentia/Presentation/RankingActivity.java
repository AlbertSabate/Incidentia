package worldsoftwaresolutions.incidentia.Presentation;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetLocalUsersSortedByScoreManager;
import worldsoftwaresolutions.incidentia.Domain.GetUserManager;
import worldsoftwaresolutions.incidentia.Domain.GetUsersSortedByScoreManager;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.R;

import static worldsoftwaresolutions.incidentia.R.drawable.container;

public class RankingActivity  extends AppCompatActivity {

    private RankingAdapter madapter;
    private LinearLayoutManager lManager;
    private RecyclerView recyclerView;
    private Button local,general;
    private TextView cityName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setLogo(null);
        mToolbar.setTitle(R.string.ranking);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        local = (Button) findViewById(R.id.localButton);
        general = (Button) findViewById(R.id.generalButton);
        cityName = (TextView) findViewById(R.id.cityName);


        local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                local.setBackgroundTintList(getResources().getColorStateList(R.color.grey));
                general.setBackgroundTintList(getResources().getColorStateList(R.color.lightGrey));
                local.setTextColor(getResources().getColor(R.color.colorWhite));
                general.setTextColor(getResources().getColor(R.color.black));
                loadLocalRanking();
            }
        });

        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                general.setBackgroundTintList(getResources().getColorStateList(R.color.grey));
                local.setBackgroundTintList(getResources().getColorStateList(R.color.lightGrey));
                general.setTextColor(getResources().getColor(R.color.colorWhite));
                local.setTextColor(getResources().getColor(R.color.black));
                loadGeneralRanking();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerShowProfileRight);

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);

        lManager = new LinearLayoutManager(getBaseContext());
        lManager.setOrientation(LinearLayoutManager.VERTICAL);

        loadLocalRanking();

        recyclerView.setLayoutManager(lManager);

    }

    public void showUser(String mail){
        Intent intent = new Intent(RankingActivity.this,ShowProfileActivity.class);
        intent.putExtra("mailToShow",mail);
        startActivity(intent);
        //finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RankingActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void loadGeneralRanking(){

        System.out.println("loadingGeneralRanking");

        DomainController.getUsersSortedByScore(new GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreListener() {
            @Override
            public void onGetUsersSortedByScore(ArrayList<User> users) {
                System.out.println("Succes");
                cityName.setText(R.string.prompt_global_ranking);
                madapter = new RankingAdapter(users,RankingActivity.this);
                recyclerView.setAdapter(madapter);
            }

            @Override
            public void onDoesNotGetUsersSortedByScore() {
                System.out.println("DoesNotGetUsersSortedByScore");
            }

            @Override
            public void onNotConnection() {
                System.out.println("No connection");
            }
        });
    }

    private void loadLocalRanking(){

        System.out.println("loadingLocalRanking");
        final String name;
        if (CurrentUser.getInstance().getUser() != null ) {

            if (CurrentUser.getInstance().getUser().getMail().equals(getString(R.string.guest_user_mail))) {
                name = "unknown";
            } else {
                name = CurrentUser.getInstance().getUser().getPoblation();
            }
        } else {
            name = "unknown";
        }

        DomainController.getLocalUsersSortedByScore(name, new GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreListener() {
            @Override
            public void onGetLocalUsersSortedByScore(ArrayList<User> users) {
                System.out.println("Succes");
                if (name.equals("unknown")) cityName.setText(R.string.prompt_unknown_ranking);
                else {
                    cityName.setText(name);
                }
                madapter = new RankingAdapter(users, RankingActivity.this);
                recyclerView.setAdapter(madapter);
            }

            @Override
            public void onDoesNotGetLocalUsersSortedByScore() {
                Toast.makeText(getBaseContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNotConnection() {
                Toast.makeText(getBaseContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
            }
        });

    }
}