package worldsoftwaresolutions.incidentia.Presentation;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import worldsoftwaresolutions.incidentia.R;

public class MyRecyclerViewAdapter  extends
        RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder>  {

    private List<String> questionsList;
    private List<String> answersList;

    public MyRecyclerViewAdapter(List<String> questionsList,List<String> answersList) {
        this.questionsList = questionsList;
        this.answersList = answersList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.help_row, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.tvQuestion.setText(questionsList.get(position));
        holder.tvAswer.setText(answersList.get(position));
        holder.tvAswer.setVisibility(View.GONE);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.tvAswer.getVisibility() == View.GONE)
                    holder.tvAswer.setVisibility(View.VISIBLE);
                else
                    holder.tvAswer.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView cardView;
        public TextView tvQuestion;
        public TextView tvAswer;

        public MyViewHolder(View view) {
            super(view);
            cardView = (CardView) view.findViewById(R.id.card_view);
            tvQuestion = (TextView) view.findViewById(R.id.question);
            tvAswer = (TextView) view.findViewById(R.id.answer);
        }
    }
}
