package worldsoftwaresolutions.incidentia.Presentation;

/**
 * Created by Pau on 22/04/2017.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.Date;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.LogInUserGoogleManager;
import worldsoftwaresolutions.incidentia.Domain.LogInUserManager;
import worldsoftwaresolutions.incidentia.Domain.Persistence;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;
import worldsoftwaresolutions.incidentia.Exceptions.MailIsNotValidException;
import worldsoftwaresolutions.incidentia.Exceptions.PasswordIsNotValidException;
import worldsoftwaresolutions.incidentia.R;

public class Splashscreen extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = Splashscreen.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }
    /** Called when the activity is first created. */
    Thread splashTread;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage( this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }
    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.splash);
        iv.clearAnimation();
        iv.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 3500) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = new Intent(Splashscreen.this,
                            MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    Splashscreen.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    Splashscreen.this.finish();
                }

            }
        };
        splashTread.start();

    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());
            final String emailGoogle = acct.getEmail();
            logInWithGoogle(emailGoogle);
            Log.e(TAG, "email: " + emailGoogle);


        } else {
            // Signed out, show unauthenticated UI.
            logInWithoutGoogle();
        }
    }

    private void logInWithGoogle(String emailGoogle) {
        DomainController.logInUserGoogle(emailGoogle, new LogInUserGoogleManager.TaskLogInUserGoogleListener() {
            @Override
            public void onLogInUserGoogle(final User user) {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/users/");
                StorageReference file = storageRef.child(user.getMail() + ".jpeg");
                final long ONE_MEGABYTE = 1024 * 1024;
                file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        Bitmap userImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        user.setPicture(userImage);
                        CurrentUser currentUser = CurrentUser.getInstance();
                        currentUser.setUser(user);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Bitmap userImage = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.user_default);

                        user.setPicture(userImage);
                        CurrentUser currentUser = CurrentUser.getInstance();
                        currentUser.setUser(user);

                    }
                });




                inicialize_current_user_incidence(user);


            }

            @Override
            public void onUserRegisteredWithoutGoogle() {
                //
            }


            @Override
            public void onUserNotRegistered() {
                //Toast.makeText(getApplicationContext(),"Incorrect mail",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onOtherError() {
                Toast.makeText(getApplicationContext(), "Strange error", Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onNotConnection() {
                Toast.makeText(getApplicationContext(), "Error connecting to internet", Toast.LENGTH_SHORT).show();

            }


        });

        System.out.println("mail: " + emailGoogle);
        StartAnimations();
    }

    private void logInWithoutGoogle() {
        String mail = "";
        String password = "";
        try {
            mail = DomainController.getAutomaticLogInMail(getApplicationContext());
            password = DomainController.getAutomaticLogInPassword(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        final CurrentUser currentUser = CurrentUser.getInstance();
        currentUser.setUser(DomainController.getGuestUser(getResources()));
        if (mail != "" && password != "") {
            try {

                DomainController.logInUser(mail, password, new LogInUserManager.TaskLogInUserListener() {
                    @Override
                    public void onLogInUser(final User user) {
                        FirebaseStorage storage = FirebaseStorage.getInstance();
                        StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/users/");
                        StorageReference file = storageRef.child(user.getMail() + ".jpeg");
                        final long ONE_MEGABYTE = 1024 * 1024;
                        file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                            @Override
                            public void onSuccess(byte[] bytes) {
                                Bitmap userImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                user.setPicture(userImage);
                                CurrentUser currentUser = CurrentUser.getInstance();
                                currentUser.setUser(user);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                                Bitmap userImage = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.user_default);

                                user.setPicture(userImage);
                                CurrentUser currentUser = CurrentUser.getInstance();
                                currentUser.setUser(user);

                            }
                        });
                        inicialize_current_user_incidence(user);


                    }


                    @Override
                    public void onPasswordIncorrect() {
                        //Toast.makeText(getApplicationContext(),"Incorrect password" ,Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onUserNotRegistered() {
                        //Toast.makeText(getApplicationContext(),"Incorrect mail",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onOtherError() {
                        Toast.makeText(getApplicationContext(), "Strange error", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onRegisteredWithGoogle() {
                        //
                    }

                    @Override
                    public void onNotConnection() {
                        Toast.makeText(getApplicationContext(), "Error connecting to internet", Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (PasswordIsNotValidException e) {
                e.printStackTrace();
            } catch (MailIsNotValidException e) {
                e.printStackTrace();
            }
        }
        System.out.println("mail: " + mail);
        StartAnimations();
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        }

        else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
//////////////// showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }

    }

    private void inicialize_current_user_incidence(final User user) {
        CurrentUser currentUser = CurrentUser.getInstance();
        currentUser.newIncidence();
        final ArrayList<Incidence> incidences = new ArrayList<Incidence>();
        ArrayList<String> ids = user.getIncidences();

        for (int i=0; i<ids.size();++i) {
            DomainController.getIncidence(ids.get(i), new GetIncidenceManager.TaskGetIncidenceListener() {
                @Override
                public void onGetIncidence(final Incidence incidence) {
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/incidences/");
                    StorageReference file = storageRef.child(incidence.getId()+".jpeg");

                    final long ONE_MEGABYTE = 1024 * 1024;
                    file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            System.out.println("SPLASHSCREEN:, BYTE: " + bytes.length);
                            Bitmap incidencePicture = BitmapFactory.decodeByteArray(bytes , 0, bytes.length);
                            incidence.setPicture(DomainController.getSquareThumbnail(incidencePicture));
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Bitmap itemView = BitmapFactory.decodeResource(getBaseContext().getResources(),
                                    R.drawable.default_picture);
                            incidence.setPicture(itemView);
                        }
                    });

                    incidences.add(incidence);
                }

                @Override
                public void onDoesNotGetIncidence() {
                    Toast.makeText(getBaseContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNotConnection() {
                    Toast.makeText(getBaseContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                }
            });
        }
        currentUser.setIncidenceArray(incidences);
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }


    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }


}