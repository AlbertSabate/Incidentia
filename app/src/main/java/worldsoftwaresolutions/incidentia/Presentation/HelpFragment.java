package worldsoftwaresolutions.incidentia.Presentation;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import worldsoftwaresolutions.incidentia.R;

public class HelpFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler);


        LinearLayoutManager layoutManager = new LinearLayoutManager(container.getContext());
        recyclerView.setLayoutManager(layoutManager);

        Resources res = getResources();
        List<String> q = new ArrayList();
        q.add(res.getString(R.string.help_question_1));
        q.add(res.getString(R.string.help_question_2));
        q.add(res.getString(R.string.help_question_3));
        List<String> a = new ArrayList();
        a.add(res.getString(R.string.help_answer_1));
        a.add(res.getString(R.string.help_answer_2));
        a.add(res.getString(R.string.help_answer_3));

        MyRecyclerViewAdapter adapter = new MyRecyclerViewAdapter(q,a);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
