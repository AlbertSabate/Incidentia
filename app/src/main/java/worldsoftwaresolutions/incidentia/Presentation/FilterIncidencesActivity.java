package worldsoftwaresolutions.incidentia.Presentation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.R;

public class FilterIncidencesActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private Spinner mSpinnerCategory;
    private Spinner mSpinnerPriority;

    private String category;
    private String priority;

    //HashMap to relate each category in all supported languages to its name in English
    private HashMap<String, String> categoryMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_incidences);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mSpinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);
        mSpinnerPriority = (Spinner) findViewById(R.id.spinnerPriority);

        mToolbar.setTitle("");
        mToolbar.setNavigationIcon(R.drawable.ic_back);



        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //SETTING UP CATEGORY SPINNER
        ArrayList<FilterIncidencesActivity.ItemData> categoryList= createCategoryList();
        FilterIncidencesActivity.SpinnerAdapter mSpinnerAdapterCategory=new FilterIncidencesActivity.SpinnerAdapter(this,
                R.layout.icon_spinner_layout,R.id.txt,categoryList);
        mSpinnerCategory.setAdapter(mSpinnerAdapterCategory);

        ArrayList<FilterIncidencesActivity.ItemData> priorityList = createPriorityList();
        FilterIncidencesActivity.SpinnerAdapter mArrayAdapterPriority=new FilterIncidencesActivity.SpinnerAdapter(this,
                R.layout.icon_spinner_layout,R.id.txt,priorityList);
        mSpinnerPriority.setAdapter(mArrayAdapterPriority);

        mSpinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ItemData categorySelected = (ItemData) adapterView.getItemAtPosition(i);
                if (i != 0) category = categoryMap.get(categorySelected.getText());
                else category = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSpinnerPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ItemData prioritySelected = (ItemData) adapterView.getItemAtPosition(i);
                if (i == 1) priority = Incidence.Priority.LOW.name();
                else if (i == 2) priority = Incidence.Priority.MID.name();
                else if (i == 3) priority = Incidence.Priority.HIGH.name();
                else priority = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_apply_filter:
                Intent intent = new Intent(FilterIncidencesActivity.this, MainActivity.class);
                intent.putExtra("category", category);
                intent.putExtra("priority", priority);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(FilterIncidencesActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //Returns an ArrayList<ItemData> in which each element contains a category and its icon,
    //ordered by category name in every supported language
    private ArrayList<FilterIncidencesActivity.ItemData> createCategoryList() {
        ArrayList<FilterIncidencesActivity.ItemData> list = new ArrayList<>();
        String [] translatedCategoryNames = getResources().getStringArray(R.array.categoryNames);
        String[] categoryNames = getResources().getStringArray(R.array.categoryNames_en);
        final TypedArray categoryIcons = getResources().obtainTypedArray(R.array.category_icons);
        categoryMap = mapStringArrays(translatedCategoryNames, categoryNames);
        for(int i= 0; i < translatedCategoryNames.length; ++i) {
            list.add(new FilterIncidencesActivity.ItemData(translatedCategoryNames[i], categoryIcons.getResourceId(i, -1)));
        }
        Collections.sort(list);
        list.add(0, new ItemData(getString(R.string.prompt_no_category)));
        categoryIcons.recycle();
        return list;

    }

    //Returns an ArrayList<ItemData> in which each element contains a priority and its icon
    private ArrayList<FilterIncidencesActivity.ItemData> createPriorityList() {
        ArrayList<FilterIncidencesActivity.ItemData> list = new ArrayList<>();
        String[] priorities = getResources().getStringArray(R.array.priorities);
        list.add(new ItemData(getString(R.string.prompt_no_priority)));
        list.add(new FilterIncidencesActivity.ItemData(priorities[0], R.drawable.low_priority));
        list.add(new FilterIncidencesActivity.ItemData(priorities[1], R.drawable.mid_priority));
        list.add(new FilterIncidencesActivity.ItemData(priorities[2], R.drawable.high_priority));
        return list;

    }

    private HashMap<String, String> mapStringArrays(String[] keyArray, String[] objectArray) {
        HashMap<String, String> ret = new HashMap<>();
        for(int i= 0; i < keyArray.length; ++i) {
            ret.put(keyArray[i], objectArray[i].replaceAll(" ", "_").toUpperCase());
        }
        return ret;
    }

    private class SpinnerAdapter extends ArrayAdapter<FilterIncidencesActivity.ItemData> {
        int groupid;
        ArrayList<FilterIncidencesActivity.ItemData> list;
        LayoutInflater inflater;
        public SpinnerAdapter(Activity context, int groupid, int id, ArrayList<FilterIncidencesActivity.ItemData>
                list){
            super(context,id,list);
            this.list=list;
            inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.groupid=groupid;
        }

        public View getView(int position, View convertView, ViewGroup parent ){
            View itemView=inflater.inflate(groupid,parent,false);
            ImageView imageView=(ImageView)itemView.findViewById(R.id.img);
            if (!list.get(position).getImageId().equals(ItemData.getNoImageValue())) {
                imageView.setImageResource(list.get(position).getImageId());
            }
            TextView textView=(TextView)itemView.findViewById(R.id.txt);
            textView.setText(list.get(position).getText());
            return itemView;
        }

        public View getDropDownView(int position, View convertView, ViewGroup
                parent){
            return getView(position,convertView,parent);

        }
    }

    private static class ItemData implements Comparable<FilterIncidencesActivity.ItemData>{

        String text;
        Integer imageId;

        private static final Integer NO_IMAGE = -1;

        private Collator collator;

        public ItemData(String text, Integer imageId){
            this.text=text;
            this.imageId=imageId;
            collator = Collator.getInstance();
            collator.setStrength(Collator.PRIMARY);
        }

        public ItemData(String text){
            this.text=text;
            this.imageId= NO_IMAGE;
            collator = Collator.getInstance();
            collator.setStrength(Collator.PRIMARY);
        }


        public String getText(){
            return text;
        }

        public Integer getImageId(){
            return imageId;
        }

        public static Integer getNoImageValue() {
            return NO_IMAGE;
        }

        @Override
        public int compareTo(FilterIncidencesActivity.ItemData data) {
            return collator.compare(this.getText(), data.getText());
        }
    }
}
