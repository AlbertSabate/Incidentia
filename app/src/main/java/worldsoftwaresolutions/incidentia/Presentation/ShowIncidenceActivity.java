package worldsoftwaresolutions.incidentia.Presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import worldsoftwaresolutions.incidentia.Domain.Comment;
import worldsoftwaresolutions.incidentia.Domain.CommentAdapter;
import worldsoftwaresolutions.incidentia.Domain.ConfirmIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.CreateCommentManager;
import worldsoftwaresolutions.incidentia.Domain.CurrentUser;

import worldsoftwaresolutions.incidentia.Domain.DeleteCommentManager;
import worldsoftwaresolutions.incidentia.Domain.DeleteIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager;
import worldsoftwaresolutions.incidentia.Domain.GetIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.ReportIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.UnconfirmIncidenceManager;
import worldsoftwaresolutions.incidentia.R;

public class ShowIncidenceActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private TextView mDescription;
    private TextView mUser;
    private TextView mCategory;
    private ImageView mImage;
    private ImageView mIcon;
    private ImageView mIconAction;
    private ImageView mIcon2;
    private TextView mLocation;
    private ImageView mConfirm;
    private TextView mNumConfirmations;
    private TextView mConfirmation;
    private Button mAddComment;
    private EditText mText;
    private RecyclerView mRecyclerView;
    private CommentAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean confirmed;
    private String mail_user;
    private String from;
    private static GoogleMap mMapPreview;
    private GoogleApiClient mGoogleApiClient;
    private static String categoryGlobal;
    private static int priorityGlobal;
    private static Bitmap incidenceMarker;
    private String id_incidence;
    private int incidenceReported;
    private int incidenceFixed;
    private int incidenceInvalid;
    static double latitude;
    static double longitude;
    Toolbar mToolbar;
    static private ShowIncidenceActivity showIncidenceActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container_incidence_show);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setPopupTheme(R.style.ThemeOverlay_AppCompat_ActionBar);
        mToolbar.setLogo(null);
        mToolbar.setTitle(R.string.title_activity_show_incidence);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);




        showIncidenceActivity=this;
        from = getIntent().getExtras().getString("from");
        mail_user = getIntent().getExtras().getString("mailUser");
        id_incidence = getIntent().getExtras().getString("idToShow");
        mImage = (ImageView) findViewById(R.id.showPicture);
        mConfirm = (ImageView) findViewById(R.id.confirm);
        mNumConfirmations = (TextView) findViewById(R.id.textViewNumberConfirmations);
        mConfirmation = (TextView) findViewById(R.id.textConfirmation);
        mUser = (TextView) findViewById(R.id.showUser);
        mDescription= (TextView) findViewById(R.id.showDescription);
        mLocation = (TextView) findViewById(R.id.incidence_location);
        mIcon = (ImageView) findViewById(R.id.showIconCategory);
        mIconAction = (ImageView) findViewById(R.id.showIconAction);
        mIcon2 = (ImageView) findViewById(R.id.showIconCategory2);
        mCategory = (TextView) findViewById(R.id.showCategory);
        mAddComment = (Button) findViewById(R.id.addComment);
        mText = (EditText) findViewById(R.id.textComment);

        SupportMapFragment supportmapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        supportmapFragment.getMapAsync(ShowIncidenceActivity.this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        if (CurrentUser.getInstance().getUser().getMail().equals(getString(R.string.guest_user_mail))) {
            mConfirm.setEnabled(false);
            mAddComment.setEnabled(false);
            mText.setEnabled(false);

        }

        if (CurrentUser.getInstance().getUser().getMail().equals(mail_user)) {
            mConfirm.setEnabled(false);
        }

            DomainController.getIncidence(id_incidence, new GetIncidenceManager.TaskGetIncidenceListener() {
            @Override
            public void onGetIncidence(Incidence incidence) {


                mDescription.setText(incidence.getDescription());
                mUser.setText(incidence.getUserId());

                mNumConfirmations.setText(String.valueOf(incidence.getNb_confirmations()));
                if (incidence.getNb_confirmations()==1) mConfirmation.setText(R.string.confirmation);
                else mConfirmation.setText(R.string.confirmations);


                if (CurrentUser.getInstance().getUser().getMail().equals(mail_user)) {
                    Bitmap tmpBitmap = DomainController.getSquareThumbnail(CurrentUser.getInstance().getIncidenceById(id_incidence).getPicture());
                    mImage.setImageBitmap(tmpBitmap);
                } else {
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/incidences/");
                    StorageReference file = storageRef.child(id_incidence+".jpeg");

                    final long ONE_MEGABYTE = 1024 * 1024;
                    file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap incidencePicture = BitmapFactory.decodeByteArray(bytes , 0, bytes.length);
                            Bitmap tmpBitmap = DomainController.getSquareThumbnail(incidencePicture);
                            mImage.setImageBitmap(tmpBitmap);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mImage.setImageDrawable(getResources().getDrawable(R.drawable.default_picture));
                        }
                    });

                }

                if (incidence.isConfirmed()) {
                    mConfirm.setImageDrawable(getResources().getDrawable(R.drawable.like_green));
                    confirmed = true;
                }   else {
                    mConfirm.setImageDrawable(getResources().getDrawable(R.drawable.like_dark));
                    confirmed = false;
                }


                Menu menu =  mToolbar.getMenu();

                if (incidence.isReported()) {
                    mIconAction.setImageDrawable(getResources().getDrawable(R.drawable.ic_reported));
                    incidenceReported = 1; // reported
                } else {
                    incidenceReported = 2; // no reported
                }

                if (incidence.isFixed()) {
                    mIconAction.setImageDrawable(getResources().getDrawable(R.drawable.ic_fixed));
                    mConfirm.setEnabled(false);
                    incidenceFixed = 1; // fixed
                } else {
                    incidenceFixed = 2; // no fixed
                }

                if (incidence.isInvalid()) {
                    mIconAction.setImageDrawable(getResources().getDrawable(R.drawable.ic_reported));

                    mConfirm.setEnabled(false);
                    incidenceInvalid = 1; // invalid
                } else {
                    incidenceInvalid = 2; // valid
                }

                if ((!(menu.findItem(R.id.action_options_delete)==null)) && (!(menu.findItem(R.id.action_options_edit)==null))
                    && (!(menu.findItem(R.id.action_options_report)==null))) {

                    if (mail_user.equals(CurrentUser.getInstance().getUser().getMail())) {

                        menu.findItem(R.id.action_options_delete).setVisible(true);
                        if (incidenceInvalid == 2 && incidenceFixed == 2) {
                            menu.findItem(R.id.action_options_edit).setVisible(true);
                        }
                    } else {
                        if (incidenceReported == 2) {
                            if (!(CurrentUser.getInstance().getUser().getMail().equals(getString(R.string.guest_user_mail)))) {
                                if (incidenceInvalid == 2 && incidenceFixed == 2) {
                                    menu.findItem(R.id.action_options_report).setVisible(true);
                                }
                            }
                        }
                    }
                }

                String category = null;
                String priority = null;
                String[] priorities = getResources().getStringArray(R.array.priorities);
                String [] categoryNames = getResources().getStringArray(R.array.categoryNames);

                switch (incidence.getPriority()){
                    case LOW:
                        mIcon.setBackgroundDrawable(getResources().getDrawable(R.drawable.low_priority));
                        mIcon2.setBackgroundDrawable(getResources().getDrawable(R.drawable.low_priority));
                        priority = priorities[0];
                        priorityGlobal = 0;
                        break;
                    case MID:
                        mIcon.setBackgroundDrawable(getResources().getDrawable(R.drawable.mid_priority));
                        mIcon2.setBackgroundDrawable(getResources().getDrawable(R.drawable.mid_priority));
                        priority = priorities[1];
                        priorityGlobal = 1;

                        break;
                    case HIGH:
                        mIcon.setBackgroundDrawable(getResources().getDrawable(R.drawable.high_priority));
                        mIcon2.setBackgroundDrawable(getResources().getDrawable(R.drawable.high_priority));
                        priority = priorities[2];
                        priorityGlobal = 2;

                        break;
                    default:
                        break;
                }

                switch (incidence.getCategory()){
                    case SIDEWALK:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.sidewalk));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.sidewalk));
                        category = categoryNames[0];
                        categoryGlobal = "SIDEWALK";
                        break;
                    case ROAD:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.road));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.road));
                        category = categoryNames[1];
                        categoryGlobal = "ROAD";

                        break;
                    case PARK:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.park));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.park));
                        category = categoryNames[2];
                        categoryGlobal = "PARK";
                        break;
                    case LIGHTS:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.lights));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.lights));
                        category = categoryNames[3];
                        categoryGlobal = "LIGHTS";

                        break;
                    case TRAFFIC_LIGHTS:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.traffic_lights));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.traffic_lights));
                        category = categoryNames[4];
                        categoryGlobal = "TRAFFIC_LIGHTS";

                        break;
                    case CLEANING:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.cleaning));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.cleaning));
                        category = categoryNames[5];
                        categoryGlobal = "CLEANING";

                        break;
                    case CONTAINER:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.container));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.container));
                        category = categoryNames[6];
                        categoryGlobal = "CONTAINER";

                        break;
                    case TREE:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.tree));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.tree));
                        category = categoryNames[7];
                        categoryGlobal = "TREE";
                        break;
                    case OTHER:
                        mIcon.setImageDrawable(getResources().getDrawable(R.drawable.other));
                        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.other));
                        category = categoryNames[8];
                        categoryGlobal = "OTHER";
                        break;
                    default:
                        break;
                }

                mCategory.setText(category + " (" + priority + ")");
                mLocation.setText(DomainController.writeLocation(DomainController.getLocationName(incidence.getLocation().latitude, incidence.getLocation().longitude, getBaseContext())));

                latitude = incidence.getLocation().latitude;
                longitude = incidence.getLocation().longitude;

                LatLng incidencePosition = new LatLng(latitude, longitude);


                Bitmap markerCategory = DomainController.getCategoryIcon(Incidence.Category.valueOf(categoryGlobal),getResources());
                Bitmap markerPriority = DomainController.getPriorityIcon(priorityGlobal,getResources());
                incidenceMarker = DomainController.overlay(markerPriority, markerCategory);
                mMapPreview.addMarker(new MarkerOptions().position(incidencePosition)
                        .icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();

                mMapPreview.animateCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition,15));



                final String userId = incidence.getUserId();

                mConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!confirmed) {
                            DomainController.confirmIncidentia(id_incidence,userId, new ConfirmIncidenceManager.TaskConfirmIncidenceListener() {

                                @Override
                                public void onConfirmIncidentia() {

                                    mNumConfirmations.setText(String.valueOf(Integer.valueOf(mNumConfirmations.getText().toString())+1));
                                    mConfirm.setImageDrawable(getResources().getDrawable(R.drawable.like_green));
                                    confirmed = true;

                                    if (Integer.valueOf(mNumConfirmations.getText().toString())==1) mConfirmation.setText(R.string.confirmation);
                                    else mConfirmation.setText(R.string.confirmations);

                                }

                                @Override
                                public void onDoesNotConfirmIncidentia() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_confirm, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNotConnection() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        else {
                            DomainController.unconfirmIncidentia(id_incidence,userId, new UnconfirmIncidenceManager.TaskUnconfirmIncidenceListener() {
                                @Override
                                public void onUnconfirmIncidentia() {
                                    mNumConfirmations.setText(String.valueOf(Integer.valueOf(mNumConfirmations.getText().toString())-1));
                                    mConfirm.setImageDrawable(getResources().getDrawable(R.drawable.like_dark));
                                    confirmed = false;
                                    if (Integer.valueOf(mNumConfirmations.getText().toString())==1) mConfirmation.setText(R.string.confirmation);
                                    else mConfirmation.setText(R.string.confirmations);

                                }

                                @Override
                                public void onDoesNotUnconfirmIncidentia() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_unconfirm, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNotConnection() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });

            }

            @Override
            public void onDoesNotGetIncidence() {
                Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_get, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNotConnection() {
                Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
            }
        });

        mUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShowIncidenceActivity.this, ShowProfileActivity.class);
                intent.putExtra("mailToShow", mail_user);
                startActivity(intent);
                //finish();
            }
        });

        mAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mText.getText().length()>0) {
                    DomainController.createComment(CurrentUser.getInstance().getUser().getMail(), id_incidence, mText.getText().toString(), new CreateCommentManager.TaskCreateCommentListener() {
                        @Override
                        public void onCreateComment() {
                            mText.setText("");
                            DomainController.getAllCommentsById(id_incidence, new GetAllCommentsByIdManager.TaskGetAllCommentsByIdListener() {
                                @Override
                                public void onGetAllCommentsById(List<Comment> listComment) {
                                    mAdapter = new CommentAdapter(listComment,showIncidenceActivity);
                                    mRecyclerView.setAdapter(mAdapter);
                                }

                                @Override
                                public void onDoesNotGetAllCommentsById() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_get, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNotConnection() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                        @Override
                        public void onDoesNotCreateComment() {
                            Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_created, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNotConnection() {
                            Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        DomainController.getAllCommentsById(id_incidence, new GetAllCommentsByIdManager.TaskGetAllCommentsByIdListener() {
            @Override
            public void onGetAllCommentsById(List<Comment> listComment) {
                mAdapter = new CommentAdapter(listComment,showIncidenceActivity);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onDoesNotGetAllCommentsById() {
                Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_get, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNotConnection() {
                Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
            }
        });
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (from != null && from.equals("map")) {
            Intent intent = new Intent(ShowIncidenceActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(ShowIncidenceActivity.this, ShowProfileActivity.class);
            intent.putExtra("mailToShow", mail_user);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.show_incidence_menu, menu);


        if (incidenceReported==1 || incidenceReported==2) {

            if (mail_user.equals(CurrentUser.getInstance().getUser().getMail())) {
                menu.findItem(R.id.action_options_delete).setVisible(true);
                if (incidenceInvalid == 2 && incidenceFixed == 2) {
                    menu.findItem(R.id.action_options_edit).setVisible(true);
                }
            } else {
                if (incidenceReported==2) {
                    if (!(CurrentUser.getInstance().getUser().getMail().equals(getString(R.string.guest_user_mail)))) {
                        if (incidenceInvalid == 2 && incidenceFixed == 2) {
                            menu.findItem(R.id.action_options_report).setVisible(true);
                        }
                    }
                }
            }
        }

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_options_delete:

                AlertDialog.Builder mBuilderDelete = new AlertDialog.Builder(ShowIncidenceActivity.this);
                mBuilderDelete.setMessage(R.string.action_confirm_delete_incidence);
                mBuilderDelete.setTitle(R.string.action_confirm_delete_title);
                mBuilderDelete.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DomainController.deleteIncidence(id_incidence, Integer.valueOf(mNumConfirmations.getText().toString()), new DeleteIncidenceManager.TaskDeleteIncidenceListener() {
                            @Override
                            public void onDeleteIncidence() {

                                CurrentUser.getInstance().deleteIncidence(id_incidence);
                                Toast.makeText(ShowIncidenceActivity.this, R.string.action_deleted_incidence, Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }

                            @Override
                            public void onDoesNotDeleteIncidence() {
                                Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_deleted, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNotConnection() {
                                Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                });
                mBuilderDelete.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                mBuilderDelete.create().show();

                return true;

            case R.id.action_options_report:

                AlertDialog.Builder mBuilderReport = new AlertDialog.Builder(ShowIncidenceActivity.this);
                mBuilderReport.setMessage(R.string.action_confirm_report_incidence);
                mBuilderReport.setTitle(R.string.action_report_incidence);
                mBuilderReport.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        DomainController.reportIncidentia(id_incidence, new ReportIncidenceManager.TaskReportIncidenceListener() {
                            @Override
                            public void onReportIncidentia() {
                                Menu menu =  mToolbar.getMenu();
                                if (!(menu.findItem(R.id.action_options_report) == null)) {
                                    menu.findItem(R.id.action_options_report).setVisible(false);
                                }
                            }

                            @Override
                            public void onDoesNotReportIncidentia() {
                                Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_reported, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNotConnection() {
                                Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });
                mBuilderReport.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                mBuilderReport.create().show();
                return true;

            case R.id.action_options_edit:
                Intent intent = new Intent(ShowIncidenceActivity.this, IncidenceFormActivity.class);
                intent.putExtra(getString(R.string.open_camera), false);
                intent.putExtra(getString(R.string.form_type), IncidenceFormActivity.Type.EDIT);
                intent.putExtra("incidenceToEdit", id_incidence);
                intent.putExtra("from", from);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void clickOnComment(final Comment comment){
        if (comment.getId_user().equals(CurrentUser.getInstance().getUser().getMail())) {
            AlertDialog.Builder mBuilderReport = new AlertDialog.Builder(ShowIncidenceActivity.this);
            mBuilderReport.setMessage(R.string.action_confirm_delete_comment);
            mBuilderReport.setTitle(R.string.action_delete_comment);
            mBuilderReport.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    DomainController.deleteComment(comment.getId(), new DeleteCommentManager.TaskDeleteCommentListener() {
                        @Override
                        public void onDeleteComment() {

                            DomainController.getAllCommentsById(id_incidence, new GetAllCommentsByIdManager.TaskGetAllCommentsByIdListener() {
                                @Override
                                public void onGetAllCommentsById(List<Comment> listComment) {
                                    mAdapter = new CommentAdapter(listComment,showIncidenceActivity);
                                    mRecyclerView.setAdapter(mAdapter);
                                }

                                @Override
                                public void onDoesNotGetAllCommentsById() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_get, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNotConnection() {
                                    Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                                }
                            });

                             }

                        @Override
                        public void onDoesNotDeleteComment() {
                            Toast.makeText(ShowIncidenceActivity.this, R.string.error_not_deleted, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNotConnection() {
                            Toast.makeText(ShowIncidenceActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            });
            mBuilderReport.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
            mBuilderReport.create().show();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapPreview = googleMap;
        mMapPreview.getUiSettings().setScrollGesturesEnabled(false);

        mMapPreview.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.overlay_fragment2_container, new SelectLocationFragment(), "Map");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    public static class SelectLocationFragment extends Fragment implements OnMapReadyCallback {

        public SelectLocationFragment() {}

        private static final String LATITUDE = "latitude";
        private static final String LONGITUDE = "longitude";


        MapView mMapView;
        private GoogleMap mMap;

        public static SelectLocationFragment newInstance(double latitude, double longitude) {
            SelectLocationFragment fragment = new SelectLocationFragment();
            Bundle args = new Bundle();
            args.putDouble(LATITUDE, latitude);
            args.putDouble(LONGITUDE, longitude);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View mView=  inflater.inflate(R.layout.fragment_map, container, false);

            View background = mView.findViewById(R.id.background);
            background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            mView.findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            });


            mMapView = (MapView) mView.findViewById(R.id.mapView);
            mMapView.onCreate(savedInstanceState);

            mMapView.onResume(); // needed to get the map to display immediately

            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            mMapView.getMapAsync(this);
            return mView;
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;

            LatLng incidencePosition = new LatLng(latitude, longitude);

            Bitmap markerCategory = DomainController.getCategoryIcon(Incidence.Category.valueOf(categoryGlobal),getResources());
            Bitmap markerPriority = DomainController.getPriorityIcon(priorityGlobal,getResources());
            incidenceMarker = DomainController.overlay(markerPriority, markerCategory);
            mMap.addMarker(new MarkerOptions().position(incidencePosition)
                    .icon(BitmapDescriptorFactory.fromBitmap(incidenceMarker))).showInfoWindow();

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(incidencePosition,15));


        }

        @Override
        public void onResume() {
            super.onResume();
            mMapView.onResume();
        }

        @Override
        public void onPause() {
            super.onPause();
            mMapView.onPause();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mMapView.onDestroy();
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            mMapView.onLowMemory();
        }

    }
}
