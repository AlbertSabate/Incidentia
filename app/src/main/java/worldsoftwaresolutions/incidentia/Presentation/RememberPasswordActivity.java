package worldsoftwaresolutions.incidentia.Presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.RememberPasswordUserManager;
import worldsoftwaresolutions.incidentia.Exceptions.MailIsNotValidException;
import worldsoftwaresolutions.incidentia.R;

public class RememberPasswordActivity extends AppCompatActivity {

    Button buttonSendMail;
    EditText editTextEmail;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remember_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        String loginmail = getIntent().getStringExtra("Mail");
        toolbar.setLogo(null);
        toolbar.setTitle(getString(R.string.title_remember_password));
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        buttonSendMail = (Button) findViewById(R.id.buttonSendMail);
        editTextEmail = (EditText) findViewById(R.id.editTextRememberPassword);
        editTextEmail.setText(loginmail);
        buttonSendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean cancel = false;
                View focusView = null;
                String email = editTextEmail.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    editTextEmail.setError(getString(R.string.error_field_required));
                    focusView = editTextEmail;
                    cancel = true;
                }
                if (cancel) focusView.requestFocus();
                else {
                    enviarCorreu(email);
                }
            }

            private void enviarCorreu(String mail) {
                try {
                    DomainController.rememberPasswordUser(mail, new RememberPasswordUserManager.TaskRememberPasswordUserListener() {
                        @Override
                        public void onRememberPasswordUser() {
                            Toast.makeText(getApplicationContext(),getString(R.string.remember_user), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RememberPasswordActivity.this, LogInActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onUserNotExists() {
                            editTextEmail.setError(getString(R.string.error_do_not_exists));
                            editTextEmail.requestFocus();
                        }

                        @Override
                        public void onGoogleUser() {
                            editTextEmail.setError(getString(R.string.remember_password_is_google_user));
                            editTextEmail.requestFocus();
                        }

                        @Override
                        public void onNotConnection() {
                            Toast.makeText(getApplicationContext(), getString(R.string.create_failed_connection), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void otherError() {
                            editTextEmail.setError(getString(R.string.strange_error));
                            editTextEmail.requestFocus();
                        }
                    });
                } catch (MailIsNotValidException e) {
                    editTextEmail.setError(getString(R.string.error_invalid_email));
                    editTextEmail.requestFocus();
                }
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RememberPasswordActivity.this, LogInActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }




}
