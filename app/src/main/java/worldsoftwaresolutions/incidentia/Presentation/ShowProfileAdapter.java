package worldsoftwaresolutions.incidentia.Presentation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.R;

public class ShowProfileAdapter extends RecyclerView.Adapter<ShowProfileAdapter.IncidentiaViewHolder> {

    private ArrayList<String> ids;
    private ShowProfileActivity showProfileActivity;

    public class IncidentiaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ArrayList<String> ids;
        private ShowProfileActivity showProfileActivity;
        public ImageView image,icon, icon2;

        public IncidentiaViewHolder(View view,ArrayList<String> ids,ShowProfileActivity showProfileActivity) {
            super(view);
            this.ids=ids;
            this.showProfileActivity=showProfileActivity;

            view.setOnClickListener(this);
            image = (ImageView) view.findViewById(R.id.imageIncidentiaShowProfile);
            icon = (ImageView) view.findViewById(R.id.iconIncidentiaShowProfile);
            icon2 = (ImageView) view.findViewById(R.id.icon2IncidentiaShowProfile);
        }

        public void bindIncidence (Incidence inc, int position) {

            if (CurrentUser.getInstance().getUser() != null && CurrentUser.getInstance().getUser().getMail().equals(showProfileActivity.shownMail)) {
                image.setImageBitmap(DomainController.getSquareThumbnail(inc.getPicture()));

            } else {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/incidences/");
                StorageReference file = storageRef.child(inc.getId()+".jpeg");

                final long ONE_MEGABYTE = 1024 * 1024;
                file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        Bitmap incidencePicture = BitmapFactory.decodeByteArray(bytes , 0, bytes.length);
                        image.setImageBitmap(DomainController.getSquareThumbnail(incidencePicture));
                        System.out.println(incidencePicture.getByteCount());

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        image.setImageDrawable(itemView.getResources().getDrawable(R.drawable.default_picture));
                        System.out.println("Ha fallat");
                    }
                });
            }

            if (inc.isInvalid()) {
                icon2.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_reported));
            } else if (inc.isFixed()) {
                icon2.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_fixed));
            }



            switch (inc.getPriority()){
                case LOW:
                    icon.setBackgroundDrawable(itemView.getResources().getDrawable(R.drawable.low_priority));
                    break;
                case MID:
                    icon.setBackgroundDrawable(itemView.getResources().getDrawable(R.drawable.mid_priority));
                    break;
                case HIGH:
                    icon.setBackgroundDrawable(itemView.getResources().getDrawable(R.drawable.high_priority));
                    break;
                default:
                    break;
            }

            switch (inc.getCategory()){
                case SIDEWALK:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.sidewalk));
                    break;
                case ROAD:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.road));
                    break;
                case PARK:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.park));
                    break;
                case LIGHTS:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.lights));
                    break;
                case TRAFFIC_LIGHTS:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.traffic_lights));
                    break;
                case CLEANING:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.cleaning));
                    break;
                case CONTAINER:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.container));
                    break;
                case TREE:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.tree));
                    break;
                case OTHER:
                    icon.setImageDrawable(itemView.getResources().getDrawable(R.drawable.other));
                    break;
                default:
                    break;
            }

        }

        @Override
        public void onClick(View view) {
            showProfileActivity.showIncidence(this.ids.get(getPosition()));
        }
    }


    public ShowProfileAdapter(ArrayList<String> ids, ShowProfileActivity showProfileActivity) {
        this.ids = ids;
        this.showProfileActivity=showProfileActivity;
    }
    public void setIds(ArrayList<String> ids){
        this.ids = ids;
    }

    @Override
    public IncidentiaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_incidentia, parent, false);

        return new IncidentiaViewHolder(itemView,this.ids,showProfileActivity);
    }

    @Override
    public void onBindViewHolder(IncidentiaViewHolder holder, int position) {
        callBind(holder,position);
    }



    public void callBind(final IncidentiaViewHolder holder, final int position){

        if (CurrentUser.getInstance().getUser() != null && CurrentUser.getInstance().getUser().getMail().equals(showProfileActivity.shownMail)) {
            Incidence incidence = CurrentUser.getInstance().getIncidenceById(ids.get(position));
            holder.bindIncidence(incidence,position);
        } else {
            DomainController.getIncidence(ids.get(position), new GetIncidenceManager.TaskGetIncidenceListener() {
                @Override
                public void onGetIncidence(Incidence incidence) {
                    holder.bindIncidence(incidence,position);
                }

                @Override
                public void onDoesNotGetIncidence() {
                    Toast.makeText(showProfileActivity, R.string.error_not_get, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNotConnection() {
                    Toast.makeText(showProfileActivity, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return this.ids.size();
    }
}
