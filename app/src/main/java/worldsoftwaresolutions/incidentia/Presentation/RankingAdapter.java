package worldsoftwaresolutions.incidentia.Presentation;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.R;

public class RankingAdapter extends RecyclerView.Adapter<RankingAdapter.RankingViewHolder> {

    private ArrayList<User> users;
    private ShowProfileActivity showProfileActivity;
    private RankingActivity rankingActivity;

    public class RankingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ArrayList<User> users;
        public TextView pos,name,punt;
        public CardView cardView;

        public RankingViewHolder(View view,ArrayList<User> users,ShowProfileActivity showProfileActivity) {
            super(view);
            Collections.sort(users, new Comparator<User>(){
                public int compare(User obj1, User obj2) {
                    return Integer.valueOf(obj2.getScore()).compareTo(obj1.getScore());
                }
            });
            this.users = users;
            view.setOnClickListener(this);
            pos = (TextView) view.findViewById(R.id.positionRank);
            name = (TextView) view.findViewById(R.id.nameRank);
            punt = (TextView) view.findViewById(R.id.pointsRank);
            cardView = (CardView) view.findViewById(R.id.cardViewRank);
        }
        public RankingViewHolder(View view,ArrayList<User> users,RankingActivity rankingActivity) {
            super(view);
            Collections.sort(users, new Comparator<User>(){
                public int compare(User obj1, User obj2) {
                    return Integer.valueOf(obj2.getScore()).compareTo(obj1.getScore());
                }
            });
            this.users = users;
            view.setOnClickListener(this);
            pos = (TextView) view.findViewById(R.id.positionRank);
            name = (TextView) view.findViewById(R.id.nameRank);
            punt = (TextView) view.findViewById(R.id.pointsRank);
            cardView = (CardView) view.findViewById(R.id.cardViewRank);
        }

        public void bindRanking (User us, int position) {
            if(position<3) {
                cardView.setCardBackgroundColor(itemView.getResources().getColor(R.color.gold));
                pos.setTextSize(pxFromDp(12, itemView.getContext()));
            }
            pos.setText(Integer.toString(position+1));
            name.setText(us.getName()+" "+us.getSurname().charAt(0)+".");
            punt.setText(String.valueOf(us.getScore()));
        }

        public float pxFromDp(float dp, Context mContext) {
            return dp * mContext.getResources().getDisplayMetrics().density;
        }

        @Override
        public void onClick(View view) {
            if (showProfileActivity != null) showProfileActivity.showUser(this.users.get(getPosition()).getMail());
            else {
                rankingActivity.showUser(this.users.get(getPosition()).getMail());
            }
        }
    }


    public RankingAdapter(ArrayList<User> users, ShowProfileActivity showProfileActivity) {
        this.users = users;
        this.showProfileActivity=showProfileActivity;
    }

    public RankingAdapter(ArrayList<User> users, RankingActivity rankingActivity) {
        this.users = users;
        this.rankingActivity=rankingActivity;
    }
    public void setUsers(ArrayList<User> users){
        this.users = users;
    }

    @Override
    public RankingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ranking, parent, false);

        if (showProfileActivity != null) return new RankingViewHolder(itemView,this.users,showProfileActivity);
        else return new RankingViewHolder(itemView,this.users,rankingActivity);
    }

    @Override
    public void onBindViewHolder(RankingViewHolder holder, int position) {
        callBind(holder,position);
    }



    public void callBind(final RankingViewHolder holder, final int position){
        holder.bindRanking(users.get(position),position);

    }


    @Override
    public int getItemCount() {
        return this.users.size();
    }
}