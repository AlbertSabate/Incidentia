package worldsoftwaresolutions.incidentia.Presentation;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.Date;
import worldsoftwaresolutions.incidentia.Domain.DeleteUserManager;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.EditPasswordUserManager;
import worldsoftwaresolutions.incidentia.Domain.EditUserManager;
import worldsoftwaresolutions.incidentia.Domain.GetUserManager;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;
import worldsoftwaresolutions.incidentia.Exceptions.PasswordIsNotValidException;
import worldsoftwaresolutions.incidentia.R;


public class EditUserActivity extends AppCompatActivity {
    private Date dateBirth;
    private int day, month, year;
    double longitude, latitude;
    private User user;
    private static final int typeDialog = 0;
    private static DatePickerDialog.OnDateSetListener datePickerDialog;

    private Toolbar mToolbar;

    //keep track of camera capture intent
    final int REQUEST_IMAGE_CAPTURE = 1;
    //keep track of pick image intent
    final int REQUEST_IMAGE_PICK = 2;
    private boolean imageTools;
    private boolean defaultImage;
    private Bitmap userPhoto;
    private Bitmap imageBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setLogo(null);
        mToolbar.setTitle(R.string.title_activity_edit_user);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);
        this.mToolbar = mToolbar;



        final EditText mName = (EditText) findViewById(R.id.editTextName);
        final EditText mSurname = (EditText) findViewById(R.id.editTextSurname);
        final EditText mDate = (EditText) findViewById(R.id.date);
        final EditText mLocation = (EditText) findViewById(R.id.location);
        final TextView mPassword = (TextView) findViewById(R.id.password);
        final TextView mTextChangePassword = (TextView) findViewById(R.id.changePassword);
        final TextView mDeleteUser = (TextView) findViewById(R.id.deleteUser);

        user = CurrentUser.getInstance().getUser();

        mName.setText(user.getName());
        mSurname.setText(user.getSurname());
        dateBirth = user.getBirth();
        day = dateBirth.getDay();
        month = dateBirth.getMonth();
        year = dateBirth.getYear();
        userPhoto=user.getPicture();
        mDate.setText(day + "/" + month + "/" + year);
        mPassword.setText(user.getPassword());
        changePhoto(userPhoto);

        Drawable originalDrawable = getResources().getDrawable(R.drawable.user_default);
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();

        imageTools = false;
        defaultImage = userPhoto.equals(originalBitmap);


        if(user.getIsGoogleUser()) {
            mPassword.setVisibility(View.GONE);
            mTextChangePassword.setVisibility(View.GONE);
        }
        updateLocation();

        //extraemos el drawable en un bitmap
        //Drawable originalDrawable = getResources().getDrawable(R.drawable.user_default);
        //Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();



        final ImageView mImageView = (ImageView) findViewById(R.id.photoUser);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageTools(true);
            }
        });

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditUserActivity.this, ShowProfileActivity.class);
                intent.putExtra("mailToShow", CurrentUser.getInstance().getUser().getMail());
                startActivity(intent);
                finish();
            }
        });


        datePickerDialog = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int newyear, int newmonth, int newday) {
                day = newday;
                month = newmonth+1;
                year = newyear;
                try {
                    dateBirth = new Date (day,month,year);
                } catch (InvalidDateException e) {
                    e.printStackTrace();
                }
                mDate.setText(day + "/" + month + "/" + year);
            }
        };
        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendary(v);
            }
        });
        mLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(EditUserActivity.this, LocationMapActivity.class);
                    intent.putExtra("Longitude", longitude);
                    intent.putExtra("Latitude", latitude);
                    startActivity(intent);
            }
        });
        mPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(EditUserActivity.this);
                builder.setTitle(R.string.prompt_change_password);

                final EditText mNewPassword = new EditText(EditUserActivity.this);
                final EditText mConfirmPassword = new EditText(EditUserActivity.this);
                mNewPassword.setHint(R.string.prompt_new_password);
                mConfirmPassword.setHint(R.string.prompt_repeat_password);
                mNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                mConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                LinearLayout mLl= new LinearLayout(EditUserActivity.this);
                mLl.setOrientation(LinearLayout.VERTICAL);
                mLl.addView(mNewPassword);
                mLl.addView(mConfirmPassword);
                builder.setView(mLl);

                builder.setNegativeButton(R.string.cancel, null);
                builder.setPositiveButton(R.string.confirm,
                        new  DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {

                            }
                        });

                final AlertDialog mAlertDialog = builder.create();
                mAlertDialog.show();

                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        mNewPassword.setError(null);
                        mConfirmPassword.setError(null);

                        if (mNewPassword.getText().toString().isEmpty()) {
                            mNewPassword.setError(getString(R.string.error_field_required));
                            mNewPassword.requestFocus();
                        }
                        else  if (mConfirmPassword.getText().toString().isEmpty()) {
                            mConfirmPassword.setError(getString(R.string.error_field_required));
                            mConfirmPassword.requestFocus();
                        }
                        else if (!(mNewPassword.getText().toString().equals(mConfirmPassword.getText().toString()))) {
                            mNewPassword.setError(getString(R.string.error_password_match));
                            mNewPassword.requestFocus();
                        }
                        else {
                            try {
                                DomainController.editPasswordUser(user.getMail(),mNewPassword.getText().toString(),new EditPasswordUserManager.TaskEditPasswordUserListener() {
                                    @Override
                                    public void onEditPasswordUser() {
                                        User auxUser=CurrentUser.getInstance().getUser();
                                        auxUser.setPassword(mNewPassword.getText().toString());
                                        CurrentUser.getInstance().setUser(auxUser);
                                        DomainController.saveAutomaticLogInPassword(getApplicationContext(), auxUser.getPassword());
                                        mPassword.setText(mNewPassword.getText().toString());
                                        Toast.makeText(getApplicationContext(), R.string.save, Toast.LENGTH_SHORT).show();
                                        mAlertDialog.dismiss();
                                    }

                                    @Override
                                    public void onDoesNotEditPasswordUser() {
                                        Toast.makeText(getApplicationContext(), R.string.error_not_edit, Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onNotConnection() {
                                        Toast.makeText(getApplicationContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (PasswordIsNotValidException e) {
                                mNewPassword.setError(getString(R.string.error_invalid_password));
                                mNewPassword.requestFocus();
                            }

                        }
                    }
                });
            }

        });
        mDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilderDelete = new AlertDialog.Builder(EditUserActivity.this);
                mBuilderDelete.setMessage(R.string.action_confirm_delete);
                mBuilderDelete.setTitle(R.string.action_confirm_delete_title);
                mBuilderDelete.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        System.out.println("0");
                        DomainController.deleteUser(user.getMail(),getBaseContext(), new DeleteUserManager.TaskDeleteUserListener() {
                            @Override
                            public void onDeleteUser() {
                                System.out.println("1");
                                CurrentUser.getInstance().deleteUser();
                                System.out.println("2");
                                Toast.makeText(EditUserActivity.this, R.string.action_deleted_user, Toast.LENGTH_SHORT).show();
                                System.out.println("3");
                                Intent intent = new Intent(EditUserActivity.this, LogInActivity.class);
                                startActivity(intent);
                                System.out.println("4");
                                finish();
                            }

                            @Override
                            public void onDoesNotDeleteUser() {
                                Toast.makeText(EditUserActivity.this, R.string.error_not_deleted, Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onNotConnection() {
                                Toast.makeText(EditUserActivity.this, R.string.error_failed_connection, Toast.LENGTH_SHORT).show();

                            }
                        });

                    }
                });
                mBuilderDelete.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                mBuilderDelete.create().show();
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new DatePickerDialog(EditUserActivity.this,datePickerDialog, year, month-1, day);

        }
        return null;
    }
    public void showCalendary (View control) {
        showDialog(typeDialog);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_user_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                action(R.string.save);
                return true;
            case R.id.action_takePicture:
                dispatchTakePictureIntent();
                return true;
            case R.id.action_uploadPicture:
                dispatchUploadPictureIntent();
                return true;
            case R.id.action_deletePicture:
                AlertDialog.Builder builder = new AlertDialog.Builder(EditUserActivity.this);
                builder.setMessage(R.string.alert_delete_picture);
                builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        resetPicture();
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showImageTools(false);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void dispatchUploadPictureIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_IMAGE_PICK);
    }

    private void resetPicture() {
        Drawable originalDrawable = getResources().getDrawable(R.drawable.user_default);
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();
        changePhoto(originalBitmap);
        showImageTools(false);
        defaultImage = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                changePhoto(imageBitmap);
            }
            else if (requestCode == REQUEST_IMAGE_PICK && data != null && data.getData() != null) {
                Uri uri = data.getData();
                try {
                    Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    changePhoto(imageBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            showImageTools(false);
            defaultImage = false;
        }
    }

    private void changePhoto(Bitmap photo) {
        userPhoto = photo;
        //creamos el drawable redondeado
        RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), DomainController.getSquareThumbnail(userPhoto));
        //asignamos el CornerRadius
        roundedDrawable.setCornerRadius(DomainController.getSquareThumbnail(userPhoto).getHeight());

        ImageView imageView = (ImageView) findViewById(R.id.photoUser);
        imageView.setImageDrawable(roundedDrawable);
    }


    private void action(int resid) {
        CurrentUser currentUser = CurrentUser.getInstance();

        final EditText mName = (EditText) findViewById(R.id.editTextName);
        final EditText mSurname = (EditText) findViewById(R.id.editTextSurname);
        mName.setError(null);
        mSurname.setError(null);
        String newName = mName.getText().toString();
        String newSurname = mSurname.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(newName)) {
            mName.setError(getString(R.string.error_field_required));
            focusView = mName;
            cancel = true;
        }
        else if (TextUtils.isEmpty(newSurname)) {
            mSurname.setError(getString(R.string.error_field_required));
            focusView = mSurname;
            cancel = true;
        }

        if (cancel) focusView.requestFocus();
        else {
            DomainController.editUser(newName, newSurname, currentUser.getUser().getMail(), userPhoto, dateBirth, new EditUserManager.TaskEditUserListener() {
                @Override
                public void onEditUser() {
                    final CurrentUser currentUser = CurrentUser.getInstance();

                    DomainController.getUser(currentUser.getUser().getMail(), new GetUserManager.TaskGetUserListener() {
                        @Override
                        public void onGetUser(User user) {
                            user.setPicture(userPhoto);
                            currentUser.setUser(user);
                            onUpdate();
                            Toast.makeText(getApplicationContext(), R.string.save, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onDoesNotGetUser() {
                            Toast.makeText(getApplicationContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onNotConnection() {
                            Toast.makeText(getApplicationContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();

                        }
                    });

                }

                @Override
                public void onDoesNotEditUser() {
                    Toast.makeText(getApplicationContext(), R.string.error_not_edit, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onNotConnection() {
                    Toast.makeText(getApplicationContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();

                }
            });
        }


    }



    public void onUpdate() {
        Intent intent = new Intent(EditUserActivity.this, ShowProfileActivity.class);
        intent.putExtra("mailToShow", CurrentUser.getInstance().getUser().getMail());
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (imageTools) {
            showImageTools(false);
        }
        else {
            Intent intent = new Intent(EditUserActivity.this, ShowProfileActivity.class);
            intent.putExtra("mailToShow", CurrentUser.getInstance().getUser().getMail());
            startActivity(intent);
            finish();
        }

    }

    private void showImageTools(boolean activate) {
        if (activate) {
            imageTools = true;
            Menu menu = mToolbar.getMenu();
            if(defaultImage) menu.findItem(R.id.action_takePicture).setVisible(true);
            if(defaultImage) menu.findItem(R.id.action_uploadPicture).setVisible(true);
            if(!defaultImage) menu.findItem(R.id.action_deletePicture).setVisible(true);
            menu.findItem(R.id.action_save).setVisible(false);
            mToolbar.setLogo(null);
            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mToolbar.setNavigationIcon(R.drawable.ic_back);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showImageTools(false);
                }
            });
        }
        else {
            imageTools = false;
            Menu menu = mToolbar.getMenu();
            menu.findItem(R.id.action_takePicture).setVisible(false);
            menu.findItem(R.id.action_uploadPicture).setVisible(false);
            menu.findItem(R.id.action_deletePicture).setVisible(false);
            menu.findItem(R.id.action_save).setVisible(true);
            mToolbar.setLogo(null);
            mToolbar.setTitle(R.string.title_activity_edit_user);
            mToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mToolbar.setNavigationIcon(R.drawable.ic_back);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        updateLocation();
    }

    private void updateLocation() {
        user = CurrentUser.getInstance().getUser();
        final EditText mLocation = (EditText) findViewById(R.id.location);
        if (user.getLongitude() != 181.000000 && user.getLatitude() != 91.000000) {
            longitude = user.getLongitude();
            latitude = user.getLatitude();
            mLocation.setText(DomainController.writeLocation(DomainController.getLocationName(latitude, longitude, getBaseContext())));
        } else {
            longitude = 181.000000;
            latitude = 91.000000;
            mLocation.setText(R.string.prompt_unknown);
        }
    }

}
