package worldsoftwaresolutions.incidentia.Presentation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.LogInUserGoogleManager;
import worldsoftwaresolutions.incidentia.Domain.LogInUserManager;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.Exceptions.MailIsNotValidException;
import worldsoftwaresolutions.incidentia.Exceptions.PasswordIsNotValidException;
import worldsoftwaresolutions.incidentia.R;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    private SignInButton btnSignIn;
    private Button btnSignOut;
    private Button btnRegister;


    Button buttonLogIn;
    EditText editTextEmail;
    EditText editTextPassword;
    TextView textViewPasswordForrbiden;
    private boolean isGoogleUser = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        btnSignOut = (Button) findViewById(R.id.btn_sign_out);
        btnRegister = (Button) findViewById(R.id.buttonRegister);
        buttonLogIn = (Button) findViewById(R.id.buttonLogIn);
        editTextEmail = (EditText) findViewById(R.id.editTextEmailLogIn);
        editTextPassword = (EditText) findViewById(R.id.editTextPasswordLogIn);
        textViewPasswordForrbiden = (TextView) findViewById(R.id.textViewGetPassword);
        btnRegister.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);
        btnSignOut.setOnClickListener(this);
        View current = getCurrentFocus();
        if (current != null) current.clearFocus();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
        btnSignIn.setScopes(gso.getScopeArray());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


        textViewPasswordForrbiden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogInActivity.this, RememberPasswordActivity.class);
                final String email = editTextEmail.getText().toString();
                intent.putExtra("Mail", email);
                startActivity(intent);
            }
        });
        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean cancel = false;
                View focusView = null;
                final String email = editTextEmail.getText().toString();
                final String password = editTextPassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    editTextEmail.setError(getString(R.string.error_field_required));
                    focusView = editTextEmail;
                    cancel = true;
                }
                else if (TextUtils.isEmpty(password)) {
                    editTextPassword.setError(getString(R.string.error_field_required));
                    focusView = editTextPassword;
                    cancel = true;
                }
                if (cancel) focusView.requestFocus();
                else {
                    try {
                        final ProgressDialog progressDialog = ProgressDialog.show(LogInActivity.this, "", getString(R.string.loading_user), true);
                        DomainController.logInUser(email, password, new LogInUserManager.TaskLogInUserListener() {
                            @Override
                            public void onLogInUser(final User user) {
                                inicialize_current_user(user);
                                DomainController.saveAutomaticLogIn(getApplicationContext(), email, password, false);
                            }

                            @Override
                            public void onPasswordIncorrect() {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),getString(R.string.error_password_match) ,Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onUserNotRegistered() {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), getString(R.string.error_do_not_exists),Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onOtherError() {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),getString(R.string.strange_error),Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onRegisteredWithGoogle() {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), R.string.MailRegisteredWithGmail ,Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNotConnection() {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), getString(R.string.create_failed_connection) , Toast.LENGTH_SHORT).show();

                            }
                        });
                    } catch (PasswordIsNotValidException e) {
                        editTextPassword.setError(getString(R.string.error_invalid_password));
                        editTextPassword.requestFocus();
                    } catch (MailIsNotValidException e) {
                        editTextEmail.setError(getString(R.string.error_invalid_email));
                        editTextEmail.requestFocus();
                    }

                }
            }
        });
    }
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());
            final String emailGoogle = acct.getEmail();
            logInWithGoogle(emailGoogle);
            Log.e(TAG, "email: " + emailGoogle);

           //TODO el que sigui

            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.GONE);
            btnSignOut.setVisibility(View.VISIBLE);
            isGoogleUser = true;
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
            btnSignOut.setVisibility(View.GONE);
            isGoogleUser = false;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_sign_in:
                System.out.println("login, Estic dins el sign in");
                signIn();
                break;

            case R.id.btn_sign_out:
                signOut();
                break;

            case R.id.buttonRegister:
                register();
                break;
        }
    }

    private void register() {
        Intent intent = new Intent(LogInActivity.this, SignUpActivity.class);
        intent.putExtra("isGoogleUser",isGoogleUser);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LogInActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void logInWithGoogle(final String email) {
        //final ProgressDialog progressDialog = ProgressDialog.show(LogInActivity.this, "", getString(R.string.loading_user), true);
        DomainController.logInUserGoogle(email, new LogInUserGoogleManager.TaskLogInUserGoogleListener() {
                @Override
                public void onLogInUserGoogle(final User user) {
                    inicialize_current_user(user);
                    DomainController.saveAutomaticLogIn(getApplicationContext(), email, "", true);
                }

                @Override
                public void onUserRegisteredWithoutGoogle() {
                    //progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), R.string.MailRegisteredWithOutGmail ,Toast.LENGTH_SHORT).show();
                    signOut();
                }


                @Override
                public void onUserNotRegistered() {
                    //progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), getString(R.string.error_do_not_exists),Toast.LENGTH_SHORT).show();
                    register();
                }

                @Override
                public void onOtherError() {
                    //progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),getString(R.string.strange_error),Toast.LENGTH_SHORT).show();
                    signOut();
                }

                @Override
                public void onNotConnection() {
                    //progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), getString(R.string.create_failed_connection) , Toast.LENGTH_SHORT).show();
                    signOut();
                }
            });



    }

    private void inicialize_current_user(final User user) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/users/");
        StorageReference file = storageRef.child(user.getMail() + ".jpeg");
        final long ONE_MEGABYTE = 1024 * 1024;
        file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap userImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                user.setPicture(userImage);
                CurrentUser currentUser = CurrentUser.getInstance();
                currentUser.setUser(user);
                inicialize_current_user_incidence(user);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Bitmap userImage = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.user_default);

                user.setPicture(userImage);
                CurrentUser currentUser = CurrentUser.getInstance();
                currentUser.setUser(user);
                inicialize_current_user_incidence(user);


            }
        });
    }
    private void inicialize_current_user_incidence(final User user) {
        final CurrentUser currentUser = CurrentUser.getInstance();
        currentUser.newIncidence();

        final ArrayList<Incidence> incidences = new ArrayList<>();
        ArrayList<String> ids = user.getIncidences();
        if (ids.size() == 0) goToNextPage(user);
        for (int i=0; i<ids.size();++i) {
            final boolean last = (i == ids.size()-1);
            DomainController.getIncidence(ids.get(i), new GetIncidenceManager.TaskGetIncidenceListener() {
                @Override
                public void onGetIncidence(final Incidence incidence) {

                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/incidences/");
                    StorageReference file = storageRef.child(incidence.getId()+".jpeg");

                    final long ONE_MEGABYTE = 1024 * 1024;
                    file.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            System.out.println("LOGINACTIVITY, BYTE: " + bytes.length);
                            Bitmap incidencePicture = BitmapFactory.decodeByteArray(bytes , 0, bytes.length);
                            incidence.setPicture(DomainController.getSquareThumbnail(incidencePicture));
                            incidences.add(incidence);
                            if (last) {
                                currentUser.setIncidenceArray(incidences);
                                goToNextPage(user);
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Bitmap itemView = BitmapFactory.decodeResource(getBaseContext().getResources(),
                                    R.drawable.default_picture);
                            incidence.setPicture(itemView);
                            incidences.add(incidence);
                            if (last) {
                                currentUser.setIncidenceArray(incidences);
                                goToNextPage(user);
                            }
                        }
                    });

                }

                @Override
                public void onDoesNotGetIncidence() {
                    Toast.makeText(getBaseContext(), R.string.error_not_get, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNotConnection() {
                    Toast.makeText(getBaseContext(), R.string.error_failed_connection, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    private void goToNextPage(User user) {
        Intent intent = new Intent(LogInActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
