package worldsoftwaresolutions.incidentia.Domain;

import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;
import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;

public class CreateIncidenceManager extends AsyncTask<CreateIncidenceManager.TaskCreateIncidenceParams, Integer, CreateIncidenceManager.TaskCreateIncidenceResult> {

    private CreateIncidenceManager.TaskCreateIncidenceListener taskCreateIncidenceListener;

    public CreateIncidenceManager(CreateIncidenceManager.TaskCreateIncidenceListener taskCreateIncidenceListener) {
        this.taskCreateIncidenceListener = taskCreateIncidenceListener;
    }

    protected CreateIncidenceManager.TaskCreateIncidenceResult doInBackground(CreateIncidenceManager.TaskCreateIncidenceParams... params){

        Incidence newIncidence = params[0].getIncidence();
        String result = "";

        result = DataControllerIncidence.createIncidence(newIncidence);

        // Return the result
        if(result.contains("ID:")) {
            String userMail = params[0].getUserMail();
            String incidenceId = result.split(":")[1];
            result = DataControllerUser.addIncidence(userMail,incidenceId);
            newIncidence.setId(incidenceId);
            System.out.println("CREATEINCIDENCEMANAGER, NEWINCIDENCE");
            System.out.println(newIncidence);
            if (result.contains(("true"))) return new CreateIncidenceManager.TaskCreateIncidenceResult(newIncidence, true, result);
            else return new CreateIncidenceManager.TaskCreateIncidenceResult(newIncidence, false,result);
        }
        else return new CreateIncidenceManager.TaskCreateIncidenceResult(newIncidence, false,result);
    }

    protected void onPostExecute(CreateIncidenceManager.TaskCreateIncidenceResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskCreateIncidenceListener.onCreateIncidence(result.getIncidence());
        }
        else if(result.getExcepcio()=="connectFail"){
            taskCreateIncidenceListener.onNotConnection();
        }
        else {
            taskCreateIncidenceListener.onDoesNotCreateIncidence();
        }

    }


    public static class TaskCreateIncidenceParams {


        public Incidence getIncidence() {
            return incidence;
        }

        public void setIncidence(Incidence incidence) {
            this.incidence = incidence;
        }

        private Incidence incidence;

        public String getUserMail() {
            return userMail;
        }

        public void setUserMail(String userMail) {
            this.userMail = userMail;
        }

        private String userMail;

        public TaskCreateIncidenceParams (Bitmap picture, String description, LatLng location, Incidence.Priority priority, String category, int nbConfirmations, String poblation){

            this.incidence = new Incidence(description,picture,location, priority, Incidence.Category.valueOf(category), CurrentUser.getInstance().getUser().getMail(),nbConfirmations, poblation);
            this.userMail = CurrentUser.getInstance().getUser().getMail();
        }
    }

    public static class TaskCreateIncidenceResult {
        private Incidence incidence;
        private boolean success;
        private String excepcio;

        public TaskCreateIncidenceResult(Incidence incidence, boolean success, String excepcio) {
            this.incidence = incidence;
            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

        public Incidence getIncidence() {
            return incidence;
        }
    }

    public interface TaskCreateIncidenceListener{
        void onCreateIncidence(Incidence incidence);

        void onDoesNotCreateIncidence();

        void onNotConnection();
    }
}
