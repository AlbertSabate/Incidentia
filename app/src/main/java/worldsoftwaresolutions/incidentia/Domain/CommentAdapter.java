package worldsoftwaresolutions.incidentia.Domain;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.*;
import java.util.Date;

import worldsoftwaresolutions.incidentia.Presentation.ShowIncidenceActivity;
import worldsoftwaresolutions.incidentia.Presentation.ShowProfileActivity;
import worldsoftwaresolutions.incidentia.R;



public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private List<Comment> mData;
    private ShowIncidenceActivity showIncidenceActivity;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mUser;
        private TextView mText;
        private TextView mDate;
        private CardView mCardView;
        private ShowIncidenceActivity showIncidenceActivity;
        private List<Comment> mData;


        public ViewHolder(View itemView,List<Comment> mData,ShowIncidenceActivity showIncidenceActivity){
            super(itemView);

            this.mData=mData;
            this.showIncidenceActivity=showIncidenceActivity;

            itemView.setOnClickListener(this);

            mUser = (TextView) itemView.findViewById(R.id.userId);
            mText = (TextView) itemView.findViewById(R.id.text_comment);
            mDate = (TextView) itemView.findViewById(R.id.date_comment);
            mCardView = (CardView) itemView.findViewById(R.id.cardViewComment);

        }

        public void bindIncidence (final Comment comment, int position) {

            if(CurrentUser.getInstance().getUser().getMail().equals(comment.getId_user())){
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mCardView.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

                mCardView.setLayoutParams(params);
                mCardView.setCardBackgroundColor(itemView.getResources().getColor(R.color.colorPrimaryLight));
                mUser.setVisibility(View.GONE);
            }
            else {
                mUser.setText(comment.getId_user());
                DomainController.getUser(comment.getId_user(), new GetUserManager.TaskGetUserListener() {
                    @Override
                    public void onGetUser(User user) {
                        mUser.setText(user.getName());
                    }

                    @Override
                    public void onDoesNotGetUser() {

                    }

                    @Override
                    public void onNotConnection() {

                    }
                });
            }
            mText.setText(comment.getText());

            DomainController.getCurrentDate(new GetCurrentDateManager.TaskGetCurrentDateListener() {
                @Override
                public void onGetCurrentDate(Date date) {
                    System.out.println("Data: "+comment.getDate().toString());
                    System.out.println("Actual: "+date.toString());
                    mDate.setText(DomainController.getData(comment.getDate(),date));
                }

                @Override
                public void onDoesNotGetCurrentDate() {
                    System.out.println("no se");
                }

                @Override
                public void onNotConnection() {
                    System.out.println("no conect");
                }
            });

        }

        @Override
        public void onClick(View view) {
            showIncidenceActivity.clickOnComment(mData.get(getPosition()));
        }
    }

    public CommentAdapter(List<Comment> mData, ShowIncidenceActivity showIncidenceActivity){
        this.mData = mData;
        this.showIncidenceActivity=showIncidenceActivity;
    }

    @Override
    public int getItemCount() {
        if(mData != null)
            return mData.size();
        else
            return 0;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.data_comment, viewGroup, false);
        return new ViewHolder(v,mData,showIncidenceActivity);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
//        viewHolder.mUser.setText(mData.get(i).getId_user());
//        viewHolder.mText.setText(mData.get(i).getText());
        viewHolder.bindIncidence(mData.get(i),i);
    }


}
