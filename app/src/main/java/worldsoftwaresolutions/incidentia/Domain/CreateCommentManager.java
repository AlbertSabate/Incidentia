package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerComment;

/**
 * Created by edgar on 4/22/17.
 */

public class CreateCommentManager extends AsyncTask<CreateCommentManager.TaskCreateCommentParams, Integer, CreateCommentManager.TaskCreateCommentResult> {

    private CreateCommentManager.TaskCreateCommentListener taskCreateCommentListener;

    public CreateCommentManager(CreateCommentManager.TaskCreateCommentListener taskCreateCommentListener) {
        this.taskCreateCommentListener = taskCreateCommentListener;
    }

    protected CreateCommentManager.TaskCreateCommentResult doInBackground(CreateCommentManager.TaskCreateCommentParams... params){

        Comment comment = params[0].getComment();

        String result = "";

        result = DataControllerComment.createComment(comment);

        // Return the result
        if(result.contains("true"))return new CreateCommentManager.TaskCreateCommentResult(true, result);
        else return new CreateCommentManager.TaskCreateCommentResult(false,result);
    }

    protected void onPostExecute(CreateCommentManager.TaskCreateCommentResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskCreateCommentListener.onCreateComment();
        }
        else if(result.getExcepcio()=="connectFail"){
            taskCreateCommentListener.onNotConnection();
        }
        else {
            taskCreateCommentListener.onDoesNotCreateComment();
        }

    }


    public static class TaskCreateCommentParams {


        public Comment getComment() {
            return comment;
        }

        public void setComment(Comment comment) {
            this.comment = comment;
        }

        private Comment comment;

        public TaskCreateCommentParams (String idUser, String idIncidence, String text){

            this.comment = new Comment(idUser,idIncidence,text,null);
        }
    }

    public static class TaskCreateCommentResult {

        private boolean success;
        private String excepcio;

        public TaskCreateCommentResult(boolean success, String excepcio) {
            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    public interface TaskCreateCommentListener{
        void onCreateComment();

        void onDoesNotCreateComment();

        void onNotConnection();
    }


}
