package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

/**
 * Created by pauca on 09/05/2017.
 */



public class isGoogleUserManager extends AsyncTask<isGoogleUserManager.TaskisGoogleUserParams, Integer, isGoogleUserManager.TaskisGoogleUserResult> {

    private TaskisGoogleUserListener taskisGoogleUserListener;

    public isGoogleUserManager(TaskisGoogleUserListener taskisGoogleUserListener) {
        this.taskisGoogleUserListener = taskisGoogleUserListener;
    }

    protected TaskisGoogleUserResult doInBackground(TaskisGoogleUserParams... params) {

        String mail = params[0].getMail();

        String result = DataControllerUser.isGoogleUser(mail);

        // Return the result
        if(result.contains("true"))return new TaskisGoogleUserResult(true,null);
        else return new TaskisGoogleUserResult(false,result);
    }

    protected void onPostExecute(TaskisGoogleUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskisGoogleUserListener.onisGoogleUser();
        } else if (result.getMessage().contains("connectFail")) {
            taskisGoogleUserListener.onNotConnection();
        } else {
            taskisGoogleUserListener.onisNotGoogleUser();
        }
    }


    public static class TaskisGoogleUserParams {

        private String mail;

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public TaskisGoogleUserParams(String mail) {
            this.mail = mail;
        }
    }

    public static class TaskisGoogleUserResult {

        private boolean success;
        private String message;

        public TaskisGoogleUserResult(boolean success,String message) {
            this.setSuccess(success);
            this.setMessage(message);
        }

        public boolean isSuccess() {
            return success;
        }
        public String getMessage() { return message; }

        public void setSuccess(boolean success) {
            this.success = success;
        }
        public void setMessage(String message) {
            this.message = message;
        }

    }

    public static interface TaskisGoogleUserListener {
        public void onisGoogleUser();

        public void onisNotGoogleUser();

        public void onNotConnection();
    }
}