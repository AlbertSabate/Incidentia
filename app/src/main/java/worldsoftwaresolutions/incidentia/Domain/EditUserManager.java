package worldsoftwaresolutions.incidentia.Domain;


import android.graphics.Bitmap;
import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;


public class EditUserManager extends AsyncTask<EditUserManager.TaskEditUserParams, Integer, EditUserManager.TaskEditUserResult> {
    private EditUserManager.TaskEditUserListener taskEditUserListener;

    public EditUserManager(EditUserManager.TaskEditUserListener taskEditUserListener) {
        this.taskEditUserListener = taskEditUserListener;
    }

    protected TaskEditUserResult doInBackground(EditUserManager.TaskEditUserParams... params){
        String mail = params[0].getMail();
        String name = params[0].getName();
        String surname = params[0].getSurname();
        Date birth = params[0].getBirth();
        Bitmap photo = params[0].getPhoto();
        String result = DataControllerUser.editUser( name, surname, mail, photo, birth);

        // Return the result

        if(result.contains("true"))return new TaskEditUserResult(true,null);
        else return new TaskEditUserResult(false,result);

    }

    protected void onPostExecute(EditUserManager.TaskEditUserResult result) {

        if (result.isSuccess()) {

            // Call the activity
            taskEditUserListener.onEditUser();
        }
        else if(result.getExcepcio()=="connectFail"){
            taskEditUserListener.onNotConnection();
        }
        else {
            taskEditUserListener.onDoesNotEditUser();
        }

    }


    public static class TaskEditUserParams {
        private String name;
        private String surname;
        private String mail;
        private Date birth;
        private Bitmap photo;


        public Bitmap getPhoto() {
            return photo;
        }

        public void setPhoto(Bitmap photo) {
            this.photo = photo;
        }



        public String getName() {
            return name;
        }
        public String getSurname() {
            return surname;
        }
        public String getMail() {
            return mail;
        }
        public Date getBirth() {
            return birth;
        }

        public void setName(String name) {
            this.name = name;
        }
        public void setSurname(String surname) {
            this.surname = surname;
        }
        public void setMail(String mail) {
            this.mail = mail;
        }
        public void setBirth(Date birth) {
            this.birth = birth;
        }

        public TaskEditUserParams (String name,String surname,String mail, Bitmap photo, Date birth){
            this.name = name;
            this.surname = surname;
            this.mail = mail;
            this.birth = birth;
            this.photo = photo;
        }
    }

    public static class TaskEditUserResult {

        private boolean success;

        private String excepcio;

        public TaskEditUserResult(boolean success, String excepcio) {
            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }


    }

    public interface TaskEditUserListener{
        void onEditUser();

        void onDoesNotEditUser();

        void onNotConnection();
    }



}