package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import org.json.JSONException;

import java.sql.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class GetUsersSortedByScoreManager extends AsyncTask<GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreParams,Integer,GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreResult> {

    private GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreListener GetUsersSortedByScoreListener;

    public GetUsersSortedByScoreManager (GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreListener GetUsersSortedByScoreListener) {

        this.GetUsersSortedByScoreListener = GetUsersSortedByScoreListener;
    }

    @Override
    protected TaskGetUsersSortedByScoreResult doInBackground(TaskGetUsersSortedByScoreParams... params) {

        ArrayList<User> users = new ArrayList<>();

        String result = null;
        try {
            result = DataControllerUser.GetUsersSortedByScore(users);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(result.equals("false") || result.equals("connectFail"))return new GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreResult(users,false, result);
        else return new GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreResult(users,true,result);
    }

    public static class TaskGetUsersSortedByScoreParams {

    }

    public static class TaskGetUsersSortedByScoreResult {

        private ArrayList<User> users;
        private boolean success;
        private String excepcio;

        public TaskGetUsersSortedByScoreResult(ArrayList<User> users,boolean success, String excepcio) {
            this.users = users;
            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public ArrayList<User> getUsers () { return users; }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    protected void onPostExecute(GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreResult result) {

        System.out.print("getUsersSortedByScore: ");

        if (result.isSuccess()) {
            System.out.println("Succed");
            GetUsersSortedByScoreListener.onGetUsersSortedByScore(result.getUsers());
        }
        else if(result.getExcepcio()=="connectFail"){
            System.out.println("Error on Connection");
            GetUsersSortedByScoreListener.onNotConnection();
        }
        else {
            System.out.println("Failed");
            GetUsersSortedByScoreListener.onDoesNotGetUsersSortedByScore();
        }

    }

    public interface TaskGetUsersSortedByScoreListener{

        void onGetUsersSortedByScore(ArrayList<User> users);

        void onDoesNotGetUsersSortedByScore();

        void onNotConnection();
    }
}
