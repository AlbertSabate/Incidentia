package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerComment;

public class GetCurrentDateManager extends AsyncTask<GetCurrentDateManager.TaskGetCurrentDateParams, Integer, GetCurrentDateManager.TaskGetCurrentDateResult> {

    private GetCurrentDateManager.TaskGetCurrentDateListener taskGetCurrentDateListener;

    public GetCurrentDateManager(GetCurrentDateManager.TaskGetCurrentDateListener taskGetCurrentDateListener) {
        this.taskGetCurrentDateListener = taskGetCurrentDateListener;
    }

    protected GetCurrentDateManager.TaskGetCurrentDateResult doInBackground(GetCurrentDateManager.TaskGetCurrentDateParams... params){
        java.util.Date result = DataControllerComment.getCurrentDate();

        // Return the result
        if(result!=null)return new GetCurrentDateManager.TaskGetCurrentDateResult(true, result);
        else return new GetCurrentDateManager.TaskGetCurrentDateResult(false, result);

    }

    protected void onPostExecute(GetCurrentDateManager.TaskGetCurrentDateResult result) {
        if (result.isSuccess()) {

            System.out.println("GetCurrentDateManager data: " + result.getCurrentDate().toString());
            // Call the activity
            taskGetCurrentDateListener.onGetCurrentDate(result.getCurrentDate());
        }
        else {
            taskGetCurrentDateListener.onDoesNotGetCurrentDate();
        }

    }


    public static class TaskGetCurrentDateParams {

        public TaskGetCurrentDateParams (){

        }
    }

    public static class TaskGetCurrentDateResult {
        private java.util.Date date;
        private boolean success;

        public TaskGetCurrentDateResult(boolean success, java.util.Date date) {
            this.setSuccess(success);
            this.date=date;
        }



        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public java.util.Date getCurrentDate() {
            return date;
        }

    }

    public interface TaskGetCurrentDateListener{
        void onGetCurrentDate(java.util.Date date);

        void onDoesNotGetCurrentDate();

        void onNotConnection();
    }


}