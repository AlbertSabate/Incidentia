package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class LogInUserGoogleManager extends AsyncTask<LogInUserGoogleManager.TaskLogInUserGoogleParams, Integer, LogInUserGoogleManager.TaskLogInUserGoogleResult> {

    private LogInUserGoogleManager.TaskLogInUserGoogleListener taskLogInUserGoogleListener;

    public LogInUserGoogleManager(LogInUserGoogleManager.TaskLogInUserGoogleListener taskLogInUserGoogleListener) {
        this.taskLogInUserGoogleListener = taskLogInUserGoogleListener;
    }

    protected LogInUserGoogleManager.TaskLogInUserGoogleResult doInBackground(LogInUserGoogleManager.TaskLogInUserGoogleParams... params){
        String mail = params[0].getMail();
        User user = new User();
        String result = "";

        result = DataControllerUser.logInUserGoogle(user, mail);
        System.out.println(user.getMail());
        // Return the result
        System.out.println("Resulta "+  result);
        if(result.contains("true"))return new LogInUserGoogleManager.TaskLogInUserGoogleResult(true, user, result);
        else return new LogInUserGoogleManager.TaskLogInUserGoogleResult(false, user, result);

    }

    protected void onPostExecute(LogInUserGoogleManager.TaskLogInUserGoogleResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskLogInUserGoogleListener.onLogInUserGoogle(result.LogInUserGoogle());
        }
        else if(result.getExcepcio()=="connectFail"){
            taskLogInUserGoogleListener.onNotConnection();
        }
        else if (result.userNotExistsError()) {
            taskLogInUserGoogleListener.onUserNotRegistered();
        }
        else if (result.userRegisteredWithoutGoogle()) {
            taskLogInUserGoogleListener.onUserRegisteredWithoutGoogle();
        }
            else taskLogInUserGoogleListener.onOtherError();
    }


    public static class TaskLogInUserGoogleParams {

        private String mail;

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }


        public TaskLogInUserGoogleParams (String mail){

            this.mail = mail;
        }
    }

    public static class TaskLogInUserGoogleResult {
        private User user;
        private boolean success;
        private String excepcio;

        public TaskLogInUserGoogleResult(boolean success, User user, String excepcio) {
            this.setSuccess(success);
            this.user=user;
            this.excepcio=excepcio;
        }

        public String getExcepcio() {
            return excepcio;
        }


        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public boolean userRegisteredWithoutGoogle()
        {
            return excepcio.contains("isNotGoogleUser");
        }
        public boolean userNotExistsError(){
            return excepcio.contains("userNotExists");
        }
        public User LogInUserGoogle() {
            return user;
        }

    }

    public interface TaskLogInUserGoogleListener{
        void onLogInUserGoogle(User user);

        void onUserRegisteredWithoutGoogle();

        void onUserNotRegistered();

        void onOtherError();

        void onNotConnection();
    }


}
