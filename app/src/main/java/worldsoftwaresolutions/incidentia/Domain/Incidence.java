package worldsoftwaresolutions.incidentia.Domain;


import android.graphics.Bitmap;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.sql.Array;
import java.util.ArrayList;


public class Incidence {


    public enum Priority {
        LOW,MID,HIGH
    }

    public enum Category {
        SIDEWALK, ROAD, PARK, LIGHTS, TRAFFIC_LIGHTS, CLEANING, CONTAINER, TREE, OTHER
    }

    public enum Status {

    }

    private static final int MINIMUM_CONFIRMATIONS = 10;
    private String id;
    private String description;
    private Bitmap picture;
    private LatLng location;
    private Priority priority;
    private Category category;
    private int nb_confirmations;
    private int nb_reports;
    private String userId;
    private ArrayList<String> confirmations;
    private ArrayList<String> reports;
    private boolean invalid;
    private boolean fixed;
    private String poblation;

    public Incidence(){}

    public Incidence(String desc, Bitmap pic, LatLng loc, Priority prio, Category cat,String userId, int nb_confirmations, String poblation) {
        this.description = desc;
        this.picture = pic;
        this.location = loc;
        this.priority = prio;
        this.category = cat;
        this.userId = userId;
        this.nb_confirmations = nb_confirmations;
        this.confirmations = new ArrayList<>();
        this.nb_reports = nb_reports;
        this.invalid = false;
        this.fixed = false;
        this.reports = new ArrayList<>();
        this.poblation = poblation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public int getNb_confirmations() {

        return confirmations.size();
    }

    public int getNb_reported() {

        return reports.size();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPoblation(String poblation) { this.poblation = poblation; }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isInvalid() { return invalid;   }

    public boolean isFixed() {
        return fixed;
    }

    public void setInvalid(boolean invalid) { this.invalid = invalid; }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public String getDescription() {
        return description;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public LatLng getLocation() {
        return location;
    }

    public Priority getPriority() {
        return priority;
    }

    public Category getCategory() {
        return category;
    }

    public String getPoblation() { return poblation; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public void confirm() {
        ++nb_confirmations;
    }

    public void report() {
        ++nb_reports;
    }


    public boolean isReported() {
        return reports.contains(CurrentUser.getInstance().getUser().getMail());
    }
    public boolean isConfirmed() {
        return confirmations.contains(CurrentUser.getInstance().getUser().getMail());
    }

    public void setConfirmations(ArrayList<String> confirmations) {
        this.confirmations = confirmations;
    }

    public void setReports(ArrayList<String> reports) {
        this.reports = reports;
    }

    public ArrayList<String> getReports() {
        return reports;
    }

    public ArrayList<String> getConfirmations() {
        return confirmations;
    }
}


