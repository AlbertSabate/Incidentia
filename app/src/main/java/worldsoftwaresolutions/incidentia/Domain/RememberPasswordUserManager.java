package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class RememberPasswordUserManager extends AsyncTask<RememberPasswordUserManager.TaskRememberPasswordUserParams, Integer, RememberPasswordUserManager.TaskRememberPasswordUserResult> {

    private RememberPasswordUserManager.TaskRememberPasswordUserListener taskRememberPasswordUserListener;

    public RememberPasswordUserManager(RememberPasswordUserManager.TaskRememberPasswordUserListener taskRememberPasswordUserListener) {
        this.taskRememberPasswordUserListener = taskRememberPasswordUserListener;
    }

    protected TaskRememberPasswordUserResult doInBackground(RememberPasswordUserManager.TaskRememberPasswordUserParams... params){

        String mail = params[0].getMail();

        String result = "";

        result = DataControllerUser.rememberPasswordUser(mail);

        // Return the result
        if(result.contains("true"))return new RememberPasswordUserManager.TaskRememberPasswordUserResult(true, result);
        else return new TaskRememberPasswordUserResult(false,result);
    }

    protected void onPostExecute(RememberPasswordUserManager.TaskRememberPasswordUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskRememberPasswordUserListener.onRememberPasswordUser();
        }
        else if(result.getExcepcio()=="connectFail"){
            taskRememberPasswordUserListener.onNotConnection();
        }
        else if (result.userNotExistsError()) {
            taskRememberPasswordUserListener.onUserNotExists();
        }
        else if (result.userRegisteredWithGoogle()) {
            taskRememberPasswordUserListener.onGoogleUser();
        }
        else {
            taskRememberPasswordUserListener.otherError();
        }

    }


    public static class TaskRememberPasswordUserParams {


        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        private String mail;

        public TaskRememberPasswordUserParams (String mail){

            this.mail = mail;
        }
    }

    public static class TaskRememberPasswordUserResult {

        private boolean success;
        private String excepcio;

        public TaskRememberPasswordUserResult(boolean success, String excepcio) {
            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }
        public boolean userRegisteredWithGoogle()
        {
            return excepcio.contains("isGoogleUser");
        }
        public boolean userNotExistsError(){
            return excepcio.contains("userNotExists");
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    public interface TaskRememberPasswordUserListener{
        void onRememberPasswordUser();

        void onUserNotExists();

        void onGoogleUser();

        void onNotConnection();

        void otherError();
    }


}
