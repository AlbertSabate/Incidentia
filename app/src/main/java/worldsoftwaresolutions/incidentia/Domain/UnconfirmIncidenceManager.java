package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;

/**
 * Created by Albert on 02/05/2017.
 */

public class UnconfirmIncidenceManager extends AsyncTask<UnconfirmIncidenceManager.TaskUnconfirmIncidenceParams,Integer,UnconfirmIncidenceManager.TaskUnconfirmIncidenceResult> {

    private UnconfirmIncidenceManager.TaskUnconfirmIncidenceListener unconfirmIncidenceListener;

    public UnconfirmIncidenceManager (UnconfirmIncidenceManager.TaskUnconfirmIncidenceListener unconfirmIncidenceListener) {

        this.unconfirmIncidenceListener = unconfirmIncidenceListener;
    }

    @Override
    protected TaskUnconfirmIncidenceResult doInBackground(TaskUnconfirmIncidenceParams... params) {

        String incidenceId = params[0].getIncidenceId();
        String userId = params[0].getUserId();

        String result = DataControllerIncidence.unconfirmIncidence(incidenceId,userId);

        if(result.contains("true"))return new UnconfirmIncidenceManager.TaskUnconfirmIncidenceResult(true, result);
        else return new UnconfirmIncidenceManager.TaskUnconfirmIncidenceResult(false,result);
    }

    public static class TaskUnconfirmIncidenceParams {

        private String incidenceId;
        private String userId;

        public String getIncidenceId() {

            return incidenceId;
        }

        public String getUserId() {

            return userId;
        }

        public TaskUnconfirmIncidenceParams(String incidenceId, String userId) {

            this.incidenceId = incidenceId;
            this.userId = userId;
        }
    }

    public static class TaskUnconfirmIncidenceResult {

        private boolean success;
        private String excepcio;

        public TaskUnconfirmIncidenceResult(boolean success, String excepcio) {

            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    protected void onPostExecute(UnconfirmIncidenceManager.TaskUnconfirmIncidenceResult result) {

        System.out.println("post execute");

        if (result.isSuccess()) {
            // Call the activity
            unconfirmIncidenceListener.onUnconfirmIncidentia();
        }
        else if(result.getExcepcio()=="connectFail"){
            unconfirmIncidenceListener.onNotConnection();
        }
        else {
            unconfirmIncidenceListener.onDoesNotUnconfirmIncidentia();
        }

    }

    public interface TaskUnconfirmIncidenceListener{

        void onUnconfirmIncidentia();

        void onDoesNotUnconfirmIncidentia();

        void onNotConnection();
    }
}