package worldsoftwaresolutions.incidentia.Domain;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.net.ParseException;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import worldsoftwaresolutions.incidentia.Domain.CreateUserManager.TaskCreateUserListener;
import worldsoftwaresolutions.incidentia.Domain.EditUserManager.TaskEditUserListener;
import worldsoftwaresolutions.incidentia.Domain.GetUserManager.TaskGetUserListener;
import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;
import worldsoftwaresolutions.incidentia.Exceptions.MailIsNotValidException;
import worldsoftwaresolutions.incidentia.Exceptions.PasswordIsNotValidException;
import worldsoftwaresolutions.incidentia.R;


public class DomainController {
    private static final String MAIL_PERSISTENT = "MAIL_PERSISTENT";
    private static final String PASSWORD_PERSISTENT = "PASSWORD_PERSISTENT";
    private static final String ISGOOGLE_PERSISTENT = "ISGOOGLE_PERSISTENT";


    public DomainController() {}

    public static void signOut(Context context) {
        CurrentUser currentUser = CurrentUser.getInstance();
        currentUser.setUser(getGuestUser(context.getResources()));
        Persistence.saveString( MAIL_PERSISTENT, "",context );
        Persistence.saveString( PASSWORD_PERSISTENT,"", context);
        Persistence.saveBoolean(ISGOOGLE_PERSISTENT, false, context);

    }

    public static void saveAutomaticLogIn(Context context, String mail, String password, boolean isGoogleUser) {
        Persistence.saveString( MAIL_PERSISTENT, mail,context );
        Persistence.saveString( PASSWORD_PERSISTENT, password, context);
        Persistence.saveBoolean(ISGOOGLE_PERSISTENT,isGoogleUser, context);
    }

    public static void saveAutomaticLogInPassword(Context context, String password) {
        Persistence.saveString( PASSWORD_PERSISTENT, password, context);
    }

    public static String getAutomaticLogInPassword(Context context) throws Exception {
        String password = Persistence.getString(PASSWORD_PERSISTENT, context);
        return password;
    }

    public static boolean getAutomaticLogInIsGoogle(Context context) throws Exception {
        boolean isGoogle = Persistence.getBoolean(ISGOOGLE_PERSISTENT, context);
        return isGoogle;
    }

    public static String getAutomaticLogInMail(Context context) throws Exception {
        String mail = Persistence.getString(MAIL_PERSISTENT, context);
        return mail;
    }

    public static void logInUser(String mail, String password, LogInUserManager.TaskLogInUserListener listener) throws PasswordIsNotValidException, MailIsNotValidException {
        if (!mailIsCorrect(mail)) throw new MailIsNotValidException();
        if (!passwordIsValid(password)) throw new PasswordIsNotValidException();
        LogInUserManager logInUserController = new LogInUserManager(listener);
        LogInUserManager.TaskLogInUserParams params = new LogInUserManager.TaskLogInUserParams(mail, password);
        logInUserController.execute(params);
    }

    public static void logInUserGoogle(String mail, LogInUserGoogleManager.TaskLogInUserGoogleListener listener) {

        LogInUserGoogleManager logInUserGoogleController = new LogInUserGoogleManager(listener);
        LogInUserGoogleManager.TaskLogInUserGoogleParams params = new LogInUserGoogleManager.TaskLogInUserGoogleParams(mail);
        logInUserGoogleController.execute(params);
    }



    public static void rememberPasswordUser(String mail,  RememberPasswordUserManager.TaskRememberPasswordUserListener listener) throws MailIsNotValidException {
        if (!mailIsCorrect(mail)) throw new MailIsNotValidException();
        RememberPasswordUserManager rememberPasswordUserController = new RememberPasswordUserManager(listener);
        RememberPasswordUserManager.TaskRememberPasswordUserParams params = new RememberPasswordUserManager.TaskRememberPasswordUserParams(mail);
        rememberPasswordUserController.execute(params);
    }


    public static void createUser(String name, String surname, String mail, String password, Bitmap photo, Date birth, boolean isGoogleUser, TaskCreateUserListener listener) throws MailIsNotValidException, PasswordIsNotValidException{
        if (!mailIsCorrect(mail)) throw new MailIsNotValidException();
        if (!passwordIsValid(password)) throw new PasswordIsNotValidException();
        CreateUserManager createUserController = new CreateUserManager(listener);
        CreateUserManager.TaskCreateUserParams params = new CreateUserManager.TaskCreateUserParams(name, surname, mail, password, photo, birth, isGoogleUser, new ArrayList<String>());
        createUserController.execute(params);
    }



    public static void createUserGoogle(String name, String surname, String mail, Bitmap photo, Date birth, boolean isGoogleUser, TaskCreateUserListener listener) {
        String password = "";
        CreateUserManager createUserController = new CreateUserManager(listener);
        CreateUserManager.TaskCreateUserParams params = new CreateUserManager.TaskCreateUserParams(name, surname, mail, password, photo, birth, isGoogleUser, new ArrayList<String>());
        createUserController.execute(params);
    }

    public static void deleteUser(String mail, Context context,DeleteUserManager.TaskDeleteUserListener listener){
        System.out.println("2.1");
        CurrentUser currentUser = CurrentUser.getInstance();
        System.out.println("2.2");
        currentUser.setUser(getGuestUser(context.getResources()));
        System.out.println("2.3");
        Persistence.saveString( MAIL_PERSISTENT, "",context );
        System.out.println("2.4");
        Persistence.saveString( PASSWORD_PERSISTENT,"", context);
        System.out.println("2.5");
        Persistence.saveBoolean(ISGOOGLE_PERSISTENT, false, context);
        System.out.println("2.6");
        DeleteUserManager deleteUserManager = new DeleteUserManager(listener);
        DeleteUserManager.TaskDeleteUserParams params = new DeleteUserManager.TaskDeleteUserParams(mail);
        deleteUserManager.execute(params);
    }

    public static void createComment(String idUser, String idIncidence, String text, CreateCommentManager.TaskCreateCommentListener listener) {
        CreateCommentManager createCommentController = new CreateCommentManager(listener);
        CreateCommentManager.TaskCreateCommentParams params = new CreateCommentManager.TaskCreateCommentParams(idUser, idIncidence, text);
        createCommentController.execute(params);
    }

    public static void deleteComment(String id, DeleteCommentManager.TaskDeleteCommentListener listener){
        DeleteCommentManager deleteCommentManager = new DeleteCommentManager(listener);
        DeleteCommentManager.TaskDeleteCommentParams params = new DeleteCommentManager.TaskDeleteCommentParams(id);
        deleteCommentManager.execute(params);
    }

    public static void createIncidence(Bitmap picture, String description, LatLng location, int priority, String category, int nbConfirmations, String poblation, CreateIncidenceManager.TaskCreateIncidenceListener listener){
        Incidence.Priority _priority = null;
        switch (priority) {
            case 0:
                _priority = Incidence.Priority.LOW;
                break;
            case 1:
                _priority = Incidence.Priority.MID;
                break;
            case 2:
                _priority = Incidence.Priority.HIGH;
                break;
            default:
                break;
        }
        CreateIncidenceManager createIncidenceController = new CreateIncidenceManager(listener);
        CreateIncidenceManager.TaskCreateIncidenceParams params = null;
        params = new CreateIncidenceManager.TaskCreateIncidenceParams(picture,description, location, _priority,category,nbConfirmations, poblation);
        createIncidenceController.execute(params);
    }


    public static void editPasswordUser (String mail, String password, EditPasswordUserManager.TaskEditPasswordUserListener listener) throws PasswordIsNotValidException {
        if (!passwordIsValid(password)) throw new PasswordIsNotValidException();
        EditPasswordUserManager editPasswordManager = new EditPasswordUserManager(listener);
        EditPasswordUserManager.TaskEditPasswordUserParams params = new EditPasswordUserManager.TaskEditPasswordUserParams(mail,password);
        editPasswordManager.execute(params);
    }


    public static void editUser (String name, String surname, String mail, Bitmap photo, Date birth, TaskEditUserListener listener) {

        EditUserManager editUserManager = new EditUserManager(listener);
        EditUserManager.TaskEditUserParams params = new EditUserManager.TaskEditUserParams(name, surname,  mail, photo, birth);
        editUserManager.execute(params);
    }

    public static void editLocation (String mail, double latitude, double longitude, String poblation, EditLocationUserManager.TaskEditLocationUserListener listener) {
        EditLocationUserManager editLocationUserManager = new EditLocationUserManager(listener);
        EditLocationUserManager.TaskEditLocationUserParams params = new EditLocationUserManager.TaskEditLocationUserParams(mail, latitude,longitude,poblation);
        editLocationUserManager.execute(params);
    }


    public static void editIncidence (String id, Bitmap picture, String description, LatLng location, int priority, String category, String poblation,EditIncidenceManager.TaskEditIncidenceListener listener) {
        Incidence.Priority _priority = null;
        switch (priority) {
            case 0:
                _priority = Incidence.Priority.LOW;
                break;
            case 1:
                _priority = Incidence.Priority.MID;
                break;
            case 2:
                _priority = Incidence.Priority.HIGH;
                break;
            default:
                break;
        }
        EditIncidenceManager editIncidenceManager = new EditIncidenceManager(listener);
        EditIncidenceManager.TaskEditIncidenceParams params = new EditIncidenceManager.TaskEditIncidenceParams(id, picture, description, location, _priority, category, poblation);
        editIncidenceManager.execute(params);
    }




    public static void getUser(String mail,TaskGetUserListener listener) {
        GetUserManager getUserManager = new GetUserManager(listener);
        GetUserManager.TaskGetUserParams params = new GetUserManager.TaskGetUserParams(mail);
        getUserManager.execute(params);

    }

    public static void getCurrentDate(GetCurrentDateManager.TaskGetCurrentDateListener listener) {
        GetCurrentDateManager getCurrentDateManager = new GetCurrentDateManager(listener);
        GetCurrentDateManager.TaskGetCurrentDateParams params = new GetCurrentDateManager.TaskGetCurrentDateParams();
        getCurrentDateManager.execute(params);

    }

    public static void getIncidence(String id, GetIncidenceManager.TaskGetIncidenceListener listener){
        GetIncidenceManager getIncidenceManager = new GetIncidenceManager(listener);
        GetIncidenceManager.TaskGetIncidenceParams params = new GetIncidenceManager.TaskGetIncidenceParams(id);
        getIncidenceManager.execute(params);
    }

    public static void getIncidencesByRadius(double latitude, double longitude, double radius,String priority, String category,GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusListener listener){
        GetIncidencesByRadiusManager getIncidencesByRadiusManager = new GetIncidencesByRadiusManager(listener);
        GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusParams params = new GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusParams(latitude,longitude,radius,priority,category);
        getIncidencesByRadiusManager.execute(params);
    }

    public static void getIncidencesByRadius(double latitude, double longitude, double radius, GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusListener listener) {
        GetIncidencesByRadiusManager getIncidencesByRadiusManager = new GetIncidencesByRadiusManager(listener);
        GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusParams params = new GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusParams(latitude,longitude,radius,null,null);
        getIncidencesByRadiusManager.execute(params);
    }


    public static void deleteIncidence(String id, int mNumConfirmations, DeleteIncidenceManager.TaskDeleteIncidenceListener listener){
        DeleteIncidenceManager deleteIncidenceManager = new DeleteIncidenceManager(listener);
        DeleteIncidenceManager.TaskDeleteIncidenceParams params = new DeleteIncidenceManager.TaskDeleteIncidenceParams(id,mNumConfirmations);
        deleteIncidenceManager.execute(params);
    }

    public static void getAllCommentsById(String id_incidence, GetAllCommentsByIdManager.TaskGetAllCommentsByIdListener listener){
        GetAllCommentsByIdManager getAllCommentsByIdController = new GetAllCommentsByIdManager(listener);
        GetAllCommentsByIdManager.TaskGetAllCommentsByIdParams params = new GetAllCommentsByIdManager.TaskGetAllCommentsByIdParams(id_incidence);
        getAllCommentsByIdController.execute(params);
    }

    public static void createIncidence(String description, Bitmap picture, Location location, int Priority, String category) {

    }

    public static void confirmIncidentia (String incidentiaId, String userId, ConfirmIncidenceManager.TaskConfirmIncidenceListener listener) {
        ConfirmIncidenceManager confirmIncidenceManager = new ConfirmIncidenceManager(listener);
        ConfirmIncidenceManager.TaskConfirmIncidenceParams params = new ConfirmIncidenceManager.TaskConfirmIncidenceParams(incidentiaId,userId);
        confirmIncidenceManager.execute(params);
    }

    public static void unconfirmIncidentia (String incidentiaId,String userId, UnconfirmIncidenceManager.TaskUnconfirmIncidenceListener listener) {
        UnconfirmIncidenceManager unconfirmIncidenceManager = new UnconfirmIncidenceManager(listener);
        UnconfirmIncidenceManager.TaskUnconfirmIncidenceParams params = new UnconfirmIncidenceManager.TaskUnconfirmIncidenceParams(incidentiaId,userId);
        unconfirmIncidenceManager.execute(params);
    }

    public static void reportIncidentia (String incidentiaId, ReportIncidenceManager.TaskReportIncidenceListener listener) {
        ReportIncidenceManager reportIncidenceManager = new ReportIncidenceManager(listener);
        ReportIncidenceManager.TaskReportIncidenceParams params = new ReportIncidenceManager.TaskReportIncidenceParams(incidentiaId);
        reportIncidenceManager.execute(params);
    }

    public static void getUsersSortedByScore (GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreListener listener) {
        GetUsersSortedByScoreManager getUsersSortedByScoreManager = new GetUsersSortedByScoreManager(listener);
        GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreParams  params= new GetUsersSortedByScoreManager.TaskGetUsersSortedByScoreParams();
        getUsersSortedByScoreManager.execute(params);
    }

    public static void getLocalUsersSortedByScore (String poblation, GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreListener listener) {
        GetLocalUsersSortedByScoreManager getLocalUsersSortedByScoreManager = new GetLocalUsersSortedByScoreManager(listener);
        GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreParams params= new GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreParams(poblation);
        getLocalUsersSortedByScoreManager.execute(params);
    }

    private static boolean passwordIsValid(String password) {
        //return true if and only if password:
        //1. have at least eight characters.
        //2. consists of only letters and digits.
        //3. must contain at least two digits.
        if (password.length() < 8) {
            return false;
        } else {
            char c;
            int count = 1;
            for (int i = 0; i < password.length() - 1; i++) {
                c = password.charAt(i);
                if (!Character.isLetterOrDigit(c)) {
                    return false;
                } else if (Character.isDigit(c)) {
                    count++;
                    if (count < 2)   {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static boolean mailIsCorrect(String mail){
        java.util.regex.Pattern pattern = Pattern.compile("^.+@.+\\..+$");
        java.util.regex.Matcher matcher = pattern.matcher(mail);
        return matcher.matches();
    }

    public static String encodeBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeFromBase64(String image) {
        byte[] decodedByteArray = android.util.Base64.decode(image, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedByteArray, 0, decodedByteArray.length);
    }

    public static String getData(java.util.Date date, java.util.Date actualDate) throws ParseException {

        String result = null;

        int daysbetween = daysBetween(date, actualDate);

        if (daysbetween == 0) {
            result = LesThanOneDayDiference(date, actualDate);
        } else if (daysbetween <= 7) {
            result = daysbetween + " days ago";
        }
        else {
            result = MoreThanSevenDaysDiference(date);

        }

        return result;
    }

    private static String MoreThanSevenDaysDiference(java.util.Date datePicture) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(datePicture);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        //gener és el mes 0, per aixo el +1.
        int month = cal.get(Calendar.MONTH) +1;
        int year = cal.get(Calendar.YEAR);
        String result = day +"/" + month +"/" + year;
        return result;
    }

    private static String LesThanOneDayDiference(java.util.Date startDate, java.util.Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;


        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        String result = elapsedHours + "h " + elapsedMinutes + "m ago";
        return result;
    }

    public static int daysBetween(java.util.Date startDate, java.util.Date endDate) {
        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        int daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static Calendar getDatePart(java.util.Date date){
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal;                                  // return the date part
    }

    private static int getSquareCropDimensionForBitmap(Bitmap bitmap)
    {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public static Bitmap getSquareThumbnail(Bitmap source) {
        int dimension = getSquareCropDimensionForBitmap(source);
        System.out.println("DOMAINCONTROLER, DIMENSION: " + dimension);
        //return ThumbnailUtils.extractThumbnail(source,dimension,dimension);
        return ThumbnailUtils.extractThumbnail(source, dimension, dimension);


    }

    public static RoundedBitmapDrawable getRoundedThumbnail(Resources res, Bitmap source) {
        Bitmap tmpBitmap = getSquareThumbnail(source);
        //creamos el drawable redondeado
        RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(res, tmpBitmap);
        //asignamos el CornerRadius
        roundedDrawable.setCornerRadius(tmpBitmap.getHeight());

        return roundedDrawable;
    }
    // PRE: get(0) is PostalCode. get(1) is city. get(2) is country
    // POST: return one string
    public static String writeLocation(ArrayList<String> nameLocation) {
        String result = "";
        if (!(nameLocation.isEmpty())) {
            if (!(nameLocation.get(0).contains("NO POSTALCODE"))) { //CODI POSTAL
                result += nameLocation.get(0);
                if (!(nameLocation.get(1).contains("NO CITY"))) { // CODI POSTAL + POBLACIO
                    result += " ";
                    result += nameLocation.get(1);
                    if (!(nameLocation.get(2).contains("NO COUNTRY"))) { // CODI POSTAL + POBLACIO + PAIS
                        result += ", ";
                        result += nameLocation.get(2);
                    }
                } else { //CODI POSTAL + NO POBLACIÓ
                    if (!(nameLocation.get(2).contains("NO COUNTRY"))) { // CODI POSTAL + NO POBLACIO + PAIS
                        result += ", ";
                        result += nameLocation.get(2);
                    }
                }
            } else { //NO HI HA CODI POSTAL
                if (!(nameLocation.get(1).contains("NO CITY"))) { // NO CODI POSTAL + POBLACIO
                    result += nameLocation.get(1);
                    if (!(nameLocation.get(2).contains("NO COUNTRY"))) { // NO CODI POSTAL + POBLACIO + PAIS
                        result += ", ";
                        result += nameLocation.get(2);
                    }
                } else { //NO CODI POSTAL + NO POBLACIÓ
                    if (!(nameLocation.get(2).contains("NO COUNTRY"))) { // NO CODI POSTAL + NO POBLACIO + PAIS
                        result += nameLocation.get(2);
                    } else {
                        result += " ";
                    }
                }
            }
        } else {
            result += " ";
        }
        return result;
    }


    // PRE: lattitude and longitude from map
    // POST: ArrayList<String> where position 0 is PostalCode, 1 is City and 2 is Country
    public static ArrayList<String> getLocationName(double lattitude, double longitude, Context context) {
        ArrayList<String> result = new ArrayList<>();
        String cityName;
        String postalCodeNumber;
        String countryName;
        Geocoder mGcd = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = mGcd.getFromLocation(lattitude, longitude, 1);
            if (!(addresses.isEmpty())) {
                String city = addresses.get(0).getLocality();
                String postalCode = addresses.get(0).getPostalCode();
                String country = addresses.get(0).getCountryName();
                if (postalCode != null && !postalCode.equals("")) {
                    postalCodeNumber = postalCode;
                    result.add(postalCodeNumber);
                }   else {
                    result.add("NO POSTALCODE");
                }
                if (city != null && !city.equals("")) {
                    cityName = city;
                    result.add(cityName);
                }   else {
                    result.add("NO CITY");
                }
                if (country != null && !country.equals("")) {
                    countryName = country;
                    result.add(countryName);
                }   else {
                    result.add("NO COUNTRY");
                }
            }
            else {
                result.add("NO POSTALCODE");
                result.add("NO CITY");
                result.add("NO COUNTRY");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static  User getGuestUser(Resources res) {
        String mail = res.getString(R.string.guest_user_mail);
        String password = res.getString(R.string.guest_user_password);
        String name = res.getString(R.string.guest_user_name);
        String surname = res.getString(R.string.guest_user_surname);
        Drawable originalDrawable = res.getDrawable(R.drawable.user_default);
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();
        boolean isGoogleUser = false;
        ArrayList<String> list = new ArrayList<>();
        try {
            return new User(mail,password,name,surname,originalBitmap,new Date(1,1,1111), isGoogleUser, list);
        } catch (InvalidDateException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        System.out.println("HOLAOVERLAY");
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, 0, null);
        return bmOverlay;
    }

    //Given an Incidence.Category, returns a bitmap corresponding to its icon
    public static Bitmap getCategoryIcon(Incidence.Category category, Resources resources) {

        switch (category){
            case SIDEWALK:
                return BitmapFactory.decodeResource(resources, R.drawable.sidewalk);
            case ROAD:
                return BitmapFactory.decodeResource(resources, R.drawable.road);
            case PARK:
                return BitmapFactory.decodeResource(resources, R.drawable.park);
            case LIGHTS:
                return BitmapFactory.decodeResource(resources, R.drawable.lights);
            case TRAFFIC_LIGHTS:
                return BitmapFactory.decodeResource(resources, R.drawable.traffic_lights);
            case CLEANING:
                return BitmapFactory.decodeResource(resources, R.drawable.cleaning);
            case CONTAINER:
                return BitmapFactory.decodeResource(resources, R.drawable.container);
            case TREE:
                return BitmapFactory.decodeResource(resources, R.drawable.tree);
            case OTHER:
                return BitmapFactory.decodeResource(resources, R.drawable.other);
            default:
                return BitmapFactory.decodeResource(resources, R.drawable.other);
        }
    }

    //Given a priority level, returns a bitmap corresponding to its icon
    public static Bitmap getPriorityIcon(int priority, Resources resources) {
        switch (priority) {
            case 0:
                return BitmapFactory.decodeResource(resources, R.drawable.low_priority);
            case 1:
                return BitmapFactory.decodeResource(resources, R.drawable.mid_priority);
            case 2:
                return BitmapFactory.decodeResource(resources, R.drawable.high_priority);
            default:
                return BitmapFactory.decodeResource(resources, R.drawable.low_priority);
        }
    }

}
