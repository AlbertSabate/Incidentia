package worldsoftwaresolutions.incidentia.Domain;

import java.util.ArrayList;

public class CurrentUser {

    static private CurrentUser currentUser;
    private User user;
    private ArrayList<Incidence> incidenceArray;

    static public CurrentUser getInstance() {
        if(currentUser == null) currentUser = new CurrentUser();
        return currentUser;
    }

    public ArrayList<Incidence> getIncidenceArray() {
        return incidenceArray;
    }

    public void newIncidence () {
        incidenceArray = new ArrayList<>();
    }

    public void addIncidence (Incidence i) {
        incidenceArray.add(i);
    }

    public void setIncidenceArray(ArrayList<Incidence> incidenceArray) {
        this.incidenceArray = incidenceArray;
    }

    public Incidence getIncidenceById (String id) {
        for (int i=0; i<incidenceArray.size();++i) {
            if (incidenceArray.get(i).getId().equals(id)) {
                return incidenceArray.get(i);
            }
        }
        return null;
    }


    public void deleteIncidence (String id) {
        for (int i=0; i<incidenceArray.size();++i) {
            if (incidenceArray.get(i).getId().equals(id)) {
                incidenceArray.remove(i);
            }
        }
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void deleteUser(){
        incidenceArray = new ArrayList<>();
    }
}
