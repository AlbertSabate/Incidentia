package worldsoftwaresolutions.incidentia.Domain;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Pau on 23/04/2017.
 */

public class Persistence {

    public static void saveString(String filename, String text, Context context){
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos =  context.openFileOutput(filename, Context.MODE_PRIVATE);
            out = new ObjectOutputStream(fos);
            out.writeObject(text);

            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getString(String filename, Context context) throws Exception {
        FileInputStream fis = null;
        ObjectInputStream in = null;
        String text;
        if(fileExists(context, filename)) {
            fis = context.openFileInput(filename);
            in = new ObjectInputStream(fis);
            text = (String) in.readObject();
            in.close();
            return text;
        }
        else {
            text = "";
            return text;
        }
    }

    public static void saveBoolean(String filename, Boolean text, Context context){
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos =  context.openFileOutput(filename, Context.MODE_PRIVATE);
            out = new ObjectOutputStream(fos);
            out.writeObject(text);

            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Boolean getBoolean(String filename, Context context) throws Exception {
        FileInputStream fis = null;
        ObjectInputStream in = null;
        Boolean text;
        if(fileExists(context, filename)) {
            fis = context.openFileInput(filename);
            in = new ObjectInputStream(fis);
            text = (Boolean) in.readObject();
            in.close();
            return text;
        }
        else {
            text = null;
            return text;
        }


    }


    public static boolean fileExists(Context context, String filename) {
        File file = context.getFileStreamPath(filename);
        if(file == null || !file.exists()) {
            return false;
        }
        return true;
    }
}
