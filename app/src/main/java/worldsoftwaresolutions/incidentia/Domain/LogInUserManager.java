package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class LogInUserManager extends AsyncTask<worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserParams, Integer, worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserResult> {

    private worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserListener taskLogInUserListener;

    public LogInUserManager(worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserListener taskLogInUserListener) {
        this.taskLogInUserListener = taskLogInUserListener;
    }

    protected worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserResult doInBackground(worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserParams... params){
        String mail = params[0].getMail();
        String password = params[0].getPassword();
        User user = new User();
        String result = "";

        result = DataControllerUser.logInUser(user, mail, password);
        System.out.println(user.getMail());
        // Return the result
        System.out.println("Resulta "+  result);
        if(result.contains("true"))return new worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserResult(true, user, result);
        else return new worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserResult(false, user, result);

    }

    protected void onPostExecute(worldsoftwaresolutions.incidentia.Domain.LogInUserManager.TaskLogInUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskLogInUserListener.onLogInUser(result.LogInUser());
        }
        else if(result.getExcepcio()=="connectFail"){
            taskLogInUserListener.onNotConnection();
        }
        else if (result.passwordError()) {
                taskLogInUserListener.onPasswordIncorrect();
            }
            else if (result.userNotExistsError()) {
                taskLogInUserListener.onUserNotRegistered();
            }
            else if (result.GoogleUser()) {
                taskLogInUserListener.onRegisteredWithGoogle();
            }
            else taskLogInUserListener.onOtherError();
    }


    public static class TaskLogInUserParams {

        private String mail;
        private String password;

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {this.password = password;}


        public TaskLogInUserParams (String mail, String password){

            this.mail = mail;
            this.password = password;
        }
    }

    public static class TaskLogInUserResult {
        private User user;
        private boolean success;
        private String excepcio;

        public TaskLogInUserResult(boolean success, User user, String excepcio) {
            this.setSuccess(success);
            this.user=user;
            this.excepcio=excepcio;
        }

        public String getExcepcio() {
            return excepcio;
        }


        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public boolean passwordError()
        {
            return excepcio.contains("IncorrectPassword");
        }
        public boolean userNotExistsError(){
            return excepcio.contains("userNotExists");
        }
        public boolean GoogleUser() {
            return excepcio.contains("isGoogleUser");
        }
        public User LogInUser() {
            return user;
        }

    }

    public interface TaskLogInUserListener{
        void onLogInUser(User user);

        void onPasswordIncorrect();

        void onUserNotRegistered();

        void onOtherError();

        void onRegisteredWithGoogle();

        void onNotConnection();
    }


}
