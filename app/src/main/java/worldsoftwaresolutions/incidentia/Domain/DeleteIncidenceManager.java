package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;


import worldsoftwaresolutions.incidentia.Data.DataControllerComment;
import worldsoftwaresolutions.incidentia.Data.DataControllerUser;
import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;



public class DeleteIncidenceManager extends AsyncTask<DeleteIncidenceManager.TaskDeleteIncidenceParams, Integer, DeleteIncidenceManager.TaskDeleteIncidenceResult> {

    private DeleteIncidenceManager.TaskDeleteIncidenceListener taskDeleteIncidenceListener;

    public DeleteIncidenceManager(DeleteIncidenceManager.TaskDeleteIncidenceListener taskDeleteIncidenceListener) {
        this.taskDeleteIncidenceListener = taskDeleteIncidenceListener;
    }

    protected DeleteIncidenceManager.TaskDeleteIncidenceResult doInBackground(DeleteIncidenceManager.TaskDeleteIncidenceParams... params) {

        String id = params[0].getId();
        int score = params[0].getScore();

        String result = DataControllerIncidence.deleteIncidence(id);

        // Return the result
        if(result.contains("true")){
            String userMail = params[0].getUserMail();

            result = DataControllerUser.deleteIncidence(userMail,id, score);
            if (result.contains(("true"))) {

                result = DataControllerComment.deleteCommentsByIdIncidence(id);

                if (result.contains("true")) return new DeleteIncidenceManager.TaskDeleteIncidenceResult(true, result);
                else return new DeleteIncidenceManager.TaskDeleteIncidenceResult(false,result);
            }
            else return new DeleteIncidenceManager.TaskDeleteIncidenceResult(false,result);
        }
        else return new DeleteIncidenceManager.TaskDeleteIncidenceResult(false,result);
    }

    protected void onPostExecute(DeleteIncidenceManager.TaskDeleteIncidenceResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskDeleteIncidenceListener.onDeleteIncidence();
        } else if (result.getMessage().contains("connectFail")) {
            taskDeleteIncidenceListener.onNotConnection();
        } else {
            taskDeleteIncidenceListener.onDoesNotDeleteIncidence();
        }
    }


    public static class TaskDeleteIncidenceParams {

        private String id;
        private int score;

        public String getUserMail() {
            return userMail;
        }

        public int getScore() {
            return score;
        }

        public void setUserMail(String userMail) {
            this.userMail = userMail;
        }

        public void setScore(int score) {
            this.score = score;
        }

        private String userMail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public TaskDeleteIncidenceParams(String id, int score) {
            this.id = id;
            this.score = score;
            this.userMail = CurrentUser.getInstance().getUser().getMail();
        }
    }

    public static class TaskDeleteIncidenceResult {

        private boolean success;
        private String message;

        public TaskDeleteIncidenceResult(boolean success,String message) {
            this.setSuccess(success);
            this.setMessage(message);
        }

        public boolean isSuccess() {
            return success;
        }
        public String getMessage() { return message; }

        public void setSuccess(boolean success) {
            this.success = success;
        }
        public void setMessage(String message) {
            this.message = message;
        }

    }

    public interface TaskDeleteIncidenceListener {
        void onDeleteIncidence();

        void onDoesNotDeleteIncidence();

        void onNotConnection();
    }

}
