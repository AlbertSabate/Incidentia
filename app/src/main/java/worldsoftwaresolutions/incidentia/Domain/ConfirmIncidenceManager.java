package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;
import android.os.SystemClock;

import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;

public class ConfirmIncidenceManager extends AsyncTask<ConfirmIncidenceManager.TaskConfirmIncidenceParams,Integer,ConfirmIncidenceManager.TaskConfirmIncidenceResult> {

    private ConfirmIncidenceManager.TaskConfirmIncidenceListener confirmIncidenceListener;

    public ConfirmIncidenceManager (ConfirmIncidenceManager.TaskConfirmIncidenceListener confirmIncidenceListener) {

        this.confirmIncidenceListener = confirmIncidenceListener;
    }

    @Override
    protected TaskConfirmIncidenceResult doInBackground(TaskConfirmIncidenceParams... params) {

        System.out.println("Execute Confirm Incide");

        String incidenceId = params[0].getIncidenceId();
        String userId = params[0].getUserId();

        String result = DataControllerIncidence.confirmIncidence(incidenceId,userId);

        if(result.contains("true")) return new ConfirmIncidenceManager.TaskConfirmIncidenceResult(true, result);
        else return new ConfirmIncidenceManager.TaskConfirmIncidenceResult(false,result);
    }

    public static class TaskConfirmIncidenceParams {

        private String incidenceId;
        private String userId;

        public String getIncidenceId() {

            return incidenceId;
        }

        public String getUserId() {

            return userId;
        }

        public TaskConfirmIncidenceParams(String incidenceId, String userId) {

            this.incidenceId = incidenceId;
            this.userId = userId;
        }

    }

    public static class TaskConfirmIncidenceResult {

        private boolean success;
        private String excepcio;

        public TaskConfirmIncidenceResult(boolean success, String excepcio) {

            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    protected void onPostExecute(ConfirmIncidenceManager.TaskConfirmIncidenceResult result) {

        System.out.println("post execute");

        if (result.isSuccess()) {
            // Call the activity
            confirmIncidenceListener.onConfirmIncidentia();
        }
        else if(result.getExcepcio()=="connectFail"){
            confirmIncidenceListener.onNotConnection();
        }
        else {
            confirmIncidenceListener.onDoesNotConfirmIncidentia();
        }

    }

    public interface TaskConfirmIncidenceListener{

        void onConfirmIncidentia();

        void onDoesNotConfirmIncidentia();

        void onNotConnection();
    }
}
