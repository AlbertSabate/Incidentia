package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;
import worldsoftwaresolutions.incidentia.Data.DataControllerComment;


public class DeleteCommentManager extends AsyncTask<DeleteCommentManager.TaskDeleteCommentParams, Integer, DeleteCommentManager.TaskDeleteCommentResult> {

    private DeleteCommentManager.TaskDeleteCommentListener taskDeleteCommentListener;

    public DeleteCommentManager(DeleteCommentManager.TaskDeleteCommentListener taskDeleteCommentListener) {
        this.taskDeleteCommentListener = taskDeleteCommentListener;
    }

    protected DeleteCommentManager.TaskDeleteCommentResult doInBackground(DeleteCommentManager.TaskDeleteCommentParams... params) {

        String id = params[0].getId();

        String result = DataControllerComment.deleteComment(id);

        // Return the result
        if(result.contains("true"))return new TaskDeleteCommentResult(true,null);
        else return new TaskDeleteCommentResult(false,result);

    }

    protected void onPostExecute(DeleteCommentManager.TaskDeleteCommentResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskDeleteCommentListener.onDeleteComment();
        } else if (result.getMessage().contains("connectFail")) {
            taskDeleteCommentListener.onNotConnection();
        } else {
            taskDeleteCommentListener.onDoesNotDeleteComment();
        }
    }


    public static class TaskDeleteCommentParams {

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public TaskDeleteCommentParams(String id) {
            this.id = id;
            }
    }

    public static class TaskDeleteCommentResult {

        private boolean success;
        private String message;

        public TaskDeleteCommentResult(boolean success,String message) {
            this.setSuccess(success);
            this.setMessage(message);
        }

        public boolean isSuccess() {
            return success;
        }
        public String getMessage() { return message; }

        public void setSuccess(boolean success) {
            this.success = success;
        }
        public void setMessage(String message) {
            this.message = message;
        }

    }

    public interface TaskDeleteCommentListener {
        void onDeleteComment();

        void onDoesNotDeleteComment();

        void onNotConnection();
    }

}
