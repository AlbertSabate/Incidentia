package worldsoftwaresolutions.incidentia.Domain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;

/**
 * Created by albertcalle on 30/3/17.
 */

public class Date {
    private int day;
    private int month;
    private int year;

    private static int currentYear;

    public Date(int day, int month, int year) throws InvalidDateException {
        String day_s = String.valueOf(day);
        String month_s = String.valueOf(month);
        String year_s = String.valueOf(year);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            format.parse(day_s + "/" + month_s + "/" + year_s);
        } catch (ParseException e) {
            throw new InvalidDateException();
        }
        this.day = day;
        this.month = month;
        this.year = year;

    }

    public Date(String date) throws InvalidDateException{
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            format.parse(date);
        } catch (ParseException e) {
            throw new InvalidDateException();
        }
        String slices[] = date.split("/");
        day = Integer.parseInt(slices[0]);
        month = Integer.parseInt(slices[1]);
        year = Integer.parseInt(slices[2]);

    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String toString() {
        String day_s = String.valueOf(day);
        String month_s = String.valueOf(month);
        String year_s = String.valueOf(year);
        return day_s + "/" + month_s + "/" + year_s;
    }

    public boolean before(Date date) {
        Calendar date1, date2;
        date1 = new GregorianCalendar(year, month, day);
        date2 = new GregorianCalendar(date.getYear(), date.getMonth(), date.getDay());
        return date1.before(date2);
        /*if (year > date.getYear()) return false;
        else if (year < date.getYear()) return true;
        else if (month > date.getMonth()) return false;
        else if (month < date.getMonth()) return true;
        else return day < date.getDay();*/
    }

    public boolean before(Calendar date) {
        Calendar date1;
        date1 = new GregorianCalendar(year, month, day);
        return date1.before(date);
    }

    public boolean after(Date date) {
        Calendar date1, date2;
        date1 = new GregorianCalendar(year, month, day);
        date2 = new GregorianCalendar(date.getYear(), date.getMonth(), date.getDay());
        return date1.after(date2);
        /*if (year < date.getYear()) return false;
        else if (year > date.getYear()) return true;
        else if (month < date.getMonth()) return false;
        else if (month > date.getMonth()) return true;
        else return day > date.getDay();*/
    }

    public boolean after(Calendar date) {
        Calendar date1;
        date1 = new GregorianCalendar(year, month, day);
        return date1.after(date);
    }

    public boolean equals(Date date) {
        Calendar date1, date2;
        date1 = new GregorianCalendar(year, month,day);
        date2 = new GregorianCalendar(date.getYear(), date.getMonth(), date.getDay());
        return date1.equals(date2);
        /*return  year  == date.getYear()  &&
                month == date.getMonth() &&
                day   == date.getDay();*/
    }

    public boolean equals(Calendar date) {
        Calendar date1;
        date1 = new GregorianCalendar(year, month, day);
        return date1.equals(date);
    }
}


