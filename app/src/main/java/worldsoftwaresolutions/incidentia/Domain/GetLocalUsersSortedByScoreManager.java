package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class GetLocalUsersSortedByScoreManager extends AsyncTask<GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreParams,Integer,GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreResult> {

    private GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreListener GetLocalUsersSortedByScoreListener;

    public GetLocalUsersSortedByScoreManager (GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreListener GetLocalUsersSortedByScoreListener) {

        this.GetLocalUsersSortedByScoreListener = GetLocalUsersSortedByScoreListener;
    }

    @Override
    protected TaskGetLocalUsersSortedByScoreResult doInBackground(TaskGetLocalUsersSortedByScoreParams... params) {

        ArrayList<User> users = new ArrayList<>();

        String poblation = params[0].getPoblation();

        String result = null;
        try {
            result = DataControllerUser.GetLocalUsersSortedByScore(poblation,users);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(result.equals("false") || result.equals("connectFail"))return new GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreResult(users,false, result);
        else return new GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreResult(users,true,result);
    }

    public static class TaskGetLocalUsersSortedByScoreParams {

        private String poblation;

        public TaskGetLocalUsersSortedByScoreParams(String poblation) {
            this.poblation = poblation;
        }

        public String getPoblation() {
            return poblation;
        }

    }

    public static class TaskGetLocalUsersSortedByScoreResult {

        private ArrayList<User> users;
        private boolean success;
        private String excepcio;

        public TaskGetLocalUsersSortedByScoreResult(ArrayList<User> users,boolean success, String excepcio) {
            this.users = users;
            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public ArrayList<User> getUsers () { return users; }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    protected void onPostExecute(GetLocalUsersSortedByScoreManager.TaskGetLocalUsersSortedByScoreResult result) {

        System.out.print("getLocalUsersSortedByScore: ");

        if (result.isSuccess()) {
            System.out.println("Succed");
            GetLocalUsersSortedByScoreListener.onGetLocalUsersSortedByScore(result.getUsers());
        }
        else if(result.getExcepcio().equals("connectFail")){
            System.out.println("Error on Connection");
            GetLocalUsersSortedByScoreListener.onNotConnection();
        }
        else {
            System.out.println("Failed");
            GetLocalUsersSortedByScoreListener.onDoesNotGetLocalUsersSortedByScore();
        }

    }

    public interface TaskGetLocalUsersSortedByScoreListener{

        void onGetLocalUsersSortedByScore(ArrayList<User> users);

        void onDoesNotGetLocalUsersSortedByScore();

        void onNotConnection();
    }
}

