package worldsoftwaresolutions.incidentia.Domain;

import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;

/**
 * Created by edgar on 4/21/17.
 */
import java.util.Date;

public class Comment {

    private String id;
    private String id_user;
    private String id_incidence;
    private String text;
    private java.util.Date date;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Comment(String id_user, String id_incidence, String text, java.util.Date date){
        this.id_user=id_user;
        this.id_incidence=id_incidence;
        this.text=text;
        this.date=date;


    }

    public String getId_user() {
        return id_user;
    }

    public String getId_incidence() {
        return id_incidence;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setId(String id) {
        this.id=id;
    }

    public String getId() {
        return id;
    }

}
