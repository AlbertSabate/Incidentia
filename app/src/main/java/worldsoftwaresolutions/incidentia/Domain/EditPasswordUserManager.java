package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class EditPasswordUserManager extends AsyncTask<EditPasswordUserManager.TaskEditPasswordUserParams, Integer, EditPasswordUserManager.TaskEditPasswordUserResult> {
    private EditPasswordUserManager.TaskEditPasswordUserListener taskEditPasswordUserListener;

    public EditPasswordUserManager(EditPasswordUserManager.TaskEditPasswordUserListener taskEditPasswordUserListener) {
        this.taskEditPasswordUserListener = taskEditPasswordUserListener;
    }

    protected TaskEditPasswordUserResult doInBackground(EditPasswordUserManager.TaskEditPasswordUserParams... params) {
        String mail = params[0].getMail();
        String password = params[0].getPassword();
        String result = DataControllerUser.editPasswordUser(mail, password);

        // Return the result
        if (result.contains("true")) return new TaskEditPasswordUserResult(true, null);
        else return new TaskEditPasswordUserResult(false, result);

    }

    protected void onPostExecute(EditPasswordUserManager.TaskEditPasswordUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskEditPasswordUserListener.onEditPasswordUser();
        } else if (result.getExcepcio() == "connectFail") {
            taskEditPasswordUserListener.onNotConnection();
        } else {
            taskEditPasswordUserListener.onDoesNotEditPasswordUser();
        }

    }


    public static class TaskEditPasswordUserParams {

        private String password;
        private String mail;

        public String getMail() {
            return mail;
        }

        public String getPassword() {
            return password;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public TaskEditPasswordUserParams(String mail, String password) {
            this.password = password;
            this.mail = mail;
        }
    }

    public static class TaskEditPasswordUserResult {
        private boolean success;

        private String excepcio;

        public TaskEditPasswordUserResult(boolean success, String excepcio) {
            this.setSuccess(success);
            this.excepcio = excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    public interface TaskEditPasswordUserListener {
        void onEditPasswordUser();

        void onDoesNotEditPasswordUser();

        void onNotConnection();
    }
}



