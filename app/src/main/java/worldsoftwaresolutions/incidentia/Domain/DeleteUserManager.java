package worldsoftwaresolutions.incidentia.Domain;



import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerComment;
import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;
import worldsoftwaresolutions.incidentia.Data.DataControllerUser;
import worldsoftwaresolutions.incidentia.Domain.DeleteUserManager.TaskDeleteUserParams;
import worldsoftwaresolutions.incidentia.Domain.DeleteUserManager.TaskDeleteUserResult;

public class DeleteUserManager extends AsyncTask<TaskDeleteUserParams, Integer, TaskDeleteUserResult> {

    private TaskDeleteUserListener taskDeleteUserListener;

    public DeleteUserManager(TaskDeleteUserListener taskDeleteUserListener) {
        this.taskDeleteUserListener = taskDeleteUserListener;
    }

    protected TaskDeleteUserResult doInBackground(TaskDeleteUserParams... params) {

        String mail = params[0].getMail();

        String result = DataControllerUser.deleteUser(mail);
        System.out.println("1.1");
        // Return the result
        if(result.contains("true")){
            System.out.println("1.2");
            result = DataControllerIncidence.deleteIncidencesByIdUser(mail);
            if (result.contains(("true"))) {
                System.out.println("1.3");
                result = DataControllerComment.deleteCommentsByIdUser(mail);

                if (result.contains("true")) return new TaskDeleteUserResult(true,null);
                else return new TaskDeleteUserResult(false,result);
            }
            else return new TaskDeleteUserResult(false,result);
        }
        else return new TaskDeleteUserResult(false,result);


    }

    protected void onPostExecute(TaskDeleteUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskDeleteUserListener.onDeleteUser();
        } else if (result.getMessage().contains("connectFail")) {
            taskDeleteUserListener.onNotConnection();
        } else {
            taskDeleteUserListener.onDoesNotDeleteUser();
        }
    }


    public static class TaskDeleteUserParams {

        private String mail;

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public TaskDeleteUserParams(String mail) {
            this.mail = mail;
        }
    }

    public static class TaskDeleteUserResult {

        private boolean success;
        private String message;

        public TaskDeleteUserResult(boolean success,String message) {
            this.setSuccess(success);
            this.setMessage(message);
        }

        public boolean isSuccess() {
            return success;
        }
        public String getMessage() { return message; }

        public void setSuccess(boolean success) {
            this.success = success;
        }
        public void setMessage(String message) {
            this.message = message;
        }

    }

    public static interface TaskDeleteUserListener {
        public void onDeleteUser();

        public void onDoesNotDeleteUser();

        public void onNotConnection();
    }
}