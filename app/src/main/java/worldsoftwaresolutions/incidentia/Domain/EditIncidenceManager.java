package worldsoftwaresolutions.incidentia.Domain;


import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;

public class EditIncidenceManager extends AsyncTask<EditIncidenceManager.TaskEditIncidenceParams, Integer, EditIncidenceManager.TaskEditIncidenceResult> {
    private EditIncidenceManager.TaskEditIncidenceListener taskEditIncidenceListener;

    public EditIncidenceManager(EditIncidenceManager.TaskEditIncidenceListener taskEditIncidenceListener) {
        this.taskEditIncidenceListener = taskEditIncidenceListener;
    }

    protected TaskEditIncidenceResult doInBackground(EditIncidenceManager.TaskEditIncidenceParams... params) {
        String id = params[0].getId();
        Bitmap picture = params[0].getPicture();
        String description = params[0].getDescription();
        double longitude = params[0].getLocation().longitude;
        double latitude = params[0].getLocation().latitude;
        String poblation = params[0].getPoblation();
        String priority = params[0].get_priority().name();
        String category = params[0].getCategory();

        String result = DataControllerIncidence.editIncidence(id, picture,description,longitude,latitude,priority,category, poblation);

        // Return the result

        if (result.contains("true")) return new TaskEditIncidenceResult(true, null);
        else return new TaskEditIncidenceResult(false, result);

    }

    protected void onPostExecute(EditIncidenceManager.TaskEditIncidenceResult result) {

        if (result.isSuccess()) {

            // Call the activity
            taskEditIncidenceListener.onEditIncidence();
        } else if (result.getExcepcio() == "connectFail") {
            taskEditIncidenceListener.onNotConnection();
        } else {
            taskEditIncidenceListener.onDoesNotEditIncidence();
        }

    }


    public static class TaskEditIncidenceParams {
        private String id;
        private Bitmap picture;
        private String description;
        private LatLng location;
        private Incidence.Priority _priority;
        private String category;
        private String poblation;

        public String getId() {
            return id;
        }

        public Bitmap getPicture() {
            return picture;
        }

        public LatLng getLocation() {
            return location;
        }

        public Incidence.Priority get_priority() {
            return _priority;
        }

        public String getCategory() {
            return category;
        }

        public String getDescription() {
            return description;
        }

        public String getPoblation() {
            return poblation;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void set_priority(Incidence.Priority _priority) {
            this._priority = _priority;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setLocation(LatLng location) {
            this.location = location;
        }

        public void setPicture(Bitmap picture) {
            this.picture = picture;
        }

        public void setPoblation(String poblation) {
            this.poblation = poblation;
        }

        public TaskEditIncidenceParams(String id, Bitmap picture, String description, LatLng location, Incidence.Priority _priority, String category, String poblation) {
            this.id = id;
            this.picture = picture;
            this.description = description;
            this.location = location;
            this._priority = _priority;
            this.category = category;
            this.poblation = poblation;
        }
    }

    public static class TaskEditIncidenceResult {

        private boolean success;

        private String excepcio;

        public TaskEditIncidenceResult(boolean success, String excepcio) {
            this.setSuccess(success);
            this.excepcio = excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }


    }

    public interface TaskEditIncidenceListener {
        void onEditIncidence();

        void onDoesNotEditIncidence();

        void onNotConnection();
    }
}



