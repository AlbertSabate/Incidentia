package worldsoftwaresolutions.incidentia.Domain;


import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class CreateUserManager extends AsyncTask<CreateUserManager.TaskCreateUserParams, Integer, CreateUserManager.TaskCreateUserResult> {

    private CreateUserManager.TaskCreateUserListener taskCreateUserListener;

    public CreateUserManager(CreateUserManager.TaskCreateUserListener taskCreateUserListener) {
        this.taskCreateUserListener = taskCreateUserListener;
    }

    protected TaskCreateUserResult doInBackground(CreateUserManager.TaskCreateUserParams... params){

        User user = params[0].getUser();

        String result = "";

        result = DataControllerUser.createUser(user);

        // Return the result
        if(result.contains("true"))return new CreateUserManager.TaskCreateUserResult(true, result);
        else return new TaskCreateUserResult(false,result);
    }

    protected void onPostExecute(CreateUserManager.TaskCreateUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskCreateUserListener.onCreateUser();
        }
        else if(result.getExcepcio()=="connectFail"){
            taskCreateUserListener.onNotConnection();
        }
        else {
            taskCreateUserListener.onDoesNotCreateUser();
        }

    }


    public static class TaskCreateUserParams {


        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        private User user;

        public TaskCreateUserParams (String name, String surname, String mail, String password, Bitmap photo, Date birth, boolean isGoogleUser, ArrayList<String> arrayList){

            this.user = new User(mail,password,name,surname,photo,birth,isGoogleUser, arrayList);
            this.user.setLongitude(181.000000);
            this.user.setLatitude(91.000000);
        }
    }

    public static class TaskCreateUserResult {

        private boolean success;
        private String excepcio;

        public TaskCreateUserResult(boolean success, String excepcio) {
            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    public interface TaskCreateUserListener{
        void onCreateUser();

        void onDoesNotCreateUser();

        void onNotConnection();
    }


}
