package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;


import worldsoftwaresolutions.incidentia.Data.DataControllerUser;
import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;


public class GetIncidenceManager extends AsyncTask<GetIncidenceManager.TaskGetIncidenceParams, Integer, GetIncidenceManager.TaskGetIncidenceResult> {

    private GetIncidenceManager.TaskGetIncidenceListener taskGetIncidenceListener;

    public GetIncidenceManager(GetIncidenceManager.TaskGetIncidenceListener taskGetIncidenceListener) {
        this.taskGetIncidenceListener = taskGetIncidenceListener;
    }

    protected GetIncidenceManager.TaskGetIncidenceResult doInBackground(GetIncidenceManager.TaskGetIncidenceParams... params){
        String id = params[0].getId();
        Incidence incidence = new Incidence();
        incidence.setId(id);

        String result = DataControllerIncidence.getIncidence(incidence,id);


        // Return the result
        if(result.contains("true"))return new GetIncidenceManager.TaskGetIncidenceResult(true, incidence, result);
        else return new GetIncidenceManager.TaskGetIncidenceResult(false, incidence, result);

    }

    protected void onPostExecute(GetIncidenceManager.TaskGetIncidenceResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskGetIncidenceListener.onGetIncidence(result.getIncidence());
        }
        else if(result.getExcepcio()=="connectFail"){
            taskGetIncidenceListener.onNotConnection();
        }
        else {
            taskGetIncidenceListener.onDoesNotGetIncidence();
        }

    }


    public static class TaskGetIncidenceParams {

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


        public TaskGetIncidenceParams (String id){

            this.id = id;
        }
    }

    public static class TaskGetIncidenceResult {
        private Incidence incidence;
        private boolean success;
        private String excepcio;

        public TaskGetIncidenceResult(boolean success, Incidence incidence, String excepcio) {
            this.setSuccess(success);
            this.incidence=incidence;
            this.excepcio=excepcio;
        }

        public String getExcepcio() {
            return excepcio;
        }


        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public Incidence getIncidence() {
            return incidence;
        }

    }

    public interface TaskGetIncidenceListener{
        void onGetIncidence(Incidence incidence);

        void onDoesNotGetIncidence();

        void onNotConnection();
    }


}