package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import worldsoftwaresolutions.incidentia.Data.DataControllerComment;

public class GetAllCommentsByIdManager extends AsyncTask<worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdParams, Integer, worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdResult> {

    private worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdListener taskGetAllCommentsByIdListener;

    public GetAllCommentsByIdManager(worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdListener taskGetAllCommentsByIdListener) {
        this.taskGetAllCommentsByIdListener = taskGetAllCommentsByIdListener;
    }

    protected worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdResult doInBackground(worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdParams... params){
        String id_incidence = params[0].getId_incidence();
        List<Comment> listComments = new ArrayList<>();

        String result = "";

        result = DataControllerComment.getAllCommentsById(listComments,id_incidence);

        // Return the result
        if(result.contains("true"))return new worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdResult(true, listComments, result);
        else return new worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdResult(false, listComments, result);

    }

    protected void onPostExecute(worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager.TaskGetAllCommentsByIdResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskGetAllCommentsByIdListener.onGetAllCommentsById(result.GetAllCommentsById());
        }
        else if(result.getExcepcio()=="connectFail"){
            taskGetAllCommentsByIdListener.onNotConnection();
        }
        else {
            taskGetAllCommentsByIdListener.onDoesNotGetAllCommentsById();
        }

    }


    public static class TaskGetAllCommentsByIdParams {

        private String id_incidence;

        public String getId_incidence() {
            return id_incidence;
        }

        public void setId_incidence(String id_incidence) {
            this.id_incidence = id_incidence;
        }




        public TaskGetAllCommentsByIdParams (String id_incidence){

            this.id_incidence = id_incidence;
        }
    }

    public static class TaskGetAllCommentsByIdResult {
        private List<Comment> listComments;
        private boolean success;
        private String excepcio;

        public TaskGetAllCommentsByIdResult(boolean success,List<Comment> listComments , String excepcio) {
            this.setSuccess(success);
            this.listComments = listComments;
            this.excepcio=excepcio;
        }

        public String getExcepcio() {
            return excepcio;
        }


        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public List<Comment> GetAllCommentsById() {
            return listComments;
        }
    }

    public interface TaskGetAllCommentsByIdListener{
        void onGetAllCommentsById(List <Comment> listComment);

        void onDoesNotGetAllCommentsById();

        void onNotConnection();
    }


}
