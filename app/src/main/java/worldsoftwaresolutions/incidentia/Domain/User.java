package worldsoftwaresolutions.incidentia.Domain;

import android.graphics.Bitmap;
import android.location.Location;

import java.io.Serializable;
import java.util.ArrayList;

import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;


public class User implements Serializable {
    private String mail, password, name, surname;
    private double latitude;
    private double longitude;
    private String dayBirth;
    private String monthBirth;
    private String yearBirth;
    private boolean isGoogleUser;
    private int score;
    private String poblation;
    public Bitmap getPicture() {
        return photo;
    }

    public void setPicture(Bitmap photo) {
        this.photo = photo;
    }

    private Bitmap photo;
    private ArrayList<String> incidences;

    public User() {

    }

    public User (String mail, String password, String name, String surname, Bitmap photo, Date birth, boolean isGoogleUser, ArrayList<String> incidences) {
        this.mail = mail;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.dayBirth = Integer.toString(birth.getDay());
        this.monthBirth = Integer.toString(birth.getMonth());
        this.yearBirth = Integer.toString(birth.getYear());
        this.incidences = incidences;
        this.photo = photo;
        this.isGoogleUser = isGoogleUser;
        score = 0;
        poblation = "unknown";
    }

    public String getMail() {
        return mail;
    }

    public Date getBirth() {
        try {
            return new Date(Integer.valueOf(dayBirth),Integer.valueOf(monthBirth),Integer.valueOf(yearBirth));
        } catch (InvalidDateException e) {
            return null;
        }
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getPoblation() {
        return poblation;
    }

    public ArrayList<String> getIncidences() {
        return incidences;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setBirth(Date birth) {
        this.dayBirth = Integer.toString(birth.getDay());
        this.monthBirth = Integer.toString(birth.getMonth());
        this.yearBirth = Integer.toString(birth.getYear());
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setPoblation(String poblation) {
        this.poblation = poblation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setIncidencies(ArrayList<String> incidencies) {
        this.incidences = incidencies;
    }

    public void addIncidencies (String incidencia) {
        this.incidences.add(incidencia);
    }
    public void deteleIncidencies (String incidencia) {
        this.incidences.remove(this.incidences.indexOf(incidencia));
    }
    public void existIncidencies (String incidencia) {
        this.incidences.contains(incidencia);
    }

    public boolean getIsGoogleUser() {
        return isGoogleUser;
    }

    public void setIsGoogleUser(boolean googleUser) {
        isGoogleUser = googleUser;
    }

    public  void setScore (int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "User{" +
                "mail='" + mail + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", location=" + latitude + " " + longitude + '\'' +
                ", birth=" + this.getBirth() +
                ", incidencies=" + incidences +
                ", score=" + score +
                '}';
    }
}
