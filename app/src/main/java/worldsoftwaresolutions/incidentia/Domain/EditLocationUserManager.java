package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class EditLocationUserManager extends AsyncTask<EditLocationUserManager.TaskEditLocationUserParams, Integer, EditLocationUserManager.TaskEditLocationUserResult> {
    private EditLocationUserManager.TaskEditLocationUserListener taskEditLocationUserListener;

    public EditLocationUserManager(EditLocationUserManager.TaskEditLocationUserListener taskEditLocationUserListener) {
        this.taskEditLocationUserListener = taskEditLocationUserListener;
    }

    protected TaskEditLocationUserResult doInBackground(EditLocationUserManager.TaskEditLocationUserParams... params) {
        String mail = params[0].getMail();
        double latitude = params[0].getLatitude();
        double longitude = params[0].getLongitude();
        String poblation = params[0].getPoblation();
        String result = DataControllerUser.editLocationUser(mail,latitude,longitude,poblation);

        // Return the result
        if (result.contains("true")) return new TaskEditLocationUserResult(true, null);
        else return new TaskEditLocationUserResult(false, result);

    }

    protected void onPostExecute(EditLocationUserManager.TaskEditLocationUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskEditLocationUserListener.onEditLocationUser();
        } else if (result.getExcepcio() == "connectFail") {
            taskEditLocationUserListener.onNotConnection();
        } else {
            taskEditLocationUserListener.onDoesNotEditLocationUser();
        }

    }


    public static class TaskEditLocationUserParams {

        private double latitude;
        private double longitude;
        private String mail;
        private String poblation;

        public String getMail() {
            return mail;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public void setLattitude(double lattitude) {
            this.latitude = latitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public TaskEditLocationUserParams(String mail, double latitude, double longitude, String poblation) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.mail = mail;
            this.poblation = poblation;
        }

        public String getPoblation() {
            return poblation;
        }
    }

    public static class TaskEditLocationUserResult {
        private boolean success;

        private String excepcio;

        public TaskEditLocationUserResult(boolean success, String excepcio) {
            this.setSuccess(success);
            this.excepcio = excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    public interface TaskEditLocationUserListener {
        void onEditLocationUser();

        void onDoesNotEditLocationUser();

        void onNotConnection();
    }
}



