package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;

public class ReportIncidenceManager extends AsyncTask<ReportIncidenceManager.TaskReportIncidenceParams,Integer,ReportIncidenceManager.TaskReportIncidenceResult> {

    private ReportIncidenceManager.TaskReportIncidenceListener ReportIncidenceListener;

    public ReportIncidenceManager (ReportIncidenceManager.TaskReportIncidenceListener ReportIncidenceListener) {

        this.ReportIncidenceListener = ReportIncidenceListener;
    }

    @Override
    protected TaskReportIncidenceResult doInBackground(TaskReportIncidenceParams... params) {

        System.out.println("Execute Report Incide");

        String incidenceId = params[0].getIncidenceId();

        String result = DataControllerIncidence.ReportIncidence(incidenceId);

        if(result.contains("true"))return new ReportIncidenceManager.TaskReportIncidenceResult(true, result);
        else return new ReportIncidenceManager.TaskReportIncidenceResult(false,result);
    }

    public static class TaskReportIncidenceParams {

        private String incidenceId;

        public String getIncidenceId() {

            return incidenceId;
        }

        public TaskReportIncidenceParams(String incidenceId) {

            this.incidenceId = incidenceId;
        }

    }

    public static class TaskReportIncidenceResult {

        private boolean success;
        private String excepcio;

        public TaskReportIncidenceResult(boolean success, String excepcio) {

            this.setSuccess(success);
            this.excepcio=excepcio;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getExcepcio() {
            return excepcio;
        }

    }

    protected void onPostExecute(ReportIncidenceManager.TaskReportIncidenceResult result) {

        System.out.println("post execute");

        if (result.isSuccess()) {
            // Call the activity
            ReportIncidenceListener.onReportIncidentia();
        }
        else if(result.getExcepcio()=="connectFail"){
            ReportIncidenceListener.onNotConnection();
        }
        else {
            ReportIncidenceListener.onDoesNotReportIncidentia();
        }

    }

    public interface TaskReportIncidenceListener{

        void onReportIncidentia();

        void onDoesNotReportIncidentia();

        void onNotConnection();
    }
}
