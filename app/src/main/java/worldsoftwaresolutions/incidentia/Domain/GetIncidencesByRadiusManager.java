package worldsoftwaresolutions.incidentia.Domain;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;


import worldsoftwaresolutions.incidentia.Data.DataControllerIncidence;

/**
 * Created by edgar on 5/16/17.
 */

public class GetIncidencesByRadiusManager extends AsyncTask<GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusParams, Integer, GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusResult> {

    private worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusListener taskGetIncidencesByRadiusListener;

    public GetIncidencesByRadiusManager(worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusListener taskGetIncidencesByRadiusListener) {
        this.taskGetIncidencesByRadiusListener = taskGetIncidencesByRadiusListener;
    }

    protected worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusResult doInBackground(worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusParams... params){
        double latitude = params[0].getLatitude();
        double longitude = params[0].getLongitude();
        double radius = params[0].getRadius();
        String priority = params[0].getPriority();
        String category = params[0].getCategory();


        List<Incidence> listIncidences = new ArrayList<>();

        String result = "";

        result = DataControllerIncidence.getIncidencesByRadius(listIncidences,latitude,longitude,radius,priority,category);

        // Return the result
        if(result.contains("true"))return new worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusResult(true, listIncidences, result);
        else return new worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusResult(false, listIncidences, result);

    }

    protected void onPostExecute(worldsoftwaresolutions.incidentia.Domain.GetIncidencesByRadiusManager.TaskGetIncidencesByRadiusResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskGetIncidencesByRadiusListener.onGetIncidencesByRadius(result.GetIncidencesByRadius());
        }
        else if(result.getExcepcio()=="connectFail"){
            taskGetIncidencesByRadiusListener.onNotConnection();
        }
        else {
            taskGetIncidencesByRadiusListener.onDoesNotGetIncidencesByRadius();
        }

    }


    public static class TaskGetIncidencesByRadiusParams {


        private double latitude;
        private double longitude;
        private double radius;
        private String priority;
        private String category;


        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getRadius() {
            return radius;
        }

        public void setRadius(double radius) {
            this.radius = radius;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public TaskGetIncidencesByRadiusParams (double latitude,double longitude,double radius, String priority, String category){

            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
            this.priority = priority;
            this.category = category;
        }
    }

    public static class TaskGetIncidencesByRadiusResult {
        private List<Incidence> listIncidences;
        private boolean success;
        private String excepcio;

        public TaskGetIncidencesByRadiusResult(boolean success,List<Incidence> listIncidences , String excepcio) {
            this.setSuccess(success);
            this.listIncidences = listIncidences;
            this.excepcio=excepcio;
        }

        public String getExcepcio() {
            return excepcio;
        }


        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public List<Incidence> GetIncidencesByRadius() {
            return listIncidences;
        }
    }

    public interface TaskGetIncidencesByRadiusListener{
        void onGetIncidencesByRadius(List <Incidence> listIncidence);

        void onDoesNotGetIncidencesByRadius();

        void onNotConnection();
    }


}
