package worldsoftwaresolutions.incidentia.Domain;


import android.os.AsyncTask;

import worldsoftwaresolutions.incidentia.Data.DataControllerUser;

public class GetUserManager extends AsyncTask<GetUserManager.TaskGetUserParams, Integer, GetUserManager.TaskGetUserResult> {

    private GetUserManager.TaskGetUserListener taskGetUserListener;

    public GetUserManager(GetUserManager.TaskGetUserListener taskGetUserListener) {
        this.taskGetUserListener = taskGetUserListener;
    }

    protected TaskGetUserResult doInBackground(GetUserManager.TaskGetUserParams... params){
        String mail = params[0].getMail();
        User user = new User();
        user.setMail(mail);
        String result = "";

        result = DataControllerUser.getUser(user,mail);

        // Return the result
        if(result.contains("true"))return new GetUserManager.TaskGetUserResult(true, user, result);
        else return new GetUserManager.TaskGetUserResult(false, user, result);

    }

    protected void onPostExecute(GetUserManager.TaskGetUserResult result) {
        if (result.isSuccess()) {
            // Call the activity
            taskGetUserListener.onGetUser(result.getUser());
        }
        else if(result.getExcepcio()=="connectFail"){
            taskGetUserListener.onNotConnection();
        }
        else {
            taskGetUserListener.onDoesNotGetUser();
        }

    }


    public static class TaskGetUserParams {

        private String mail;

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }


        public TaskGetUserParams (String mail){

            this.mail = mail;
        }
    }

    public static class TaskGetUserResult {
        private User user;
        private boolean success;
        private String excepcio;

        public TaskGetUserResult(boolean success, User user, String excepcio) {
            this.setSuccess(success);
            this.user=user;
            this.excepcio=excepcio;
        }

        public String getExcepcio() {
            return excepcio;
        }


        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public User getUser() {
            return user;
        }

    }

    public interface TaskGetUserListener{
        void onGetUser(User user);

        void onDoesNotGetUser();

        void onNotConnection();
    }


}
