package worldsoftwaresolutions.incidentia.Data;


import android.graphics.Bitmap;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import worldsoftwaresolutions.incidentia.Domain.Date;
import worldsoftwaresolutions.incidentia.Domain.GetUsersSortedByScoreManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;

public class DataControllerUser {
    private static final String baseHostname = "10.4.41.153";
    private static final int basePort = 3000;
    private static final String baseUrl = "http://" + baseHostname + ":" + String.valueOf(basePort) + "/";
    private static final String deleteUserUrl = "deleteUser";
    private static final String editUserUrl = "editUser";
    private static final String addIncidenceUrl = "addIncidenceUser";
    private static final String deleteIncidenceUrl = "deleteIncidenceUser";
    private static final String editPasswordUrl = "editPasswordUser";
    private static final String editLocationUrl = "editLocationUser";
    private static final String createUserUrl = "createUser";
    private static final String logInUserUrl = "logInGetUser";
    private static final String logInUserGoogleUrl = "logInGetUserGoogle";
    private static final String sendMailUserUrl = "sendMailUser";
    private static final String getUserUrl = "getUser";
    private static final String isGoogleUserUrl = "isGoogleUserUrl";
    private static final String getUsersSortedByScore = "getUsersSortedByScore";
    private static final String getLocalUsersSortedByScore = "getLocalUsersSortedByScore";
    private static final String setScore = "setScore";
    private static final String idParam = "_id";
    private static final String mailParameter = "mail";
    private static final String passwordParameter = "password";
    private static final String nameParameter = "name";
    private static final String surnameParameter = "surname";
    private static final String dayBirthParameter = "dayBirth";
    private static final String monthBirthParameter = "monthBirth";
    private static final String yearBirthParameter = "yearBirth";
    private static final String incidencesParameter = "incidences";
    private static final String latitudeParameter = "latitude";
    private static final String longitudeParameter = "longitude";
    private static final String googleUserParameter = "googleUser";
    private static final String photoParameter = "photo";
    private static final String scoreParameter = "score";
    private static final String changeParameter = "change";
    private static final String poblationParameter = "poblation";


    // Returns "true" if the user is deleted, returns the error message in other cases
    public static String deleteUser(String mail) {
        // Build the URL
        String url = baseUrl + deleteUserUrl;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, mail);

        String deleted = post(url, parameters);

        return deleted;
    }

    public static String editUser(String name, String surname, String mail, Bitmap photo, Date birth) {

        // Build the URL
        String url = baseUrl + editUserUrl;
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, mail);
        parameters.put(nameParameter, name);
        parameters.put(surnameParameter, surname);
        parameters.put(dayBirthParameter, String.valueOf(birth.getDay()));
        parameters.put(monthBirthParameter, String.valueOf(birth.getMonth()));
        parameters.put(yearBirthParameter, String.valueOf(birth.getYear()));
        String postResponse = post(url, parameters);
        if (postResponse.contains("true")) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/users/");
            StorageReference file = storageRef.child(mail + ".jpeg");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();
            file.putBytes(data);
        }

        return postResponse;
    }

    public static String addIncidence(String mail, String incidenceId) {
        //Build the URL
        String url = baseUrl + addIncidenceUrl;
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, mail);
        parameters.put(incidencesParameter, incidenceId);
        String postResponse = post(url, parameters);

        return postResponse;
    }

    public static String deleteIncidence(String mail, String incidenceId, int score) {
        //Build the URL
        String url = baseUrl + deleteIncidenceUrl;
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, mail);
        parameters.put(incidencesParameter, incidenceId);
        parameters.put(scoreParameter, String.valueOf(score));
        System.out.println("DATACONTROLLERUSER: " + String.valueOf(score));
        String postResponse = post(url, parameters);

        return postResponse;
    }

    public static String editPasswordUser(String mail, String password) {

        // Build the URL
        String url = baseUrl + editPasswordUrl;


        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, mail);
        parameters.put(passwordParameter, password);
        String postResponse = post(url, parameters);

        return postResponse;
    }
    public static String editLocationUser(String mail, double latitude, double longitude, String poblation) {
        // Build the URL
        String url = baseUrl + editLocationUrl;
        String lattitudeS = String.valueOf(latitude);
        String longitudeS = String.valueOf(longitude);
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, mail);
        parameters.put(latitudeParameter, lattitudeS);
        parameters.put(longitudeParameter, longitudeS);
        parameters.put(poblationParameter, poblation);
        String postResponse = post(url, parameters);

        return postResponse;
    }

    public static String createUser(User user) {

        // Build the URL
        String url = baseUrl + createUserUrl;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, user.getMail());
        parameters.put(passwordParameter, user.getPassword());
        parameters.put(nameParameter, user.getName());
        parameters.put(surnameParameter, user.getSurname());
        parameters.put(dayBirthParameter, String.valueOf(user.getBirth().getDay()));
        parameters.put(monthBirthParameter, String.valueOf(user.getBirth().getMonth()));
        parameters.put(yearBirthParameter, String.valueOf(user.getBirth().getYear()));
        parameters.put(latitudeParameter, String.valueOf(user.getLatitude()));
        parameters.put(longitudeParameter, String.valueOf(user.getLongitude()));
        parameters.put(googleUserParameter, String.valueOf(user.getIsGoogleUser()));
        parameters.put(poblationParameter, user.getPoblation());
        String created = post(url, parameters);
        if (created.contains("true")) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/users/");
            StorageReference file = storageRef.child(user.getMail() + ".jpeg");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            user.getPicture().compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] data = baos.toByteArray();
            file.putBytes(data);
        }

        return created;
    }

    public static String rememberPasswordUser(String mail) {

        // Build the URL
        String url = baseUrl + sendMailUserUrl;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(mailParameter, mail);

        String created = post(url, parameters);

        return created;
    }

    public static String logInUser(User user, String mail, String password) {

        // Build the URL
        String url = baseUrl + logInUserUrl;
        String result = "false";
        try {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(mailParameter, mail);
            parameters.put(passwordParameter, password);
            String postResponse = post(url, parameters);
            if (postResponse.contains("connectFail")) {
                result = "connectFail";
            } else {
                result = postResponse;
                // Analyze response content
                System.out.print(postResponse);
                JSONArray jsonUserArray = (JSONArray) new JSONTokener(postResponse).nextValue();
                JSONObject jsonUser = (JSONObject) jsonUserArray.get(0);

                // Try
                try {
                    createUser(jsonUser, user);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                result = "true";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String logInUserGoogle(User user, String mail) {

        // Build the URL
        String url = baseUrl + logInUserGoogleUrl;
        String result = "false";
        try {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(mailParameter, mail);
            String postResponse = post(url, parameters);
            if (postResponse.contains("connectFail")) {
                result = "connectFail";
            } else {
                result = postResponse;
                // Analyze response content
                System.out.print(postResponse);
                JSONArray jsonUserArray = (JSONArray) new JSONTokener(postResponse).nextValue();
                JSONObject jsonUser = (JSONObject) jsonUserArray.get(0);

                // Try
                try {
                    createUser(jsonUser, user);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                result = "true";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
        public static String GetUsersSortedByScore (List < User > users) throws
        JSONException, ParseException {

            String url = baseUrl + getUsersSortedByScore;

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(mailParameter, "allUsers");

            String result = post(url, parameters);

            System.out.println("RESULT: " + result);

            if (result.contains("connectFail")) {
                result = "connectFail";
            } else {
                System.out.print(result);
                JSONArray jsonUserArray = (JSONArray) new JSONTokener(result).nextValue();
                for (int i = 0; i < jsonUserArray.length(); i++) {
                    User user = new User();
                    JSONObject jsonUser = (JSONObject) jsonUserArray.get(i);
                    createUser(jsonUser, user);
                    users.add(user);
                    System.out.println("User " + i + ": " + user);
                }
                System.out.println(users);
            }

            return result;
        }


        public static String isGoogleUser (String mail){
            //Build the URL
            String url = baseUrl + isGoogleUserUrl;
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(mailParameter, mail);
            String postResponse = post(url, parameters);
            return postResponse;
        }

        public static String GetLocalUsersSortedByScore (String poblation, ArrayList < User > users) throws
        JSONException, ParseException {

            String url = baseUrl + getLocalUsersSortedByScore;

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(poblationParameter, poblation);

            String result = post(url, parameters);

            System.out.println("RESULT: " + result);

            if (result.contains("connectFail")) {
                result = "connectFail";
            } else {
                System.out.print(result);
                JSONArray jsonUserArray = (JSONArray) new JSONTokener(result).nextValue();
                for (int i = 0; i < jsonUserArray.length(); i++) {
                    User user = new User();
                    JSONObject jsonUser = (JSONObject) jsonUserArray.get(i);
                    createUser(jsonUser, user);
                    users.add(user);
                    System.out.println("User " + i + ": " + user);
                }
                System.out.println(users);
            }

            return result;
        }

        public static String setScore (String mail, String change){

            String url = baseUrl + setScore;

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(mailParameter, mail);
            parameters.put(changeParameter, change);

            String result = post(url, parameters);

            System.out.println("SET SCORE: " + result);

            return result;
        }

        public static String getUser (User user, String mail){

            // Build the URL
            String url = baseUrl + getUserUrl;
            String result = "false";
            try {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put(mailParameter, mail);
                String postResponse = post(url, parameters);
                if (postResponse.contains("connectFail")) {
                    result = "connectFail";
                } else {
                    // Analyze response content
                    System.out.print(postResponse);
                    JSONArray jsonUserArray = (JSONArray) new JSONTokener(postResponse).nextValue();
                    JSONObject jsonUser = (JSONObject) jsonUserArray.get(0);

                    // Try
                    try {
                        createUser(jsonUser, user);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }

                    result = "true";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        private static void createUser (JSONObject jsonUser, User user)  throws
        JSONException, ParseException {
            //TODO afgir location


            if (jsonUser.length() >= 0) {
                String userMail = jsonUser.getString(idParam); //posem id perque el mail es la clau primaria
                String userPassword = jsonUser.getString(passwordParameter);
                String userName = jsonUser.getString(nameParameter);
                String userSurname = jsonUser.getString(surnameParameter);
                int userDay = Integer.valueOf(jsonUser.getString(dayBirthParameter));
                int userMonth = Integer.valueOf(jsonUser.getString(monthBirthParameter));
                int userYear = Integer.valueOf(jsonUser.getString(yearBirthParameter));
                double latitude = Double.valueOf(jsonUser.getString(latitudeParameter));
                double longitude = Double.valueOf(jsonUser.getString(longitudeParameter));
                if (jsonUser.has(scoreParameter)) {
                    int score = Integer.valueOf(jsonUser.getString(scoreParameter));
                    user.setScore(score);
                }
                if (jsonUser.has("poblation")) {
                    String poblation = jsonUser.getString(poblationParameter);
                    user.setPoblation(poblation);
                }

                Date userBirth = null;
                try {
                    userBirth = new Date(userDay, userMonth, userYear);
                } catch (InvalidDateException e) {
                    e.printStackTrace();
                }
                JSONArray userIncidencesJSON = jsonUser.getJSONArray(incidencesParameter);
                ArrayList<String> userIncidences = new ArrayList<>();
                for (int i = 0; i < userIncidencesJSON.length(); ++i) {
                    userIncidences.add(userIncidencesJSON.getString(i));
                }
                boolean isGoogleUser = Boolean.valueOf(jsonUser.getString(googleUserParameter));
                user.setName(userName);
                user.setSurname(userSurname);
                user.setMail(userMail);
                user.setPassword(userPassword);
                user.setBirth(userBirth);
                user.setIncidencies(userIncidences);
                user.setLatitude(latitude);
                user.setLongitude(longitude);
                user.setIsGoogleUser(isGoogleUser);

            }

        }

        private static String post (String url, HashMap < String, String > parameters){

            StringBuilder params = new StringBuilder("");
            String result = "";
            try {
                for (String s : parameters.keySet()) {
                    params.append("&" + s + "=");

                    params.append(URLEncoder.encode(parameters.get(s), "UTF-8"));
                }


                //String url =_url;
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                con.setRequestProperty("Accept-Language", "UTF-8");

                con.setDoOutput(true);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
                outputStreamWriter.write(params.toString());
                outputStreamWriter.flush();

                int responseCode = con.getResponseCode();

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine + "\n");
                }
                in.close();

                result = response.toString();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (Exception e) {
                result = "UnexpectedErrorOnPost";
            } finally {
                return result;
            }
        }


    }

