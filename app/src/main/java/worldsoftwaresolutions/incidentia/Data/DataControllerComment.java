package worldsoftwaresolutions.incidentia.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import worldsoftwaresolutions.incidentia.Domain.Comment;
import worldsoftwaresolutions.incidentia.Domain.Date;
import worldsoftwaresolutions.incidentia.Domain.User;



public class DataControllerComment {
    private static final String baseHostname = "10.4.41.153";
    private static final int basePort = 3001;
    private static final String baseUrl = "http://" + baseHostname + ":" + String.valueOf(basePort) + "/";
    private static final String getAllCommentsByIdUrl = "getAllCommentsById";
    private static final String getCurrentTimeUrl = "getCurrentTime";
    private static final String createCommentUrl = "createComment";
    private static final String deleteCommentUrl = "deleteComment";
    private static final String deleteCommentsByIdIncidenceUrl = "deleteCommentsByIdIncidence";
    private static final String deleteCommentsByIdUserUrl = "deleteCommentsByIdUser";
    private static final String idIncidenceParameter = "id_incidence";
    private static final String idUserParameter = "id_user";
    private static final String idCommentParameter = "_id";
    private static final String textParameter = "text";
    private static final String dateParameter = "createdAt";
    private static final String Data=null;
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");


    public static String deleteComment(String id) {
        // Build the URL
        String url = baseUrl + deleteCommentUrl;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idCommentParameter, id);

        String deleted = post(url, parameters);

        return deleted;
    }

    public static String deleteCommentsByIdIncidence(String id_incidence) {
        //Build the URL
        String url = baseUrl + deleteCommentsByIdIncidenceUrl;
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idIncidenceParameter, id_incidence);
        String postResponse = post(url, parameters);
        return postResponse;
    }

    public static String deleteCommentsByIdUser(String mail) {
        //Build the URL
        String url = baseUrl + deleteCommentsByIdUserUrl;
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idUserParameter, mail);
        String postResponse = post(url, parameters);
        return postResponse;
    }

    public static String getAllCommentsById(List<Comment> comments,String id_incidence) {

        // Build the URL
        String url = baseUrl + getAllCommentsByIdUrl;
        String result = "false";
        try {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(idIncidenceParameter, id_incidence);
            String postResponse = post(url, parameters);
            if (postResponse.contains("connectFail")) {
                result = "connectFail";
            }
            else {
                // Analyze response content
                System.out.print(postResponse);
                JSONArray jsonCommentsArray = (JSONArray) new JSONTokener(postResponse).nextValue();
                for(int k = jsonCommentsArray.length()-1;k>=0; --k){
                    JSONObject jsonComment = (JSONObject) jsonCommentsArray.get(k);
                    try {
                        Comment comment = createComment(jsonComment);
                        comments.add(comment);
                    }
                    catch (JSONException e1) {
                        e1.printStackTrace();
                    }

                }

                result = "true";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }



    public static java.util.Date getCurrentDate() {

        // Build the URL
        String url = baseUrl + getCurrentTimeUrl;
        java.util.Date result = null;
        try {
            HashMap<String, String> parameters = new HashMap<>();
            String postResponse = post(url, parameters);
            String dateAux=postResponse.substring(1,postResponse.length()-2);
            result = stringToDate(dateAux);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }



    private static java.util.Date stringToDate(String dataS) throws ParseException {
        java.util.Date date = format.parse(dataS.substring(0,dataS.length()-5)+"Z");
        System.out.println(date.toString());
        return date;
    }


    public static String createComment(Comment comment) {

        // Build the URL
        String url = baseUrl + createCommentUrl;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idIncidenceParameter, comment.getId_incidence());
        parameters.put(idUserParameter, comment.getId_user());
        parameters.put(textParameter, comment.getText());


        String created = post(url, parameters);

        return created;
    }


    private static Comment createComment(JSONObject jsonComment)  throws JSONException, ParseException {
        //TODO afgir birth i location

        String idIncidence = jsonComment.getString(idIncidenceParameter);
        String idUser = jsonComment.getString(idUserParameter);
        String text = jsonComment.getString(textParameter);
        String dateAux = jsonComment.getString(dateParameter);
        java.util.Date date = stringToDate(dateAux);
        String id = jsonComment.getString(idCommentParameter);
        Comment c = new Comment(idUser,idIncidence,text,date);
        c.setId(id);
        return c;


    }


    private static String post(String url, HashMap<String, String> parameters) {

        StringBuilder params=new StringBuilder("");
        String result="";
        try {
            for(String s:parameters.keySet()){
                params.append("&"+s+"=");

                params.append(URLEncoder.encode(parameters.get(s),"UTF-8"));
            }


            //String url =_url;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "UTF-8");

            con.setDoOutput(true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            in.close();

            result = response.toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (ProtocolException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            result="connectFail";
        }
        finally {
            return  result;
        }
    }
}

