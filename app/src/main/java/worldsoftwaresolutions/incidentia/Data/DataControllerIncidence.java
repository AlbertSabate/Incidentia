package worldsoftwaresolutions.incidentia.Data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

import worldsoftwaresolutions.incidentia.Domain.Comment;
import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.Date;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;


public class DataControllerIncidence {
    private static final String baseHostname = "10.4.41.153";
    private static final int basePort = 3002;
    private static final String baseUrl = "http://" + baseHostname + ":" + String.valueOf(basePort) + "/";
    private static final String deleteIncidenceUrl = "deleteIncidence";
    private static final String deleteIncidencesByIdUserUrl = "deleteIncidencesByIdUser";
    private static final String createIncidenceUrl = "createIncidence";
    private static final String editIncidenceUrl = "editIncidence";
    private static final String getIncidenceUrl = "getIncidence";
    private static final String getIncidenceByRadiusUrl = "getAllByRadius";
    private static final String getIncidenceByRadiusAndCategoryUrl = "getAllByRadiusAndCategory";
    private static final String getIncidenceByRadiusAndPriorityUrl = "getAllByRadiusAndPriority";
    private static final String getIncidenceByRadiusAndPriorityAndCategoryUrl = "getAllByRadiusAndPriorityAndCategory";
    private static final String idParam = "_id";
    private static final String descriptionParameter = "description";
    private static final String priorityParameter = "priority";
    private static final String categoryParameter = "category";
    private static final String poblationParameter = "poblation";
    private static final String latitudeParameter = "latitude";
    private static final String radiusParameter = "radius";
    private static final String longitudeParameter = "longitude";
    private static final String invalidParameter = "invalid";
    private static final String fixedParameter = "fixed";
    private static final String nbConfirmationsParameter = "nbConfirmations";
    private static final String confirmationsParameter = "confirmations";
    private static final String reportsParameter = "reports";
    private static final String confirmIncidence = "confirmIncidence";
    private static final String unconfirmIncidence = "unconfirmIncidence";
    private static final String reportIncidence = "reportIncidence";
    private static final String userIdParameter = "userId";
    private static final String numConfirmations = "numConfirmations";

    // Returns "true" if the Incidence is deleted, returns the error message in other cases
    public static String deleteIncidence(String id) {
        // Build the URL
        String url = baseUrl + deleteIncidenceUrl;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idParam, id);

        String deleted = post(url, parameters);

        return deleted;
    }

    public static String deleteIncidencesByIdUser(String mail) {
        //Build the URL
        String url = baseUrl + deleteIncidencesByIdUserUrl;
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(userIdParameter, mail);
        String postResponse = post(url, parameters);
        return postResponse;
    }

    public static String createIncidence(Incidence incidence) {

        // Build the URL
        String url = baseUrl + createIncidenceUrl;
        String id = "false";
        try {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(descriptionParameter, incidence.getDescription());
            parameters.put(priorityParameter, incidence.getPriority().name());
            parameters.put(categoryParameter, incidence.getCategory().name());
            parameters.put(latitudeParameter, String.valueOf(incidence.getLocation().latitude));
            parameters.put(longitudeParameter, String.valueOf(incidence.getLocation().longitude));
            parameters.put(poblationParameter, incidence.getPoblation());
            parameters.put(userIdParameter, incidence.getUserId());
            parameters.put(nbConfirmationsParameter, String.valueOf(incidence.getNb_confirmations()));
            parameters.put(invalidParameter, String.valueOf(false));
            parameters.put(fixedParameter, String.valueOf(false));
            String postResponse = post(url, parameters);
            if (postResponse.contains("connectFail")) {
                id = "connectFail";
            }
            else {
                System.out.print("postResponse: " + postResponse);
                String jsonIncidenceID = (String) new JSONTokener(postResponse).nextValue();

                if (jsonIncidenceID.length() >= 0) {
                    id = "ID:" + jsonIncidenceID;
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/incidences/");
                    StorageReference file = storageRef.child(jsonIncidenceID+".jpeg");
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    incidence.getPicture().compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();
                    file.putBytes(data);
                }

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        return id;
    }

    public static String editIncidence(String id, Bitmap picture, String description, double longitude, double latitude, String priority, String category, String poblation) {

        // Build the URL
        String url = baseUrl + editIncidenceUrl;
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idParam, id);
        parameters.put(descriptionParameter, description);
        parameters.put(longitudeParameter, String.valueOf(longitude));
        parameters.put(latitudeParameter, String.valueOf(latitude));
        parameters.put(priorityParameter, priority);
        parameters.put(poblationParameter, poblation);
        parameters.put(categoryParameter, category);
        String postResponse = post(url, parameters);
        if (postResponse.contains("true")) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/incidences/");
            StorageReference file = storageRef.child(id + ".jpeg");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] data = baos.toByteArray();
            file.putBytes(data);
        }
        return postResponse;

    }


    public static String getIncidence(Incidence incidence, String id) {

        // Build the URL
        String url = baseUrl + getIncidenceUrl;
        String result = "false";
        try {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(idParam, id);
            String postResponse = post(url, parameters);
            if (postResponse.contains("connectFail")) {
                result = "connectFail";
            }
            else {
                // Analyze response content
                System.out.print("POSTRESPONSE: " + postResponse);
                JSONArray jsonIncidenceArray = (JSONArray) new JSONTokener(postResponse).nextValue();
                JSONObject jsonIncidence = (JSONObject) jsonIncidenceArray.get(0);

                // Try
                try {
                    createIncidence(jsonIncidence, incidence);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                result = "true";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getIncidencesByRadius(List<Incidence> listIncidences, double latitude, double longitude, double radius, String priority, String category){
        String url = baseUrl ;
        String result = "false";
        try {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(latitudeParameter, String.valueOf(latitude));
            parameters.put(longitudeParameter, String.valueOf(longitude));
            parameters.put(radiusParameter, String.valueOf(radius));
            String postResponse;
            if(priority==null && category==null){
                 url += getIncidenceByRadiusUrl;
                 postResponse = post(url, parameters);

            }
            else if(priority==null){
                System.out.println("priority es nula");
                url += getIncidenceByRadiusAndCategoryUrl;
                parameters.put(categoryParameter,category);
                postResponse = post(url,parameters);
            }
            else if(category==null){
                url += getIncidenceByRadiusAndPriorityUrl;
                System.out.println("category es nula");
                parameters.put(priorityParameter,priority);
                postResponse = post(url,parameters);
            }
            else {
                parameters.put(priorityParameter,priority);
                System.out.println("cap es NULAAAA");
                url += getIncidenceByRadiusAndPriorityAndCategoryUrl;
                parameters.put(categoryParameter,category);
                postResponse = post(url,parameters);
            }


            if (postResponse.contains("connectFail")) {
                result = "connectFail";
            }
            else {
                // Analyze response content
                System.out.print(postResponse);
                JSONArray jsonIncidencesArray = (JSONArray) new JSONTokener(postResponse).nextValue();
                for(int k = jsonIncidencesArray.length()-1;k>=0; --k){
                    JSONObject jsonIncidence = (JSONObject) jsonIncidencesArray.get(k);
                    try {
                        Incidence incidence = new Incidence();
                        createIncidence(jsonIncidence,incidence);
                        listIncidences.add(incidence);
                    }
                    catch (JSONException e1) {
                        e1.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

                result = "true";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;


    }






    private static void createIncidence(JSONObject jsonIncidence,final Incidence incidence)  throws JSONException, ParseException {
        //TODO afgir location


        if (jsonIncidence.length() >= 0) {
            String incidenceId = jsonIncidence.getString(idParam);
            String incidenceDescription = jsonIncidence.getString(descriptionParameter);
            Incidence.Priority incidencePriority = Incidence.Priority.valueOf(jsonIncidence.getString(priorityParameter));
            Incidence.Category incidenceCategory = Incidence.Category.valueOf(jsonIncidence.getString(categoryParameter));
            String poblation = jsonIncidence.getString(poblationParameter);
            Double latitude = Double.parseDouble(jsonIncidence.getString(latitudeParameter));
            Double longitude = Double.parseDouble(jsonIncidence.getString(longitudeParameter));
            boolean invalid = Boolean.valueOf(jsonIncidence.getString(invalidParameter));
            boolean fixed = Boolean.valueOf(jsonIncidence.getString(fixedParameter));
            String userId = jsonIncidence.getString(userIdParameter);
            JSONArray aux = jsonIncidence.getJSONArray(confirmationsParameter);
            JSONArray aux2 = jsonIncidence.getJSONArray(reportsParameter);
            ArrayList<String> confirmations = new ArrayList<>();
            ArrayList<String> reports = new ArrayList<>();

            for(int i = 0; i < aux.length(); ++i) {
                confirmations.add((String) aux.get(i));
            }

            for(int i = 0; i < aux2.length(); ++i) {
                reports.add((String) aux2.get(i));
            }

            incidence.setId(incidenceId);
            incidence.setPicture(null);
            incidence.setPoblation(poblation);
            incidence.setDescription(incidenceDescription);
            incidence.setPriority(incidencePriority);
            incidence.setCategory(incidenceCategory);
            incidence.setLocation(new LatLng(latitude, longitude));
            incidence.setConfirmations(confirmations);
            incidence.setReports(reports);
            incidence.setUserId(userId);
            incidence.setInvalid(invalid);
            incidence.setFixed(fixed);

        }

    }

    public static String confirmIncidence(String incidenceId, String userId) {

        System.out.println("Accedeix a la base de dades per confirmar Incidencia");

        String url = baseUrl + confirmIncidence;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idParam, incidenceId);
        parameters.put(userIdParameter, CurrentUser.getInstance().getUser().getMail());

        String result = post(url, parameters);

        if (result.contains("true")) DataControllerUser.setScore(userId,String.valueOf(1));
        else System.out.println("ERROR: confirm incidence");

        return result;
    }

    public static String unconfirmIncidence(String incidenceId,String userId) {

        String url = baseUrl + unconfirmIncidence;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idParam, incidenceId);
        parameters.put(userIdParameter, CurrentUser.getInstance().getUser().getMail());

        String result = post(url, parameters);

        if (result.contains("true")) DataControllerUser.setScore(userId,String.valueOf(-1));
        else System.out.println("ERROR: unconfirm incidence");

        return result;

    }

    public static String ReportIncidence(String incidenceId) {

        String url = baseUrl + reportIncidence;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(idParam, incidenceId);
        parameters.put(userIdParameter, CurrentUser.getInstance().getUser().getMail());

        String result = post(url, parameters);

        return result;
    }

    private static String post(String url, HashMap<String, String> parameters) {

        StringBuilder params=new StringBuilder("");
        String result="";
        try {
            for(String s:parameters.keySet()){
                params.append("&"+s+"=");

                params.append(URLEncoder.encode(parameters.get(s),"UTF-8"));
            }


            //String url =_url;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "UTF-8");

            con.setDoOutput(true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            in.close();

            result = response.toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (ProtocolException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            result="connectFail";
        }
        finally {
            return  result;
        }
    }
}

//    FirebaseStorage storage = FirebaseStorage.getInstance();
//    StorageReference storageRef = storage.getReferenceFromUrl("gs://incidentiapes.appspot.com/");
//    StorageReference file = storageRef.child(incidenceId+".jpeg");
//
//    final long ONE_MEGABYTE = 1024 * 1024;
//    Task<byte[]> b = file.getBytes(ONE_MEGABYTE);
//    Bitmap incidencePicture = BitmapFactory.decodeByteArray(b[0] , 0, b[0].length);
