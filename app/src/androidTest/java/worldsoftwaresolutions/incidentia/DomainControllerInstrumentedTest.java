package worldsoftwaresolutions.incidentia;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import worldsoftwaresolutions.incidentia.Domain.Comment;
import worldsoftwaresolutions.incidentia.Domain.CreateCommentManager;
import worldsoftwaresolutions.incidentia.Domain.CreateIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.CreateUserManager;
import worldsoftwaresolutions.incidentia.Domain.CurrentUser;
import worldsoftwaresolutions.incidentia.Domain.Date;
import worldsoftwaresolutions.incidentia.Domain.DomainController;
import worldsoftwaresolutions.incidentia.Domain.GetAllCommentsByIdManager;
import worldsoftwaresolutions.incidentia.Domain.GetIncidenceManager;
import worldsoftwaresolutions.incidentia.Domain.GetUserManager;
import worldsoftwaresolutions.incidentia.Domain.Incidence;
import worldsoftwaresolutions.incidentia.Domain.User;
import worldsoftwaresolutions.incidentia.Exceptions.InvalidDateException;
import worldsoftwaresolutions.incidentia.Exceptions.MailIsNotValidException;
import worldsoftwaresolutions.incidentia.Exceptions.PasswordIsNotValidException;

import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class DomainControllerInstrumentedTest {

    private String id;

    @Test
    public void createUserInstrumentedTest() {
        User user = new User();
        user.setMail("user@test.com");

        CurrentUser.getInstance().setUser(user);

        DomainController.createIncidence(null, null, null, 0, "SIDEWALK", 0, null, new CreateIncidenceManager.TaskCreateIncidenceListener() {
            @Override
            public void onCreateIncidence(Incidence incidence) {
                id = incidence.getId();
                assertNotNull(incidence);
            }

            @Override
            public void onDoesNotCreateIncidence() {
                assert false;
            }

            @Override
            public void onNotConnection() {
                assert false;
            }
        });
    }

    @Test
    public void getUserIntrumentedTest() {
        DomainController.getIncidence(id, new GetIncidenceManager.TaskGetIncidenceListener() {
            @Override
            public void onGetIncidence(Incidence incidence) {
                assertNotNull(incidence);
            }

            @Override
            public void onDoesNotGetIncidence() {
                assert false;
            }

            @Override
            public void onNotConnection() {
                assert false;
            }
        });
    }

    @Test
    public void createUserIntrumentedTest() throws MailIsNotValidException, PasswordIsNotValidException, InvalidDateException {
        DomainController.createUser("userName", "userSurname", "user@test.com", "password", null, new Date(1,1,1111), false, new CreateUserManager.TaskCreateUserListener() {
            @Override
            public void onCreateUser() {
                assert true;
            }

            @Override
            public void onDoesNotCreateUser() {
                assert false;
            }

            @Override
            public void onNotConnection() {
                assert false;
            }
        });
    }

    @Test
    public void getuserInstrumentedTest() {
        DomainController.getUser("user@prova.com", new GetUserManager.TaskGetUserListener() {
            @Override
            public void onGetUser(User user) {
                assertNotNull(user);
            }

            @Override
            public void onDoesNotGetUser() {
                assert false;
            }

            @Override
            public void onNotConnection() {
                assert false;
            }
        });
    }

    @Test
    public void createCommentInstrumentedTest() {
        DomainController.createComment("user@test.com", id, "comment", new CreateCommentManager.TaskCreateCommentListener() {
            @Override
            public void onCreateComment() {
                assert true;
            }

            @Override
            public void onDoesNotCreateComment() {
                assert false;
            }

            @Override
            public void onNotConnection() {
                assert false;
            }
        });
    }

    @Test
    public void getCommentsInstrumentedTest() {
        DomainController.getAllCommentsById(id, new GetAllCommentsByIdManager.TaskGetAllCommentsByIdListener() {
            @Override
            public void onGetAllCommentsById(List<Comment> listComment) {
                assertNotNull(listComment);
            }

            @Override
            public void onDoesNotGetAllCommentsById() {
                assert false;
            }

            @Override
            public void onNotConnection() {
                assert false;
            }
        });
    }
}
