package worldsoftwaresolutions.incidentia;

import org.junit.Before;
import org.junit.Test;

import worldsoftwaresolutions.incidentia.Domain.Incidence;

import static org.junit.Assert.*;

public class IncidenceUnitTest {

    private Incidence incidence;

    @Before
    public void setUp() {
        incidence = new Incidence();
    }

    @Test
    public void incidenceIdTest() {
        String id = "id";
        incidence.setId(id);
        assertEquals(incidence.getId(),id);
    }

    @Test
    public void incidenceDescriptionUnitTest() {
        String description = "description";
        incidence.setDescription(description);
        assertEquals(incidence.getDescription(),description);
    }

    @Test
    public void incidencePriorityUnitTest() {
        incidence.setPriority(Incidence.Priority.HIGH);
        assertEquals(incidence.getPriority(), Incidence.Priority.HIGH);
    }

    @Test
    public void incidenceCategoryUnitTest() {
        incidence.setCategory(Incidence.Category.LIGHTS);
        assertEquals(incidence.getCategory(), Incidence.Category.LIGHTS);
    }

    @Test
    public void incidenceUserIdUnitTest() {
        String id = "id";
        incidence.setUserId(id);
        assertEquals(incidence.getUserId(),id);
    }
}