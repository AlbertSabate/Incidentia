package worldsoftwaresolutions.incidentia;

import org.junit.Before;
import org.junit.Test;

import worldsoftwaresolutions.incidentia.Domain.User;

import static org.junit.Assert.*;

public class UserUnitTest {

    private User user;

    @Before
    public void setUp() {
        user = new User();
    }

    @Test
    public void userMailUnitTest() {
        String mail = "mail";
        user.setMail(mail);
        assertEquals(user.getMail(),mail);
    }

    @Test
    public void userNameUnitTest() {
        String name = "name";
        user.setName(name);
        assertEquals(user.getName(),name);
    }

    @Test
    public void userPasswordUnitTest() {
        String password = "password";
        user.setPassword(password);
        assertEquals(user.getPassword(),password);
    }

    @Test
    public void userSurnameUnitTest() {
        String surname = "surname";
        user.setSurname(surname);
        assertEquals(user.getSurname(),surname);
    }

    @Test
    public void userLatitudeUnitTest() {
        double latitude = 2.5;
        user.setLatitude(latitude);
        assertEquals(user.getLatitude(),latitude,0);
    }

    @Test
    public void userLongitudeUnitTest() {
        double longitude = 2.5;
        user.setLongitude(longitude);
        assertEquals(user.getLongitude(),longitude,0);
    }

    @Test
    public void userScoreUnitTest() {
        int score = 3;
        user.setScore(score);
        assertEquals(user.getScore(),score);
    }
}
